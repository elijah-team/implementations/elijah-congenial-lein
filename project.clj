(defproject congenial-lein "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "??"
  :license {:name "??"
            :url  "??"}
  :repositories [["jitpack.io" "https://jitpack.io"]]
  :javac-options ["-target" "17" "-source" "17" "-g"]
  :dependencies [[org.clojure/clojure "1.11.1"]

                 ;[aleph "0.7.1"]
                 ;[integrant "0.8.1"]
                 [party.donut/system "0.0.241"]
                 [funcool/promesa "11.0.678"]
                 [missionary/missionary "b.36"]

                 ;[org.eclipse.xtend/org.eclipse.xtend.lib "2.34.0"]
                 [io.reactivex.rxjava3/rxjava "3.1.8"]
                 [org.reactivestreams/reactive-streams "1.0.4"]

                 [commons-codec/commons-codec "1.16.1"]
                 [org.apache.commons/commons-lang3 "3.14.0"]

                 [antlr/antlr "2.7.7"]

                 [org.jdeferred.v2/jdeferred-core "2.0.0"]

                 [org.jetbrains/annotations "24.1.0"]
                 [org.jetbrains.kotlin/kotlin-stdlib-jdk8 "1.9.22"]

                 ;                 api project(':tripleo.congenial.base')
                 ;                api project(':tripleo.elijah.congenial-vendor')
                 ;               api project(':tripleo.small')

                 ;[com.tngtech.archunit/archunit-junit4 "1.2.1"]
                 [org.jetbrains.kotlin/kotlin-test-junit "1.9.22"]
                 [org.hamcrest/hamcrest "2.2"]
                 [org.projectlombok/lombok "1.18.30"]

                 ; [org.eclipse.xtend/org.eclipse.xtend.core "2.34.0" :exclusions [org.eclipse.platform/org.eclipse.osgi]]
                 ; [tripleo.buffers/buffers-v1 "0.0.3"]
                 [com.gitlab.tripleo1/buffers "3038ff102c"]
                 [com.gitlab.Tripleo/range "v0.0.3b"]

                 [org.checkerframework/checker-qual "3.42.0"]

                 ]
  :test-paths ["clj-test"]
  :java-source-paths [
                      "java-combined-src"
                      ;"../elijah-base/src-bnxn/elijah-nextgen-outputstatement/src/main/java"
                      ;"../elijah-base/src-bnxn/elijah-stateful/src/main/java"
                      ;"../elijah-base/src-bnxn/tripleo-reactive/src/main/java"
                      ;"../tripleo-small/src/main/java/"
                      ;
                      ;"../elijah-base/src-damar/elijah-good-api/src/main/java"
                      ;"../elijah-base/src-damar/elijah-sourcemodel-api/src/main/java"
                      ;"../elijah-base/src-damar/elijah-sourcemodel-impl/src/main/java"
                      ;
                      ;"../elijah-base/src/main/java"
                      ;"../elijah-base/src/main/lombok"
                      ;;                      "../elijah-base/antilombok"
                      ;
                      ;;"../elijah-congenial-vendor/src/main/java"
                      ;;"../elijah-congenial-vendor/src/main/lombok"
                      ;
                      ;"../elijah-core/target-source/src"
                      ;"../elijah-core/src/main/java"
                      ;"../elijah-core/src/main/lombok"
                      ;"../elijah-core/xtend-gen/src/main/java"
                      ;"../elijah-core/antilombok"
                      ]
  :main ^:skip-aot congenial-lein.core
  :target-path "target/%s"
  :jvm-opts ["-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5010"]
  :profiles {:uberjar {:aot      :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}
             ;:user    {:plugins [[lein-nevam "0.1.2"]]}

             })
