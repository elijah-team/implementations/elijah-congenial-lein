package tripleo.vendor.org_apache_commons_cli;

import tripleo.elijah.comp.i.ICompilerInput;

import java.util.List;

public interface CommandLineParser {
	CommandLine parse(Options options, String[] arguments) throws ParseException;

	CommandLine parse(Options aOptions, List<ICompilerInput> aInputs) throws ParseException;
}
