/*
  Licensed to the Apache Software Foundation (ASF) under one or more
  contributor license agreements.  See the NOTICE file distributed with
  this work for additional information regarding copyright ownership.
  The ASF licenses this file to You under the Apache License, Version 2.0
  (the "License"); you may not use this file except in compliance with
  the License.  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
 */
package tripleo.vendor.org_apache_commons_cli;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.util.NotImplementedException;
import tripleo.elijah.util.ProgramIsWrongIfYouAreHere;

import java.io.Serializable;
import java.util.*;

import static tripleo.vendor.org_apache_commons_cli.Util.EMPTY_STRING_ARRAY;

public class CommandLine implements Serializable {
	private static final long                serialVersionUID = 1L;

	private final        List<ICompilerInput> ci_args    = new LinkedList<>();
	private final        List<ICompilerInput> ci_options = new ArrayList<>();
	private final        List<Option>        options          = new ArrayList<>();

	/**
	 * Creates a command line.
	 */
	protected CommandLine() {
		// nothing to do
	}

	protected void addArg(final ICompilerInput arg) {
		ci_args.add(arg);
	}

	protected void addOption(final Option opt) {
		options.add(opt);
	}

	public List<String> getArgList() {
//        return args;
		throw new NotImplementedException();
	}

	public String @NotNull [] getArgs() {
//        return args.toArray(Util.EMPTY_STRING_ARRAY);
		List<String> l = ci_args.stream()
				.filter(ICompilerInput::isNull)
				.map(ICompilerInput::getInp)
				.toList();

		String[] r = new String[l.size()];
		int      i = 0; // invariants
		for (ICompilerInput compilerInput : ci_args) {
			if (compilerInput.isNull())
				r[i] = compilerInput.getInp();
			i++;
		}
		return r;
	}


	/**
	 * Return the {@code Object} type of this {@code Option}.
	 *
	 * @param opt the name of the option.
	 * @return the type of opt.
	 * @deprecated due to System.err message. Instead use getParsedOptionValue(char)
	 */
	@Deprecated
	public Object getOptionObject(final char opt) {
		return getOptionObject(String.valueOf(opt));
	}

	/**
	 * Return the {@code Object} type of this {@code Option}.
	 *
	 * @param opt the name of the option.
	 * @return the type of this {@code Option}.
	 * @deprecated due to System.err message. Instead use getParsedOptionValue(String)
	 */
	@Deprecated
	public @Nullable Object getOptionObject(final String opt) {
		try {
			return getParsedOptionValue(opt);
		} catch (final ParseException pe) {
			System.err.println("Exception found converting " + opt + " to desired type: " + pe.getMessage());
			return null;
		}
	}

	/**
	 * Return a version of this {@code Option} converted to a particular type.
	 *
	 * @param opt the name of the option.
	 * @return the value parsed into a particular object.
	 * @throws ParseException if there are problems turning the option value into the desired type
	 * @see PatternOptionBuilder
	 * @since 1.2
	 */
	public Object getParsedOptionValue(final String opt) throws ParseException {
		return getParsedOptionValue(resolveOption(opt));
	}

	/**
	 * Return a version of this {@code Option} converted to a particular type.
	 *
	 * @param option the name of the option.
	 * @return the value parsed into a particular object.
	 * @throws ParseException if there are problems turning the option value into the desired type
	 * @see PatternOptionBuilder
	 * @since 1.5.0
	 */
	public @Nullable Object getParsedOptionValue(final @Nullable Option option) throws ParseException {
		if (option == null) {
			return null;
		}
		final String res = getOptionValue(option);
		if (res == null) {
			return null;
		}
		return TypeHandler.createValue(res, option.getType());
	}

	private @Nullable Option resolveOption(String opt) {
		opt = Util.stripLeadingHyphens(opt);
		for (final Option option : options) {
			if (opt.equals(option.getOpt()) || opt.equals(option.getLongOpt())) {
				return option;
			}

		}
		return null;
	}

	/**
	 * Retrieve the first argument, if any, of this option.
	 *
	 * @param option the name of the option.
	 * @return Value of the argument if option is set, and has an argument, otherwise null.
	 * @since 1.5.0
	 */
	public @Nullable String getOptionValue(final @Nullable Option option) {
		if (option == null) {
			return null;
		}
		final String[] values = getOptionValues(option);
		return values == null ? null : values[0];
	}

	public String @Nullable [] getOptionValues(final Option option) {
		final List<String> values = new ArrayList<>();

		for (final Option processedOption : options) {
			if (processedOption.equals(option)) {
				values.addAll(processedOption.getValuesList());
			}
		}

		return values.isEmpty() ? null : values.toArray(EMPTY_STRING_ARRAY);
	}

	public @NotNull Properties getOptionProperties(final Option option) {
		final Properties props = new Properties();

		for (final Option processedOption : options) {
			if (processedOption.equals(option)) {
				final List<String> values = processedOption.getValuesList();
				if (values.size() >= 2) {
					// use the first 2 arguments as the key/value pair
					props.put(values.get(0), values.get(1));
				} else if (values.size() == 1) {
					// no explicit value, handle it as a boolean
					props.put(values.get(0), "true");
				}
			}
		}

		return props;
	}

	public @NotNull Properties getOptionProperties(final @NotNull String opt) {
		final Properties props = new Properties();

		for (final Option option : options) {
			if (opt.equals(option.getOpt()) || opt.equals(option.getLongOpt())) {
				final List<String> values = option.getValuesList();
				if (values.size() >= 2) {
					// use the first 2 arguments as the key/value pair
					props.put(values.get(0), values.get(1));
				} else if (values.size() == 1) {
					// no explicit value, handle it as a boolean
					props.put(values.get(0), "true");
				}
			}
		}

		return props;
	}

	public Option @NotNull [] getOptions() {
		return options.toArray(Option.EMPTY_ARRAY);
	}

	public String getOptionValue(final char opt) {
		return getOptionValue(String.valueOf(opt));
	}

	public String getOptionValue(final String opt) {
		return getOptionValue(resolveOption(opt));
	}

	public String getOptionValue(final char opt, final String defaultValue) {
		return getOptionValue(String.valueOf(opt), defaultValue);
	}

	public String getOptionValue(final String opt, final String defaultValue) {
		return getOptionValue(resolveOption(opt), defaultValue);
	}

	public String getOptionValue(final Option option, final String defaultValue) {
		final String answer = getOptionValue(option);
		return answer != null ? answer : defaultValue;
	}

	public String[] getOptionValues(final char opt) {
		return getOptionValues(String.valueOf(opt));
	}

	public String[] getOptionValues(final String opt) {
		return getOptionValues(resolveOption(opt));
	}

	public Object getParsedOptionValue(final char opt) throws ParseException {
		return getParsedOptionValue(String.valueOf(opt));
	}

	public boolean hasOption(final char opt) {
		return hasOption(String.valueOf(opt));
	}

	public boolean hasOption(final String opt) {
		return hasOption(resolveOption(opt));
	}

	public boolean hasOption(final Option opt) {
		return options.contains(opt);
	}

	public @NotNull Iterator<Option> iterator() {
		return options.iterator();
	}

	public void addArg(final String ignoredString) {
		throw new ProgramIsWrongIfYouAreHere("CommandLine#addArg(String)");
	}
}
