package tripleo.elijah_congenial.lang;

import com.google.common.collect.ImmutableList;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.lang.i.Context;
import tripleo.elijah_durable_congenial.lang.i.ISearchList;

import java.util.ArrayList;
import java.util.List;

public class SearchList implements ISearchList {
	@NotNull List<Context> alreadySearched = new ArrayList<>();

	@Override
	public void add(Context c) {
		alreadySearched.add(c);
	}

	@Override
	public boolean contains(Context context) {
		return alreadySearched.contains(context);
	}

	@Override
	public @NotNull ImmutableList<Context> getList() {
		return ImmutableList.copyOf(alreadySearched);
	}
}
