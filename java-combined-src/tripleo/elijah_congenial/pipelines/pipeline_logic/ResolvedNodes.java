package tripleo.elijah_congenial.pipelines.pipeline_logic;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.PipelineLogic;
import tripleo.elijah_congenial.deduce.t.TEvaClass;
import tripleo.elijah_durable_congenial.comp.Finally;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.nextgen.rosetta.DeducePhase.DeducePhase_deduceModule_Request;
import tripleo.elijah_durable_congenial.stages.deduce.DeducePhase;
import tripleo.elijah_durable_congenial.stages.deduce.GeneratedClasses;
import tripleo.elijah_durable_congenial.stages.gen_fn.*;
import tripleo.elijah_durable_congenial.stages.gen_generic.ICodeRegistrar;
import tripleo.elijah_durable_congenial.world.i.WorldModule;

import java.util.ArrayList;
import java.util.List;

class ResolvedNodes {
	final         List<EvaNode>  resolved_nodes = new ArrayList<EvaNode>();
	private final ICodeRegistrar cr;
	private final Compilation cc;

	public ResolvedNodes(final ICodeRegistrar aCr, final Compilation aCc) {
		cr = aCr;
		cc = aCc;
	}

	public void do_better(final GeneratedClasses lgc, final @NotNull PipelineLogic pipelineLogic, final @NotNull WorldModule worldModule) {
		this.init(lgc);
		this.part2();
		this.part3(pipelineLogic, worldModule, lgc);
	}

	public void init(final @NotNull GeneratedClasses c) {
		if (cc.reports().outputOn(Finally.Outs.Out_6262)) {
			System.err.println("2222 " + c);
		}

		for (final EvaNode evaNode : c) {
			final GNCoded coded;

			if (evaNode instanceof TEvaClass tec) {
				coded = tec.getCoded();
			} else if (!(evaNode instanceof GNCoded)) {
				throw new IllegalStateException("node must be coded");
			} else {
				coded = (GNCoded) evaNode;
			}

			switch (coded.getRole()) {
			case FUNCTION -> {
				cr.registerFunction1((IBaseEvaFunction) evaNode);
			}
			case CLASS -> {
				if (evaNode instanceof TEvaClass tec) {
					final IEvaClass evaClass = tec.getEvaClass();

					if (evaClass.getCode() == 0) {
						cr.registerClass1(evaClass);
					} else {
						// FIXME 09/10 enable this
						// complain
					}

					for (IEvaClass evaClass2 : evaClass.getClassMapValues()) {
						if (evaClass2.getCode() == 0) {
							cr.registerClass1(evaClass2);
						}
					}
					for (IEvaFunction generatedFunction : evaClass.getFunctionMapValues()) {
						for (IdentTableEntry identTableEntry : generatedFunction.idte_list()) {
							identTableEntry.onResolvedType(resolved_nodes::add);
						}
					}
				} else {
					final IEvaClass evaClass = (EvaClass) evaNode;

					if (evaClass.getCode() == 0) {
						cr.registerClass1(evaClass);
					} else {
						// FIXME 09/10 enable this
						// complain
					}

					for (IEvaClass evaClass2 : evaClass.getClassMapValues()) {
						if (evaClass2.getCode() == 0) {
							cr.registerClass1(evaClass2);
						}
					}
					for (IEvaFunction generatedFunction : evaClass.getFunctionMapValues()) {
						for (IdentTableEntry identTableEntry : generatedFunction.idte_list()) {
							identTableEntry.onResolvedType(resolved_nodes::add);
						}
					}
				}
			}
			case NAMESPACE -> {
				final EvaNamespace evaNamespace = (EvaNamespace) evaNode;
				if (coded.getCode() == 0) {
					//coded.setCode(mod.getCompilation().nextClassCode());
					cr.registerNamespace(evaNamespace);
				}
				for (IEvaClass evaClass3 : evaNamespace.classMap.values()) {
					if (evaClass3.getCode() == 0) {
						//evaClass.setCode(mod.getCompilation().nextClassCode());
						cr.registerClass1(evaClass3);
					}
				}
				for (IEvaFunction generatedFunction : evaNamespace.functionMap.values()) {
					for (IdentTableEntry identTableEntry : generatedFunction.idte_list()) {
						identTableEntry.onResolvedType(resolved_nodes::add);
					}
				}
			}
			default -> throw new IllegalStateException("Unexpected value: " + coded.getRole());
			}
		}
	}

	public void part2() {
		resolved_nodes.stream()
				.filter(evaNode -> evaNode instanceof GNCoded)
				.map(evaNode -> (GNCoded) evaNode)
				.filter(coded -> coded.getCode() == 0)
				.forEach(coded -> {
					System.err.println("-*-*- __processResolvedNodes [NOT CODED] " + coded);
					coded.register(cr);
				});
	}

	public void part3(final @NotNull PipelineLogic pipelineLogic,
					  final @NotNull WorldModule mod,
					  final GeneratedClasses lgc) {
		final DeducePhase deducePhase = pipelineLogic.dp;

		final DeducePhase_deduceModule_Request rq = new DeducePhase_deduceModule_Request(mod.module(),
																						 lgc,
																						 pipelineLogic.getVerbosity(),
																						 deducePhase);
		deducePhase.deduceModule(rq);
	}
}
