package tripleo.elijah_congenial.pipelines.eva;

import org.jdeferred2.DoneCallback;
import tripleo.elijah_durable_congenial.comp.AccessBus;
import tripleo.elijah_durable_congenial.comp.i.CompilationEnclosure;
import tripleo.elijah_durable_congenial.comp.i.IPipelineAccess;
import tripleo.elijah_durable_congenial.comp.internal.Provenance;
import tripleo.elijah_durable_congenial.stages.gen_fn.EvaNode;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaFunction;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaNamespace;
import tripleo.elijah_durable_congenial.stages.gen_generic.pipeline_impl.GenerateResultSink;

import java.util.List;

public interface EvaPipelineImpl_PipelineAccess {
	CompilationEnclosure getCompilationEnclosure();

	AccessBus getAccessBus();

	void registerNodeList(DoneCallback<List<EvaNode>> cb);

	void setGenerateResultSink(GenerateResultSink aGenerateResultSink);

	void setEvaPipeline(EvaPipelineImpl aEvaPipeline);

	void install_notate(Provenance aProvenance, Class<? /*super GN_Notable*/> aNotableClass, Class<? /*super GN_Env*/> aEnvClass);

	@Deprecated IPipelineAccess _dgrs();

	void activeFunction(IEvaFunction aEvaFunction);

	void activeClass(IEvaClass aEvaClass);

	void activeNamespace(IEvaNamespace aEvaNamespace);

	@Deprecated String getFilename_forfunctionStatement(FunctionStatement aFunctionStatement);
}
