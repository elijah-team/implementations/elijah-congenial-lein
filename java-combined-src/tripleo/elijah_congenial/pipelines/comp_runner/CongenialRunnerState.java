package tripleo.elijah_congenial.pipelines.comp_runner;

import tripleo.elijah_durable_congenial.comp.i.CompilationEnclosure;
import tripleo.elijah_durable_congenial.comp.i.ICompilationAccess;
import tripleo.elijah_durable_congenial.comp.i.ProcessRecord;
import tripleo.elijah_durable_congenial.comp.internal.CompilationRunner;

public interface CongenialRunnerState {
	ICompilationAccess ca();

	CompilationEnclosure ce();

	CompilationRunner runner();

	void setRunner(CompilationRunner aCompilationRunner);

	ProcessRecord _access_pr();

	void set_started();

	boolean started();
}
