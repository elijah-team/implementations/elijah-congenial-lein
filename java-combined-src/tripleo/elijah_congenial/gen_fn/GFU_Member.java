package tripleo.elijah_congenial.gen_fn;

import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaFunction;

public record GFU_Member(IEvaFunction aGf) implements GFU_Understanding { }
