package tripleo.elijah_congenial.deduce.t;

import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.stages.gen_fn.*;
import tripleo.elijah_durable_congenial.stages.gen_generic.ICodeRegistrar;
import tripleo.elijah_durable_congenial.stages.gen_generic.IREvaClass;
import tripleo.elijah_durable_congenial.world.i.LivingClass;

import java.util.function.Consumer;

public class TEvaClass implements IREvaClass, EvaNode {
	private final EvaNode evaNode;
	private int           code;
	private _TEvaClass_GNCoded tEvaClassGnCoded;

	public TEvaClass(final IEvaClass aClass) {
		this.evaNode = aClass;
	}

	@Override
	public String identityString() {
		return evaNode.identityString();
	}

	@Override
	public OS_Module module() {
		return evaNode.module(); //
	}

	@Override
	public boolean withEvaContainerNC(final Consumer<EvaContainerNC> cb) {
		cb.accept((EvaContainerNC) evaNode);
		return true;
	}

	@Override
	public void register(final ICodeRegistrar aRegistrar) {
		getCoded().register(aRegistrar);
	}

	public GNCoded getCoded() {
		if (tEvaClassGnCoded == null) {
			tEvaClassGnCoded = new _TEvaClass_GNCoded(this);
		}
		return tEvaClassGnCoded;
	}

	@Override
	public ClassStatement getKlass() {
		return ((EvaClass) evaNode).getKlass();
	}

	@Override
	public IEvaClass getEvaClass() {
		return ((EvaClass) evaNode);
	}

	@Override
	public int getCode() {
		return getCoded().getCode();
	}

	@Override
	public void setCode(final int aI) {
		getCoded().setCode(aI); // code = aI; ... // we did this before
	}

	@Override
	public void setLiving(final LivingClass aLivingClass) {
		((EvaClass) evaNode).setLiving(aLivingClass);
	}

	private class _TEvaClass_GNCoded implements GNCoded {
		private final TEvaClass parentCarrier;

		public _TEvaClass_GNCoded(final TEvaClass aParentCarrier) {
			this.parentCarrier = aParentCarrier;
		}

		@Override
		public int getCode() {
			return parentCarrier.code;
		}

		@Override
		public void setCode(final int aCode) {
			parentCarrier.code = aCode;
		}

		@Override
		public Role getRole() {
			return Role.CLASS;
		}

		@Override
		public void register(final ICodeRegistrar aRegistrar) {
			assert false;
		}
	}
}
