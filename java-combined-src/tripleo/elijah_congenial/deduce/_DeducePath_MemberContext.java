package tripleo.elijah_congenial.deduce;

import org.jetbrains.annotations.Nullable;
import tripleo.elijah_durable_congenial.lang.i.*;
import tripleo.elijah_durable_congenial.lang.impl.ContextImpl;
import tripleo.elijah_durable_congenial.lang.impl.VariableStatementImpl;
import tripleo.elijah_durable_congenial.stages.deduce.DeducePath;
import tripleo.elijah_durable_congenial.stages.gen_fn.GenType;

public class _DeducePath_MemberContext extends ContextImpl {

	private final DeducePath deducePath;
	private final OS_Element element;
	private final int        index;
	private final @Nullable GenType    type;

	public _DeducePath_MemberContext(DeducePath aDeducePath, int aIndex, OS_Element aElement) {
		assert aIndex >= 0;

		deducePath = aDeducePath;
		index      = aIndex;
		element    = aElement;

		type = deducePath.getType(aIndex);
	}

	@Override
	public @Nullable Context getParent() {
		if (index == 0)
			return element.getContext().getParent();
		return deducePath.getContext(index - 1);
	}

	@Override
	public @Nullable LookupResultList lookup(String name, int level, LookupResultList Result, ISearchList alreadySearched, boolean one) {
//			if (index == 0)


		if (type.getResolved() == null) {

			//c = getContext(this.index)
			@Nullable final OS_Element ell = deducePath.getElement(this.index);
			if (ell == null) {
				throw new AssertionError("202 no element found");
			} else {

				if (ell instanceof VariableStatementImpl) {
					VariableStatementImpl variableStatement = (VariableStatementImpl) ell;
					final Context         ctx2              = variableStatement.getParent().getContext();

					String n2 = null;
					if (type.getNonGenericTypeName() != null) {
						final RegularTypeName ngtn = (RegularTypeName) type.getTypeName().getTypeName();
						n2 = ngtn.getName();
					}

					if (n2 != null) {
						return ctx2.lookup(n2, level + 1, Result, alreadySearched, one);
					}
				}
			}
			return null;
		}


		return type.getResolved().getElement().getContext().lookup(name, level, Result, alreadySearched, one);
//			else
//				return null;
	}
}
