package tripleo.elijah_congenial.deduce.umbrella;

import tripleo.elijah_durable_congenial.nextgen.query.QueryRef;

public interface DS_NamedEntity {
	QueryRef getQueryRef();
}
