package tripleo.elijah_congenial.deduce.umbrella;

import tripleo.elijah_durable_congenial.lang.i.FunctionDef;
import tripleo.elijah_durable_congenial.stages.deduce.FunctionInvocation;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;


/**
 * This is a callback for resolveWith.
 * <p>
 * It should be prepared with a {@link DS_Rider}
 * </p>
 */
public interface DS_FunctionDef extends DS_Base {
	void accept(FunctionDef fd);
	void accept(IBaseEvaFunction gf);
	void accept(FunctionInvocation aFunctionInvocation);
}
