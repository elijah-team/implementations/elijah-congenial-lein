package tripleo.elijah_congenial.moogly;

import tripleo.elijah_durable_congenial.stages.deduce.DeducePhase;

public interface MDeducePhaseCatcher {
	void doCatch(DeducePhase aDeducePhase);
}
