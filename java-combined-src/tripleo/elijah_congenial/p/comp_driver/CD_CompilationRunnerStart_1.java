package tripleo.elijah_congenial.p.comp_driver;

import org.jetbrains.annotations.*;
import tripleo.elijah.ci.*;
import tripleo.elijah_durable_congenial.comp.i.*;
import tripleo.elijah_durable_congenial.comp.internal.*;

public class CD_CompilationRunnerStart_1 implements CD_CompilationRunnerStart {
	@Override
	public void start(final @NotNull CompilerInstructions aRootCI,
	                  final @NotNull CR_State crState,
	                  final @NotNull CB_Output out) {
		final @NotNull CompilationRunner cr          = crState.runner();
		final Compilation                compilation = crState.ce().getCompilation();

		compilation.pushItem(new CSS_StartRunner_SimpleSignal(cr, compilation, aRootCI, crState, out));
	}
}
