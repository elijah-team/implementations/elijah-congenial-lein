package tripleo.elijah_congenial.p.comp_driver;

import static tripleo.elijah_durable_congenial.util.Helpers.List_of;

import java.util.ArrayList;
import java.util.List;

import org.jetbrains.annotations.NotNull;

import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.util.Mode;
import tripleo.elijah.util.Ok;
import tripleo.elijah.util.Operation;
import tripleo.elijah_durable_congenial.comp.i.CR_Action;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.internal.CB_Output;
import tripleo.elijah_durable_congenial.comp.internal.CR_ProcessInitialAction;
import tripleo.elijah_durable_congenial.comp.internal.CR_RunBetterAction;
import tripleo.elijah_durable_congenial.comp.internal.CR_State;
import tripleo.elijah_durable_congenial.comp.internal.CSS_RunEnv;
import tripleo.elijah_durable_congenial.comp.internal.CSS_SimpleSignal;
import tripleo.elijah_durable_congenial.comp.internal.CompilationRunner;
import tripleo.elijah_durable_congenial.comp.internal.CompilerBeginning;
import tripleo.elijah_durable_congenial.comp.internal.U;

class CSS_StartRunner_SimpleSignal implements CSS_SimpleSignal {
	private final @NotNull CompilationRunner    cr;
	private final          Compilation          compilation;
	private final @NotNull CompilerInstructions rootCI;
	private final @NotNull CR_State             crState;
	private final @NotNull CB_Output            out;

	public CSS_StartRunner_SimpleSignal(final @NotNull CompilationRunner aCr, final Compilation aCompilation, final @NotNull CompilerInstructions aRootCI, final @NotNull CR_State aCrState, final @NotNull CB_Output aOut) {
		cr          = aCr;
		compilation = aCompilation;
		rootCI      = aRootCI;
		crState     = aCrState;
		out         = aOut;
	}

	@Override
	public boolean canRun() {
		return true;
	}

	@Override
	public void simpleSignalRun(final CSS_RunEnv aRunEnv) {
		assert cr == aRunEnv.cr();
		//assert ce == aRunEnv.ce();
		assert compilation == aRunEnv.ce().getCompilation();

		// TODO 23/11/16 ca3??
		//  also this maybe wanted to be progressive (see other )
		// README 24/03/09 A Progressive is
		//  (a collection of joins/ek/latches or incremental)
		final CompilerBeginning beginning = new CompilerBeginning(
				compilation,
				rootCI,
				null, //compilerInputs,
				cr.progressSink,
				null //cfg
		);

		// TODO 23/11/16 pa.notate (? -> prob)
		if (crState.started()) {
			boolean should_never_happen = false;
			// noinspection ConstantValue
			assert should_never_happen;
		} else {
			crState.set_started();

			//final CR_FindCIs              f1 = crState.runner().cr_find_cis;
			final CR_ProcessInitialAction f2 = new CR_ProcessInitialAction(beginning);
			//final CR_AlmostComplete       f3 = new CR_AlmostComplete();
			final CR_RunBetterAction f4 = new CR_RunBetterAction();

			//final @NotNull List<CR_Action>     crActionList       = List_of(f1, f2, f3, f4);
			final @NotNull List<CR_Action>     crActionList       = List_of(f2, f4);
			final @NotNull List<Operation<Ok>> crActionResultList = new ArrayList<>(crActionList.size());

			for (final CR_Action each : crActionList) {
				U._run_action_list__internal(crState, out, each, crActionResultList);
			}

			for (int i = 0; i < crActionResultList.size(); i++) {
				var                 action           = crActionList.get(i);
				final Operation<Ok> booleanOperation = crActionResultList.get(i);

				final String s = ("5959 %s %b").formatted(action.name(), (booleanOperation.mode() == Mode.SUCCESS));
				out.logProgress(5959, s);
			}
		}
	}

	@Override
	public boolean isOnce() {
		return true; // yes no complain complainHere(Consumer<CSS_ComplaintEnv>) // or you could just
	}
}
