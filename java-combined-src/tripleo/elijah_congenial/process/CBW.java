package tripleo.elijah_congenial.process;

import tripleo.elijah_durable_congenial.comp.i.CB_Process;
import tripleo.elijah_durable_congenial.comp.i.ICompilationBus;

import java.util.concurrent.*;

public class CBW {
	private final ICompilationBus           cb;
	private final BlockingDeque<CB_Process> start = new LinkedBlockingDeque<>();
	private final ExecutorService           threadPool;

	public CBW(final ICompilationBus aCompilationBus) {
		cb = aCompilationBus;

		int threadCount = 5; //  i think there's only 3

		threadPool = Executors.newFixedThreadPool(threadCount);
		threadPool.execute(() -> {
			System.err.println("blorp");
			while (true) {
				try {
					CB_Process p = start.poll(1, TimeUnit.SECONDS);
					System.err.println("beep");


					assert p != null;

					//threadPool.submit(() -> {
					cb.runOneProcess(p);
					//});
				} catch (InterruptedException aE) {
					System.err.println("9998-0029 " + aE);
					//throw new RuntimeException(aE);
				}
			}
		});

	}

	public void submit(final CB_Process aProcess) {
		start.offer(aProcess);
	}

	public void start() {
		Runnable runnable = new Runnable() {
			public void run() {
				try {
					threadPool.awaitTermination(5, TimeUnit.SECONDS);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}
		};
		new Thread(runnable).start();
	}
}
