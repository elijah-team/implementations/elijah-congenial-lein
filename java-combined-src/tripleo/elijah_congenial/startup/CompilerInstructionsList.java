package tripleo.elijah_congenial.startup;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CompilerInstructionsList {
	private final File                            directory;
	private final CilRole                         role;
	private final CompilationClosure cc;
	private final Map<File, CompilerInstructions> puts = new HashMap<>();
	private       String[]                        directoryList;

	public CompilerInstructionsList(final File aDirectory, final @NotNull CompilationClosure aCc) {
		cc        = aCc;
		role      = CilRole.DIR;
		directory = aDirectory;
	}

	public void setDirectoryList(final String[] aDirectoryList) {
		directoryList = aDirectoryList;
	}

	public void put(final File aFile, final CompilerInstructions aEzFile) {
		//getPromise(aFile).resolve(aEzFile);
		puts.put(aFile, aEzFile);
	}

	public void reportError(final int code, final Object message) {
		this.cc.errSink().reportError(""+code+" "+message.toString());
	}

	public List<CompilerInstructions> getList() {
		return this.puts.values().stream().toList();
	}

	public enum CilRole {DIR}
}
