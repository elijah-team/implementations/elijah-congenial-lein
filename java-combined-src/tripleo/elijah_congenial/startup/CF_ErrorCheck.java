package tripleo.elijah_congenial.startup;

import tripleo.elijah_congenial.startup.CF_Base;
import tripleo.elijah_durable_congenial.comp.i.Compilation;

public interface CF_ErrorCheck extends CF_Base {
	@Override
	void run(Compilation c);
}
