package tripleo.elijah_congenial.startup;

import tripleo.elijah_durable_congenial.comp.i.Compilation;

public interface CF_Base {
	void run(Compilation c);
}
