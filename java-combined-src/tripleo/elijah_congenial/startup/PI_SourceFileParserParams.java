package tripleo.elijah_congenial.startup;

//import tripleo.elijah.ci.CompilerInstructions;
//import tripleo.elijah.comp.internal.CSS_SimpleSignal;
import tripleo.elijah_durable_congenial.comp.internal.SourceFileParserParams;

public class PI_SourceFileParserParams /*implements CSS_SimpleSignal*/ {
	private final SourceFileParserParams p;

	public PI_SourceFileParserParams(final SourceFileParserParams aP) {
		p = aP;
	}

	public SourceFileParserParams getP() {
		return p;
	}
}
