package tripleo.elijah_congenial.startup;

import tripleo.elijah_congenial.startup.CF_Base;
import tripleo.elijah_durable_congenial.comp.i.Compilation;

public record CF_turnAllOutputOff() implements CF_Base {
	@Override
	public void run(final Compilation c) {
		c.reports().turnAllOutputOff();
	}
}
