package tripleo.elijah_congenial.startup;

import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah_durable_congenial.comp.CompilerInput;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.i.CompilerController;

import java.util.List;
import java.util.stream.Collectors;

public record CF_Start() implements CF_Base {
	@Override
	public void run(final Compilation c) {
		final CF_Inputs x        = (CF_Inputs) c.getFluffy().getStart();
		final boolean   hasStart = x != null;
		if (hasStart) {
			x.deferredRun(c);
		} else {
			final List<ICompilerInput> inps       = x.getInps().stream().map(CompilerInput::new).map(xx -> (ICompilerInput) xx).collect(Collectors.toList());
			final CompilerController   controller = c.getFluffy().getController();
			assert controller != null;
			c.feedInputs(inps, controller);
		}
	}
}
