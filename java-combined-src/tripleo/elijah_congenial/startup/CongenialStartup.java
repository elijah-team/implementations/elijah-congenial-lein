package tripleo.elijah_congenial.startup;

import tripleo.elijah.util.Eventual;

import java.util.HashMap;
import java.util.Map;

public enum CongenialStartup {
	;
	private static final Map<Class<?>, Eventual<?>> ps = new HashMap<>(); // PersistentMap?

	public static <T> void resolve(Class<T> aClass, T aValue) {
		var v = getPromise(aClass);
		v.resolve(aValue);
	}

	public static <T> Eventual<T> getPromise(final Class<T> aClass) {
		if (!ps.containsKey(aClass)) {
			var ne = new Eventual<T>();
			ps.put(aClass, ne);
			return ne;
		} else {
			//noinspection unchecked
			return (Eventual<T>) ps.get(aClass);
		}
	}
}
