package tripleo.elijah_congenial.startup;

import tripleo.elijah_congenial.startup.CF_Base;
import tripleo.elijah_durable_congenial.comp.i.Compilation;

import java.util.List;

public class CF_Inputs implements CF_Base {
	private final List<String> inputString;

	public CF_Inputs(final List<String> aInputString) {
		inputString = aInputString;
	}

	@Override
	public void run(Compilation c) {
		c.getFluffy().setStart(this);
	}

	public void deferredRun(final Compilation c) {
		try {
			c.feedCmdLine(inputString);
		} catch (Exception aE) {
			c.getErrSink().exception(aE);
		}
	}

	public List<String> getInps() {
		return this.inputString;
	}
}
