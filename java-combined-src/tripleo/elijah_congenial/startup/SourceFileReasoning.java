package tripleo.elijah_congenial.startup;

public enum SourceFileReasoning {
	SRC, EZ, STDLIB;
}
