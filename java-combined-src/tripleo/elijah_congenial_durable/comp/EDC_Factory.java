package tripleo.elijah_congenial_durable.comp;

import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.comp.i.ILazyCompilerInstructions;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;

public interface EDC_Factory {
	ILazyCompilerInstructions mkILazyCompilerInstructions(ICompilerInput aInput, CompilationClosure aCompilationClosure);
}
