package tripleo.elijah_congenial_durable.comp;

import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.comp.i.ILazyCompilerInstructions;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;

public class EDC_Factory1 implements EDC_Factory {
	@Override
	public ILazyCompilerInstructions mkILazyCompilerInstructions(final ICompilerInput aInput, final CompilationClosure aCompilationClosure) {
		return ILazyCompilerInstructions_.of(aInput, aCompilationClosure);
	}
}
