package tripleo.elijah_congenial_durable.comp;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.comp.i.ILazyCompilerInstructions;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Mode;
import tripleo.elijah.util.Operation;
import tripleo.elijah.util.UnintendedUseException;
import tripleo.elijah_congenial.startup.PI_SourceFileParserParams;
import tripleo.elijah_congenial.startup.SourceFileReasoning;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;
import tripleo.elijah_durable_congenial.comp.internal.SourceFileParserParams;

import java.io.File;

public enum ILazyCompilerInstructions_ {
	;

	@Contract(value = "_ -> new", pure = true)
	public static @NotNull ILazyCompilerInstructions of(final @NotNull CompilerInstructions aCompilerInstructions) {
		return new __ILazyCompilerInstructions_Given(aCompilerInstructions);
	}

	@Contract(value = "_, _ -> new", pure = true)
	public static @NotNull ILazyCompilerInstructions of(final @NotNull ICompilerInput input, final @NotNull CompilationClosure cc) {
		return new __ILazyCompilerInstructions__convertedFrom_CompilerInput(cc.getFile(input.getInp(), input), input, cc);
	}

	public record __ILazyCompilerInstructions_Given(
			//Eventual<Operation<CompilerInstructions>> e,
			CompilerInstructions                      compilerInstructions
	) implements ILazyCompilerInstructions {
		@Override
		public Eventual<Operation<CompilerInstructions>> getEventual() {
			throw new UnintendedUseException("this could have been a null");
			//return null; // I guess
		}

		@Override
		public CompilerInstructions get() {
			return this.compilerInstructions();
		}
	}

	private static class __ILazyCompilerInstructions__convertedFrom_CompilerInput implements ILazyCompilerInstructions {
		private final @NotNull Eventual<Operation<CompilerInstructions>> e;
		private final          File                                      f;
		private final @NotNull ICompilerInput                            input;
		private final @NotNull CompilationClosure                        cc;

		public __ILazyCompilerInstructions__convertedFrom_CompilerInput(final File aF,
		                                                                final @NotNull ICompilerInput aInput,
		                                                                final @NotNull CompilationClosure aCc) {
			f     = aF;
			input = aInput;
			cc    = aCc;
			e     = new Eventual<>();
		}

		@Override
		public Eventual<Operation<CompilerInstructions>> getEventual() {
			return e;
		}

		@Override
		public CompilerInstructions get() {
			try {
				final var file_name = f.getPath();
				final var p         = new SourceFileParserParams(input, f, file_name, cc, e, SourceFileReasoning.EZ);

				final PI_SourceFileParserParams pisfpp = new PI_SourceFileParserParams(p);
				cc.getCompilation().pushItem(pisfpp);

				final Operation<CompilerInstructions> oci = cc.getCompilation().getCompilationEnclosure().getCompilationRunner().parseEzFile(p);

				if (oci.mode() == Mode.SUCCESS) {
					final CompilerInstructions parsed = oci.success();
					return parsed;
				} else {
					throw new RuntimeException(oci.failure()); // TODO ugh
				}
			} catch (Exception aE) {
				//return Operation.failure(aE);
				throw new RuntimeException(aE); // TODO ugh
			}
		}
	}
}
