package tripleo.elijah_durable_congenial.world.i;

import org.jetbrains.annotations.Nullable;
import tripleo.elijah_durable_congenial.comp.functionality.f291.AmazingPart;
import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.stages.garish.GarishClass;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;

public interface LivingClass extends LivingNode {
	@Nullable IEvaClass evaNode();

	int getCode();

	ClassStatement getElement();

	GarishClass getGarish();

	void offer(AmazingPart aAmazingPart);

	//void setGarish(GarishClass aGarishClass);
}
