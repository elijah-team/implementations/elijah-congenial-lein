package tripleo.elijah_durable_congenial.world.i;

import org.jdeferred2.DoneCallback;
import tripleo.elijah_durable_congenial.comp.functionality.f291.AmazingPart;
import tripleo.elijah_durable_congenial.lang.i.FunctionDef;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;


public interface LivingFunction {
	int getCode();

	FunctionDef getElement();

	void offer(AmazingPart aAp);

	IBaseEvaFunction evaNode();

	void codeRegistration(LF_CodeRegistration acr);

	boolean isRegistered();

	void listenRegister(DoneCallback<Integer> aCodeCallback);
}
