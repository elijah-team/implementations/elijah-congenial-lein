package tripleo.elijah_durable_congenial.world.i;

import tripleo.elijah.util.Eventual;
import tripleo.elijah_congenial.pipelines.pipeline_logic.GN_PL_Run2;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.nextgen.inputtree.EIT_ModuleInput;
import tripleo.elijah_durable_congenial.stages.deduce.GeneratedClasses;

public interface WorldModule {
	OS_Module module();

	EIT_ModuleInput input();

	GN_PL_Run2.GenerateFunctionsRequest rq();

	Eventual<GeneratedClasses> getEventual();
}
