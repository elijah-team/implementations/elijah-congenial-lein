package tripleo.elijah_durable_congenial.world.i;

import tripleo.elijah.util.Eventual;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaFunction;

public interface LF_CodeRegistration {
	void accept(final IEvaFunction aEvaFunction, final Eventual<Integer> aCodeCallback);
}
