package tripleo.elijah_durable_congenial.world.i;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.util.CompletableProcess;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.lang.i.OS_Package;
import tripleo.elijah_durable_congenial.lang.i.Qualident;
import tripleo.elijah_durable_congenial.lang.impl.BaseFunctionDef;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaNamespace;
import tripleo.elijah_durable_congenial.stages.gen_generic.IREvaClass;
import tripleo.elijah_durable_congenial.world.impl.DefaultLivingClass;
import tripleo.elijah_durable_congenial.world.impl.DefaultLivingFunction;
import tripleo.elijah_durable_congenial.world.impl.DefaultLivingNamespace;

import java.util.Collection;
import java.util.List;

public interface LivingRepo {
	DefaultLivingClass addClass(IREvaClass aClass, Add addFlag);

	LivingClass addClass(ClassStatement cs);

	DefaultLivingFunction addFunction(@NotNull IBaseEvaFunction aFunction, Add aMainFunction);

	void addModule(OS_Module mod, String aFilename, final Compilation aC);

	LivingFunction addFunction(BaseFunctionDef fd);

	LivingPackage addPackage(OS_Package pk);

	//DefaultLivingClass addClass(EvaClass aClass, Add aMainClass);

	DefaultLivingNamespace addNamespace(IEvaNamespace aNamespace, Add aNone);

	LivingNamespace getNamespace(IEvaNamespace aEvaNamespace);

	LivingClass getClass(IEvaClass aEvaClass);

	OS_Package getPackage(String aPackageName);

	boolean hasPackage(String aPackageName);

	LivingFunction getFunction(IBaseEvaFunction aBaseEvaFunction);

	void addModuleProcess(CompletableProcess<WorldModule> wmcp);

	Collection<WorldModule> modules();

	void addModule2(WorldModule aMod1);

	@Nullable WorldModule getModule(OS_Module aModule);

	OS_Package makePackage(Qualident aPkgName);

	List<LivingClass> getClassesForClassStatement(ClassStatement cls);

	List<LivingClass> getClassesForClassNamed(String string);

	enum Add {MAIN_CLASS, MAIN_FUNCTION, NONE}
}
