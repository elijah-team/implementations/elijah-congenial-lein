package tripleo.elijah_durable_congenial.world.impl;

import org.jdeferred2.DoneCallback;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.util.Eventual;
import tripleo.elijah_durable_congenial.comp.functionality.f291.AmazingPart;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.lang.i.FunctionDef;
import tripleo.elijah_durable_congenial.stages.deduce.fluffy.i.FluffyComp;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaFunction;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaFunctionBase;
import tripleo.elijah_durable_congenial.world.i.LF_CodeRegistration;
import tripleo.elijah_durable_congenial.world.i.LivingFunction;

public class DefaultLivingFunction implements LivingFunction {
	private final @NotNull FunctionDef       _element;
	private final @NotNull IBaseEvaFunction  _gf;
	private                Eventual<Integer> codeCallback;
	private                boolean           __registered;

	public DefaultLivingFunction(final @NotNull IBaseEvaFunction aFunction) {
		_element = aFunction.getFD();
		_gf      = aFunction;
	}

	@Override
	public int getCode() {
		return _gf.getCode();
	}

	@Override
	public FunctionDef getElement() {
		return _element;
	}

	@Override
	public void offer(final AmazingPart aAp) {
		aAp.reverseOffer(this);
	}

	@Override
	public IBaseEvaFunction evaNode() {
		return _gf;
	}

	@Override
	public void codeRegistration(final LF_CodeRegistration acr) {
		if (codeCallback == null) {
			// 1. allocate
			codeCallback = new Eventual<>();

			// 2. initialize
			final Compilation compilation = _element.getContext().module().getCompilation();
			final FluffyComp  fluffy      = compilation.getFluffy();
			codeCallback.register(fluffy);

			// 3. setup
			codeCallback.then(i -> {
				final IEvaFunctionBase evaFunction = evaNode();
				evaFunction.setCode(i);
				__registered = true;
			});
		}

		// 4. trigger
		if (evaNode().getCode() == 0) {
			final IEvaFunction evaFunction = (IEvaFunction) evaNode();
			acr.accept(evaFunction, codeCallback);
		}
	}

	@Override
	public boolean isRegistered() {
		return __registered;
	}

	@Override
	public void listenRegister(final DoneCallback<Integer> aCodeCallback) {
		codeCallback.then(aCodeCallback);
	}
}
