package tripleo.elijah_durable_congenial.world.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah_durable_congenial.comp.functionality.f291.AmazingPart;
import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.stages.garish.GarishClass;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;
import tripleo.elijah_durable_congenial.stages.gen_generic.IREvaClass;
import tripleo.elijah_durable_congenial.world.i.LivingClass;

public class DefaultLivingClass implements LivingClass {
	private final @Nullable IEvaClass      _gc;
	private final @Nullable IREvaClass     _gcr;
	private final           ClassStatement _element;
	private @Nullable       GarishClass    _garish;

	public DefaultLivingClass(final @NotNull IEvaClass aClass) {
		_element = aClass.getKlass();
		_gc      = aClass;
		_garish  = null;
		_gcr     = null;
	}

	public DefaultLivingClass(final IREvaClass aClass) {
		_element = aClass.getKlass();

		final IEvaClass evaClass = aClass.getEvaClass();
		if (evaClass == null) {
			_gc  = evaClass;
			_gcr = null;
		} else {
			_gc  = evaClass;
			_gcr = aClass;
		}
		_garish = null;
	}

	@Override
	public @Nullable IEvaClass evaNode() {
		if (_gcr != null)
			return _gcr.getEvaClass();
		else return _gc;
	}

	@Override
	public int getCode() {
		assert _gcr != null; // this.evaNode() ??
		return _gcr.getCode();
	}

	@Override
	public ClassStatement getElement() {
		return _element;
	}

	@Override
	public @NotNull GarishClass getGarish() {
		if (_garish == null) {
			_garish = new GarishClass(this);
		}

		return _garish;
	}

	@Override
	public void offer(final AmazingPart amazingPart) {
		amazingPart.reverseOffer(this);
	}
}
