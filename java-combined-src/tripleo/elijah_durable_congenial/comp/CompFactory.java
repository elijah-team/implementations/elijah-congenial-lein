package tripleo.elijah_durable_congenial.comp;

import org.jetbrains.annotations.Nullable;
import tripleo.elijah.ci.LibraryStatementPart;
import tripleo.elijah.comp.Finally_Nameable;
import tripleo.elijah.comp.IInputRequest;
import tripleo.elijah.nextgen.inputtree.EIT_InputType;
import tripleo.elijah.util.Operation2;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.lang.i.Qualident;
import tripleo.elijah_durable_congenial.nextgen.inputtree.EIT_ModuleInput;
import tripleo.elijah_durable_congenial.world.i.WorldModule;

import java.io.File;
import java.util.List;

public interface CompFactory {
	EIT_ModuleInput createModuleInput(OS_Module aModule);

	Qualident createQualident(List<String> sl);

	IInputRequest createInputRequest(File aFile, final boolean aDo_out, final @Nullable LibraryStatementPart aLsp);

	WorldModule createWorldModule(OS_Module aM);

	class InputRequest implements IInputRequest {
		private final java.io.File            _file;
		private final boolean                 _do_out;
		private final LibraryStatementPart    lsp;
		private       Operation2<WorldModule> op;

		public InputRequest(final java.io.File aFile, final @Nullable LibraryStatementPart aLsp) {
			_file   = aFile;
			_do_out = false;
			lsp     = aLsp;
		}

		@Override
		public java.io.File file() {
			return _file;
		}

		@Override
		public boolean do_out() {
			return false;
		}

		@Override
		public LibraryStatementPart lsp() {
			return lsp;
		}

		@Override
		public Finally_Nameable getInput(final EIT_InputType aTy) {
			var _c = this;
			return new Finally_Nameable() {
				@Override
				public String getNameableName() {
					final File file = _c._file;
					return file.toString();
				}
			};
		}

		//@Override
		public void setOp(final Operation2<WorldModule> aOwm) {
			op = aOwm;
		}
	}
}
