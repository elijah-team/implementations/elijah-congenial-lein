package tripleo.elijah_durable_congenial.comp.nextgen;

import java.io.*;

public interface CP_SubFile {
	File toFile();
}
