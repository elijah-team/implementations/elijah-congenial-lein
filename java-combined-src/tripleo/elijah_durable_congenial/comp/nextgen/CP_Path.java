package tripleo.elijah_durable_congenial.comp.nextgen;

import org.jdeferred2.Promise;

import java.nio.file.Path;

public interface CP_Path {
	CP_SubFile subFile(String aFile);

	CP_Path child(String aPath0);

	Path getPath();

	Promise<Path, Void, Void> getPathPromise();

	java.io.File toFile();

	java.io.File getRootFile();

	CP_Path getParent();

	String getName();

	_CP_RootPath getRootPath();
}
