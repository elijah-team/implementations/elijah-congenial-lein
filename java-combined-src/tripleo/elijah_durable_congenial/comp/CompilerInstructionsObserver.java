package tripleo.elijah_durable_congenial.comp;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.util.NotImplementedException;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.internal.CSS_HasInstructions;
import tripleo.elijah_durable_congenial.comp.internal.CompilationImpl;

import java.util.ArrayList;
import java.util.List;

public class CompilerInstructionsObserver implements Observer<CompilerInstructions> {
	private final Compilation                compilation;
	private final List<CompilerInstructions> l = new ArrayList<>();

	public CompilerInstructionsObserver(final Compilation aCompilation) {
		compilation = aCompilation;
	}

	public void almostComplete() {
		System.err.println("2323 "+l.size());

		if (l.size() == 1) {
			compilation.pushItem(new CSS_HasInstructions(l.get(0)));
		} else {
			//throw new AssertionError();
			((CompilationImpl)compilation).NOCIS = true;
		}
	}

	@Override
	public void onSubscribe(@NonNull final Disposable d) {
		//Disposable x = d;
		//NotImplementedException.raise();
	}

	@Override
	public void onNext(@NonNull final CompilerInstructions aCompilerInstructions) {
		l.add(aCompilerInstructions);
	}

	@Override
	public void onError(@NonNull final Throwable e) {
		NotImplementedException.raise();
	}

	@Override
	public void onComplete() {
		throw new IllegalStateException("Error");
	}
}
