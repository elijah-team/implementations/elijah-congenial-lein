package tripleo.elijah_durable_congenial.comp;

import com.google.common.base.MoreObjects;
import tripleo.elijah.comp.Finally_Input;
import tripleo.elijah.comp.Finally_Nameable;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.comp.i.ILazyCompilerInstructions;
import tripleo.elijah.nextgen.inputtree.EIT_InputType;
import tripleo.elijah.util.*;

import java.io.File;

public class CompilerInput implements ICompilerInput {
	private final String                           inp;
	private       Maybe<ILazyCompilerInstructions> accept_ci;
	private       File                             dir_carrier;
	private       Ty                               ty;
	private       String                           hash;

	public CompilerInput(final String aS) {
		inp = aS;
		ty  = Ty.NULL;
	}

	@Override
	public String getInp() {
		return inp;
	}

	@Override
	public void accept_ci(final Maybe<ILazyCompilerInstructions> aMaybeCI) {
		accept_ci = aMaybeCI;

		if (aMaybeCI.isException()) {
			var exc1 = aMaybeCI.exc;
//			throw new UnintendedUseException("ok");
		} else {
			ILazyCompilerInstructions xx = aMaybeCI.o;
		}
	}

	@Override
	public Maybe<ILazyCompilerInstructions> acceptance_ci() {
		return accept_ci;
	}

	@Override
	public boolean isNull() {
		return ty == Ty.NULL;
	}

	@Override
	public boolean isSourceRoot() {
		return ty == Ty.SOURCE_ROOT;
	}

	@Override
	public void setSourceRoot() {
		ty = Ty.SOURCE_ROOT;
	}

	@Override
	public void setDirectory(File f) {
		ty          = Ty.SOURCE_ROOT;
		dir_carrier = f;
	}

	@Override
	public void setArg() {
		ty = Ty.ARG;
	}

	@Override
	public void accept_hash(final String hash) {
		this.hash = hash;
	}

	@Override
	public Finally_Input getInput(final EIT_InputType aTy) {
		// this.con().new... ??
		final Finally_Nameable finallyNameable = ICompilerInput.getFinallyNameable(this);
		return new Finally_Input(finallyNameable, aTy);
	}

	@Override
	public Finally_Nameable getNameable() {
		return ICompilerInput.getFinallyNameable(this); // ^^
	}

	@Override
	public String toString() {
		return MoreObjects.toStringHelper(this)
				.add("ty", ty)
				.add("inp", inp)
				.add("accept_ci", accept_ci.toString())
				.add("dir_carrier", dir_carrier)
				.add("hash", hash)
				.toString();
	}

	public Ty ty() {
		return ty;
	}

	public enum Ty {NULL, SOURCE_ROOT, ARG}
}
