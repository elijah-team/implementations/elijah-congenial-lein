package tripleo.elijah_durable_congenial.comp.functionality.f291;

import tripleo.elijah_durable_congenial.comp.i.ICompilationAccess2;
import tripleo.elijah_durable_congenial.stages.generate.OutputStrategyC;
import tripleo.elijah_durable_congenial.stages.write_stage.pipeline_impl.NG_OutputRequest;

import java.util.List;

public record WPIS_GenerateOutputsFinalization(
		List<NG_OutputRequest> outputRequestList,
		OutputStrategyC outputStrategyC,
		ICompilationAccess2 compilationAccess2) {
}
