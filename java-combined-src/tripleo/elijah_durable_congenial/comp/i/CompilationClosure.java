package tripleo.elijah_durable_congenial.comp.i;

import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah_durable_congenial.comp.IO;
import tripleo.elijah_durable_congenial.comp.nextgen.CP_Paths;
import tripleo.elijah_durable_congenial.nextgen.query.QueryDatabase;

import java.io.File;

public interface CompilationClosure {
	ErrSink errSink();

	IO io();

	// convenience
	default void pushItem(CompilerInstructions aCompilerInstructions) {
		getCompilation().pushItem(aCompilerInstructions);
	}

	Compilation getCompilation();

	// semi-convenience/utility
	File getFile(String aFileName, ICompilerInput aInput);

	// convenience
	QueryDatabase queryDb();

	CP_Paths paths();

	CompilationEnclosure getCompilationEnclosure();
}
