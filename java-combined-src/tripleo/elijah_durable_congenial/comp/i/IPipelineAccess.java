package tripleo.elijah_durable_congenial.comp.i;

import org.jdeferred2.DoneCallback;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.PipelineLogic;
import tripleo.elijah.comp.notation.GN_Env;
import tripleo.elijah.comp.notation.GN_Notable;
import tripleo.elijah.util.Eventual;
import tripleo.elijah_congenial.pipelines.eva.FunctionStatement;
import tripleo.elijah_durable_congenial.comp.AccessBus;
import tripleo.elijah_durable_congenial.comp.EvaPipeline;
import tripleo.elijah_durable_congenial.comp.WritePipeline;
import tripleo.elijah_durable_congenial.comp.internal.Provenance;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.nextgen.inputtree.EIT_ModuleList;
import tripleo.elijah_durable_congenial.nextgen.output.NG_OutputItem;
import tripleo.elijah_durable_congenial.stages.deduce.DeducePhase;
import tripleo.elijah_durable_congenial.stages.gen_c.GenerateC;
import tripleo.elijah_durable_congenial.stages.gen_fn.EvaNode;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaNamespace;
import tripleo.elijah_durable_congenial.stages.gen_generic.pipeline_impl.GenerateResultSink;
import tripleo.elijah_durable_congenial.stages.logging.ElLog;

import java.util.List;
import java.util.function.Consumer;

public interface IPipelineAccess {
	void addLog(ElLog aLOG);
	void addOutput(NG_OutputItem aO);

	void setNodeList(List<EvaNode> aEvaNodeList);
	void registerNodeList(DoneCallback<List<EvaNode>> done);

	void activeClass(IEvaClass aEvaClass);
	List<IEvaClass> getActiveClasses();

	void activeNamespace(IEvaNamespace aEvaNamespace);
	List<IEvaNamespace> getActiveNamespaces();

	List<IBaseEvaFunction> getActiveFunctions();
	void activeFunction(IBaseEvaFunction aEvaFunction);
	void addFunctionStatement(FunctionStatement aFunctionStatement);

	void _send_GeneratedClass(EvaNode aClass);

	void waitGenC(OS_Module mod, Consumer<GenerateC> aCb);

	void install_notate(Provenance aProvenance, Class<? extends GN_Notable> aRunClass, Class<? extends GN_Env> aEnvClass);

	void notate(Provenance provenance, GN_Notable aNotable);
	void notate(Provenance aProvenance, GN_Env aPlRun2);
	DeducePhase getDeducePhase();

	EIT_ModuleList getModuleList();

	void _ModuleList_add(OS_Module aM);


	void resolvePipelinePromise(PipelineLogic aPipelineLogic);

	void setEvaPipeline(@NotNull EvaPipeline agp);

	void _setAccessBus(AccessBus ab);

	@NotNull Eventual<EvaPipeline> getEvaPipelinePromise();

	@NotNull Eventual<PipelineLogic> getPipelineLogicPromise();

	AccessBus getAccessBus();

	Compilation getCompilation();

	CompilationEnclosure getCompilationEnclosure();

	GenerateResultSink getGenerateResultSink();

	void setGenerateResultSink(GenerateResultSink aGenerateResultSink);

	WritePipeline getWritePipeline();

	void setWritePipeline(WritePipeline aWritePipeline);

	EIT_ModuleList mods(); // line 54
	//List<NG_OutputItem> getOutputs();
}
