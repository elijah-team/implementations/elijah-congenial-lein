package tripleo.elijah_durable_congenial.comp.i;

import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.*;
import io.reactivex.rxjava3.subjects.*;
import lombok.*;
import org.jetbrains.annotations.*;
import tripleo.elijah.comp.*;
import tripleo.elijah.comp.i.*;
import tripleo.elijah.nextgen.*;
import tripleo.elijah.nextgen.outputstatement.*;
import tripleo.elijah.nextgen.reactive.*;
import tripleo.elijah.util.*;
import tripleo.elijah_congenial.pipelines.*;
import tripleo.elijah_durable_congenial.comp.*;
import tripleo.elijah_durable_congenial.comp.internal.*;
import tripleo.elijah_durable_congenial.comp.nextgen.*;
import tripleo.elijah_durable_congenial.comp.nextgen.i.*;
import tripleo.elijah_durable_congenial.comp.notation.*;
import tripleo.elijah_durable_congenial.lang.i.*;
import tripleo.elijah_durable_congenial.nextgen.outputtree.*;
import tripleo.elijah_durable_congenial.nextgen.spi.*;
import tripleo.elijah_durable_congenial.pre_world.*;
import tripleo.elijah_durable_congenial.stages.gen_fn.*;
import tripleo.elijah_durable_congenial.stages.logging.*;
import tripleo.elijah_durable_congenial.world.i.*;

import java.nio.file.*;
import java.util.*;

import static tripleo.elijah_durable_congenial.util.Helpers.*;

public class CompilationEnclosure {
	public final List<ElLog> elLogs = new LinkedList<>();

	public final  Eventual<IPipelineAccess> pipelineAccessPromise = new Eventual<>();
	private final CB_Output                                   _cbOutput             = new CB_Output();
	private final Compilation                                 compilation;
	private final Eventual<AccessBus>                         accessBusPromise      = new Eventual<>();
	private final Subject<ReactiveDimension>                  dimensionSubject      = ReplaySubject.<ReactiveDimension>create();
	private final Subject<Reactivable>                        reactivableSubject    = ReplaySubject.<Reactivable>create();
	private final List<ModuleListener>                        _moduleListeners      = new ArrayList<>();
	private AccessBus           ab;
	private ICompilationAccess  ca;
	private ICompilationBus      compilationBus;
	private CompilationRunner    compilationRunner;
	private List<ICompilerInput> inp;
	private IPipelineAccess      pa;
	private PipelineLogic       pipelineLogic;
	private NextgenFactory      _nextgenFactory;


	private MalBulge _mb;

	public CompilationEnclosure(final Compilation aCompilation) {
		compilation = aCompilation;

		final CompilationEnclosure _ce = this;

		getPipelineAccessPromise().then(pa -> {
			ab = new AccessBus(getCompilation(), pa);

			accessBusPromise.resolve(ab);

			ab.addPipelinePlugin(new __Plugins.HooliganPipelinePlugin());
			ab.addPipelinePlugin(new __Plugins.EvaPipelinePlugin());
			ab.addPipelinePlugin(new __Plugins.DeducePipelinePlugin());
			ab.addPipelinePlugin(new __Plugins.WritePipelinePlugin());
			ab.addPipelinePlugin(new __Plugins.WriteMakefilePipelinePlugin());
			ab.addPipelinePlugin(new __Plugins.WriteMesonPipelinePlugin());
			ab.addPipelinePlugin(new __Plugins.WriteOutputTreePipelinePlugin());

			pa._setAccessBus(ab);

			//_ce.provide(pa);
		});
	}

	@Contract(pure = true)
	public Eventual<IPipelineAccess> getPipelineAccessPromise() {
		return pipelineAccessPromise;
	}

	@Contract(pure = true)
	public Compilation getCompilation() {
		return compilation;
	}

	public void addReactive(@NotNull Reactive r) {
		dimensionSubject.subscribe(new Observer<ReactiveDimension>() {
			@Override
			public void onSubscribe(@NonNull final Disposable d) {

			}

			@Override
			public void onNext(@NonNull ReactiveDimension dim) {
				r.join(dim);
			}

			@Override
			public void onError(@NonNull final @NotNull Throwable e) {
				e.printStackTrace();
			}

			@Override
			public void onComplete() {

			}
		});
	}

	public @NotNull Eventual<AccessBus> getAccessBusPromise() {
		return accessBusPromise;
	}

	@Contract(pure = true)
	public CB_Output getCB_Output() {
		return this._cbOutput;
	}

	@Contract(pure = true)
	public @NotNull ICompilationAccess getCompilationAccess() {
		return ca;
	}

	public void setCompilationAccess(@NotNull ICompilationAccess aca) {
		ca = aca;
	}

	//@Contract(pure = true) //??
	public CompilationClosure getCompilationClosure() {
		return this.getCompilation().getCompilationClosure();
	}

	@Contract(pure = true)
	public CompilerDriver getCompilationDriver() {
		return getCompilationBus().getCompilationDriver();
	}

	@Contract(pure = true)
	public ICompilationBus getCompilationBus() {
		return compilationBus;
	}

	public void setCompilationBus(final ICompilationBus aAcb) {
		this.compilationBus = aAcb;
		this.compilationBus.start();
	}

	@Contract(pure = true)
	public CompilationRunner getCompilationRunner() {
		return compilationRunner;
	}

	public void setCompilationRunner(final CompilationRunner aCr) {
		compilationRunner = aCr;
	}

	@Contract(pure = true)
	public List<ICompilerInput> getCompilerInput() {
		return inp;
	}

	public void setCompilerInput(final List<ICompilerInput> aInputs) {
		inp = aInputs;
	}

	@Contract(pure = true)
	public IPipelineAccess getPipelineAccess() {
		return pa;
	}

	@Contract(pure = true)
	public PipelineLogic getPipelineLogic() {
		return pipelineLogic;
	}

	public void setPipelineLogic(final PipelineLogic aPipelineLogic) {
		pipelineLogic = aPipelineLogic;

		getPipelineAccessPromise().then(pa -> pa.resolvePipelinePromise(aPipelineLogic));
	}

	public @NotNull void addModuleThing(final OS_Module aMod) {
		System.err.println("9998-0336 addModuleThing "+aMod.getFileName());
	}

	public void noteAccept(final @NotNull WorldModule aWorldModule) {
		var mod = aWorldModule.module();

		System.err.println("9998-0323 " + mod);
		System.err.println("9998-0324 " + mod.getFileName());
	}

	public void reactiveJoin(final Reactive aReactive) {
		// throw new IllegalStateException("Error");

		// aReactive.join();
		System.err.println("reactiveJoin " + aReactive.toString());
	}

	public void addModuleListener(final ModuleListener aModuleListener) {
		_moduleListeners.add(aModuleListener);
	}

	public void addModule(final WorldModule aWorldModule) {
		// TODO Reactive pattern (aka something ala ReplaySubject)
		for (final ModuleListener moduleListener : _moduleListeners) {
			moduleListener.listen(aWorldModule);
		}
	}

	public void addEntryPoint(final @NotNull Mirror_EntryPoint aMirrorEntryPoint, final IClassGenerator dcg) {
		aMirrorEntryPoint.generate(dcg);
	}

	public void spi(final Object spiable) {
		if (spiable instanceof SPI_Loggable) {
			addLog(((SPI_Loggable) spiable).spiGetLog());
		}
		if (spiable instanceof SPI_ReactiveDimension) {
			addReactiveDimension(((SPI_ReactiveDimension) spiable).spiGetReactiveDimension());
		}
	}

	//public void spi(final Object spiable, final Object cacheUnderKey) {
	//	throw new NotImplementedException("12/31/23");
	//}

	public void addLog(final ElLog aLOG) {
		elLogs.add(aLOG);
	}

	public void addReactiveDimension(final ReactiveDimension aReactiveDimension) {
		dimensionSubject.onNext(aReactiveDimension);

		reactivableSubject.subscribe(new Observer<Reactivable>() {
			@Override
			public void onSubscribe(@NonNull final Disposable d) {

			}

			@Override
			public void onNext(@NonNull final @NotNull Reactivable aReactivable) {
				addReactive(aReactivable);
			}

			@Override
			public void onError(@NonNull final @NotNull Throwable e) {
				e.printStackTrace();
			}

			@Override
			public void onComplete() {

			}
		});

//		aReactiveDimension.setReactiveSink(addReactive);
	}

	public void addReactive(@NotNull Reactivable r) {
		int y = 2;
		reactivableSubject.onNext(r);

		dimensionSubject.subscribe(new Observer<ReactiveDimension>() {
			@Override
			public void onSubscribe(@NonNull final Disposable d) {

			}

			@Override
			public void onNext(final ReactiveDimension aReactiveDimension) {
				// r.join(aReactiveDimension);
				r.respondTo(aReactiveDimension);
			}

			@Override
			public void onError(@NonNull final @NotNull Throwable e) {
				e.printStackTrace();
			}

			@Override
			public void onComplete() {

			}
		});
	}

	public ICompilationAccess2 ca2() {
		return compilation.getCompilationAccess2();
	}

	public NextgenFactory nextgenFactory() {
		if (_nextgenFactory == null) {
			_nextgenFactory = new NextgenFactory() {
				@Override
				public ER_Node createERNode(final CP_Path aPath, final EG_Statement aSeq) {
					/**
					 * See {@link CompOutput#writeToPath(CE_Path, EG_Statement)}
					 */
					return new ER_Node() {
						@Override
						public @NotNull String toString() {
							return "17 ER_Node " + aPath.toFile();
						}

						@Override
						public Path getPath() {
							final Path pp = aPath.getPath();
							return pp;
						}

						@Override
						public EG_Statement getStatement() {
							return aSeq;
						}
					};
				}
			};
		}
		return _nextgenFactory;
	}

	public void __addLogs(final @NotNull List<EOT_OutputFile> l) {
		final List<ElLog> logs = elLogs;
		final String      s1   = logs.get(0).getFileName();

		for (final ElLog log : logs) {
			final List<EG_Statement> stmts = new ArrayList<>();

			if (log.getEntries().isEmpty()) continue; // FIXME 24j1 Prelude.elijjah "fails" here

			for (final LogEntry entry : log.getEntries()) {
				final String logentry = String.format("[%s] [%tD %tT] %s %s", s1, entry.time, entry.time, entry.level, entry.message);
				stmts.add(new EG_SingleStatement(logentry + "\n"));
			}

			final EG_SequenceStatement seq      = new EG_SequenceStatement(new EG_Naming("wot.log.seq"), stmts); // <- ??
			final String               fileName = log.getFileName().replace("/", "~~");
			final EOT_OutputFile       off      = new EOT_OutputFile(List_of(), fileName, EOT_OutputType.LOGS, seq);
			l.add(off);
		}
	}

	public void provide(final IPipelineAccess aPipelineAccess) {
		this.pa = aPipelineAccess;
		getCompilation().set_pa(aPipelineAccess);
	}

	public void writeLogs() {
		final IPipelineAccess xpa      = compilation.pa();
		final GN_WriteLogs    aNotable = new GN_WriteLogs(ca, elLogs);

		assert pa == xpa;

		xpa.notate(Provenance.DefaultCompilationAccess__writeLogs, aNotable);
	}

	public MalBulge getMalBulge() {
		return this._mb;
	}

	public void setMalBulge(MalBulge mb) {
		this._mb = mb;
	}

	public interface ModuleListener {
		void listen(WorldModule module);

		void close();
	}
}
