package tripleo.elijah_durable_congenial.comp.i;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Operation;
import tripleo.elijah_durable_congenial.comp.internal.CR_State;

public interface CD_FindStdLib extends CompilerDriven {
	void findStdLib(@NotNull CR_State crState,
	                @NotNull String aPreludeName,
	                @NotNull Eventual<Operation<CompilerInstructions>> coci);
}
