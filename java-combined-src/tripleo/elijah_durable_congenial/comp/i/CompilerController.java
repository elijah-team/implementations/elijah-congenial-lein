package tripleo.elijah_durable_congenial.comp.i;

import tripleo.elijah.comp.i.ICompilerInput;

import java.util.List;

public interface CompilerController {
	void _setInputs(Compilation aCompilation, List<ICompilerInput> aInputs);

	void printUsage();

	void processOptions();

	void runner();
}
