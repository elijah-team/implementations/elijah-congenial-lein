package tripleo.elijah_durable_congenial.comp.i;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.nextgen.outputstatement.EG_Statement;
import tripleo.elijah.util.Ok;
import tripleo.elijah.util.Operation;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.nextgen.inputtree.EIT_Input;
import tripleo.elijah_durable_congenial.nextgen.outputtree.*;
import tripleo.elijah_durable_congenial.world.i.LivingRepo;
import tripleo.elijah_durable_congenial.world.i.WorldModule;

import java.util.List;

public interface ICompilationAccess2 {
	LivingRepo world();

	EOT_OutputTree getOutputTree();

	void addCodeOutput(EOT_FileNameProvider aFileNameProvider, EOT_OutputFile aOutputFile);

	void addCodeOutput(EOT_FileNameProvider aFilename, EOT_OutputFile aOutputFile, boolean addFlag);

	WorldModule createWorldModule(OS_Module aMod);

	EOT_OutputFile createOutputFile(List<EIT_Input> aInputs,
									EOT_FileNameProvider aFilename,
									EOT_OutputType aEOTOutputType,
									EG_Statement aStatement);

	@NotNull Operation<Ok> mal_ReadEval(String string);

	EOT_OutputFileCreator createOutputFile2(EOT_OutputType aOutputType);
}
