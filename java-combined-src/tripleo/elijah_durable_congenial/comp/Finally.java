package tripleo.elijah_durable_congenial.comp;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.Finally_Input;
import tripleo.elijah.comp.IInputRequest;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.nextgen.inputtree.EIT_InputType;
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;
import tripleo.elijah_durable_congenial.nextgen.outputtree.EOT_FileNameProvider;
import tripleo.elijah_durable_congenial.nextgen.outputtree.EOT_OutputFile;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

public class Finally {
	@SuppressWarnings("TypeMayBeWeakened")
	private final Set<Outs>            outputOffs = new HashSet<>();
	private final List<Finally_Input>  inputs     = new ArrayList<>();
	private final List<Finally_Output> outputs    = new ArrayList<>();
	@SuppressWarnings("BooleanVariableAlwaysNegated")
	private       boolean              turnAllOutputOff;

	public void turnOutputOff(final Outs aOut) {
		outputOffs.add(aOut);
	}

	public void addInput(final ICompilerInput aInp, final EIT_InputType ty) {
		inputs.add(aInp.getInput(ty));
	}

	public void turnAllOutputOff() {
		turnAllOutputOff = true;
	}

	public void addInput(final IInputRequest aInp, final EIT_InputType ty) {
		inputs.add(new Finally_Input(aInp, ty));
	}

	public boolean containsCodeOutput(@NotNull final String s) {
		return outputs.stream().anyMatch(i -> i.name().equals(s));
	}

	public void addCodeOutput(final EOT_FileNameProvider aFileNameProvider, final EOT_OutputFile aOff) {
		outputs.add(new Finally_Output(aFileNameProvider, aOff));
	}

	public int codeOutputSize() {
		return outputs.size();
	}

	public int codeInputSize() {
		return inputs.size();
	}

	public boolean containsCodeInput(final String aS) {
		return containsInput(aS);
	}

	public boolean containsInput(final String aS) {
		return inputs.stream().anyMatch(i -> i.name().equals(aS));
	}

	public List<String> codeOutputs() {
		return this.outputs.stream()
				.map(o -> o.fileNameProvider.getFilename())
				.toList();
	}

	public void makeOutput(final Outs aOuts, final Supplier<String> ass) {
		if (outputOn(aOuts)) {
			SimplePrintLoggerToRemoveSoon.println_err_Finally(ass.get());
		}
	}

	public boolean outputOn(final Outs aOuts) {
		return !turnAllOutputOff && !outputOffs.contains(aOuts);
	}

	public enum Outs {Out_6262, Out_727, Out_350, Out_364, Out_252, Out_2121, Out_486, Out_5757, Out_1069, Out_141, Out_EVTE_159, Out_353, Out_120, Out_40, Out_153, Out_600142, Out_6011189, Out_401b}

	//public enum Out2 {
	//	EZ, ELIJAH
	//}
}
