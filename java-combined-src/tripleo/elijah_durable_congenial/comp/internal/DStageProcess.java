package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.Contract;
import tripleo.elijah_durable_congenial.comp.Stages;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.i.ICompilationAccess;
import tripleo.elijah_durable_congenial.comp.i.RuntimeProcess;

public class DStageProcess implements RuntimeProcess {
	private final ICompilationAccess ca;

	@Contract(pure = true)
	public DStageProcess(final ICompilationAccess aCa) {
		ca = aCa;
	}

	@Override
	public void postProcess() {
	}

	@Override
	public void prepare() {
		assert ca.getStage() == Stages.D;
	}

	@Override
	public void run(final Compilation aComp, final CR_State st, final CB_Output output) {

	}
}
