package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.stateful.DefaultStateful;
import tripleo.elijah.stateful.State;
import tripleo.elijah.stateful.StateRegistrationToken;
import tripleo.elijah.stateful._RegistrationTarget;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Operation;
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;
import tripleo.elijah_congenial.startup.CongenialStartup;
import tripleo.elijah_durable_congenial.comp.i.CCI;
import tripleo.elijah_durable_congenial.comp.i.CR_Action;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.i.CompilationEnclosure;
import tripleo.elijah_durable_congenial.comp.i.ICompilationAccess;
import tripleo.elijah_durable_congenial.comp.i.ICompilationBus;
import tripleo.elijah_durable_congenial.comp.i.IPipelineAccess;
import tripleo.elijah_durable_congenial.comp.i.IProgressSink;

import java.util.concurrent.Executors;

public class CompilationRunner extends _RegistrationTarget {
	public          ICompilationBus cb;
	public          CR_State        crState;
	public @NotNull IProgressSink   progressSink;
	CIS cis;
	CR_FindCIs cr_find_cis;
	private          Compilation _compilation;
	private @NotNull CCI         cci;
	private          EzM         ezm;
	private CB_StartCompilationRunnerAction startAction;

	public CompilationRunner(final @NotNull ICompilationAccess aca, final CR_State aCrState) {
		CongenialStartup.getPromise(ICompilationAccess.class).resolve(aca);
		CongenialStartup.getPromise(CR_State.class).resolve(aCrState);

		var cb0 = aca.getCompilation().getCompilationEnclosure().getCompilationBus();
		CongenialStartup.getPromise(ICompilationBus.class).resolve(cb0);

		xyz();
	}

	private void xyz() {
		final int[]     i       = {0};
		final boolean[] timeout = {false};

		var paca = CongenialStartup.getPromise(ICompilationAccess.class);
		paca.then(ig -> i[0]++);
		var pcrs = CongenialStartup.getPromise(CR_State.class);
		pcrs.then(ig -> i[0]++);
		var pcbs = CongenialStartup.getPromise(ICompilationBus.class);
		pcbs.then(ig -> i[0]++);

		while (!timeout[0] && i[0] < 3) {
			var es = Executors.newFixedThreadPool(2);
			es.submit(new Runnable() {
				@Override
				public void run() {
					// wait a reasonable maximum of time for everything
					// to start the ball rolling
					// then signal to start another ball
					// or continue as normal
					try {
						//noinspection MagicNumber
						Thread.sleep(10_000); // large b/c i like to step through
						timeout[0] = true;
					} catch (InterruptedException aE) {
						throw new RuntimeException(aE);
					}
				}
			});
		}

		assert i[0] == 3;

		{

			paca.then(aca -> {
				_compilation = aca.getCompilation();
				_compilation.getCompilationEnclosure().setCompilationAccess(aca);
				cis = _compilation._cis();
				ezm = new EzM(_compilation.getCompilationClosure());
			});

			pcbs.then(acb -> {
				_compilation.getCompilationEnclosure().setCompilationBus((ICompilationBus) acb);
				cb           = acb;
				progressSink = cb.defaultProgressSink();
				cci          = new DefaultCCI(_compilation, cis, progressSink);
			});

			pcrs.then(acrs -> {
				crState = acrs;
			});
		}

		ST.register(this);
	}

	//public CompilationRunner(final CongenialStartup cst) {
	//	xyz();
	//}
	//
	//public CompilationRunner(final @NotNull ICompilationAccess aca, final CR_State aCrState, final Supplier<CompilationBus> scb) {
	//	CongenialStartup.getPromise(ICompilationAccess.class).resolve(aca);
	//	CongenialStartup.getPromise(CR_State.class).resolve(aCrState);
	//	CongenialStartup.getPromise(ICompilationBus.class).resolve(scb.get());
	//
	//	xyz();
	//}

	public void logProgress(final int number, final String text) {
		if (number == 130) return;

		SimplePrintLoggerToRemoveSoon.println_err_3("%d %s".formatted(number, text));
	}

	public @NotNull Operation<CompilerInstructions> parseEzFile(final @NotNull SourceFileParserParams p) {
		final Operation<CompilerInstructions> oci = ezm.parseEzFile1(p);
		assert oci != null;

		_compilation.getInputTree().setNodeOperation(p.input(), oci);

		return oci;
	}

	public @NotNull Eventual<Operation<CompilerInstructions>> realParseEzFile(final @NotNull SourceFileParserParams p) {
		ezm.realParseEzFile(p);
		p.eoci().then(ao -> _compilation.getInputTree().setNodeOperation(p.input(), ao));
		return p.eoci();
	}

	public CR_Action cr_find_cis() {
		if (this.cr_find_cis == null) { // lunacy
			var beginning = _accessCompilation().beginning(this);
			this.cr_find_cis = new CR_FindCIs(beginning);
		}
		return this.cr_find_cis;
	}

	public Compilation _accessCompilation() {
		return _compilation;
	}

	public CompilationEnclosure getCompilationEnclosure() {
		return this._compilation.getCompilationEnclosure();
	}

	public void start(final CompilerInstructions ci, final @NotNull IPipelineAccess pa) {
		var ce = pa.getCompilationEnclosure();
		var mb = new MalBulge(ce);

		ce.setMalBulge(mb);


		// FIXME only run once 06/16
		if (startAction == null) {
			startAction = new CB_StartCompilationRunnerAction(this, pa, ci);
			// FIXME CompilerDriven vs Process ('steps' matches "CK", so...)
			cb.add(startAction.cb_Process());
		}
	}

	public enum ST {
		;

		public static State EXIT_CONVERT_USER_TYPES;
		public static State EXIT_RESOLVE;
		public static State INITIAL;

		public static void register(final @NotNull _RegistrationTarget art) {
			//EXIT_RESOLVE            = registerState(new ST.ExitResolveState());
			INITIAL = art.registerState(new ST.InitialState());
		}

		static class InitialState implements State {
			private StateRegistrationToken identity;

			@Override
			public void apply(final DefaultStateful element) {

			}

			@Override
			public boolean checkState(final DefaultStateful aElement3) {
				return true;
			}

			@Override
			public void setIdentity(final StateRegistrationToken aId) {
				identity = aId;
			}
		}
	}
}
