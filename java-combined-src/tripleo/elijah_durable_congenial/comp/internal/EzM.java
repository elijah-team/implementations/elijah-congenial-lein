package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.comp.diagnostic.ExceptionDiagnostic;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.EventualExtract;
import tripleo.elijah.util.Operation;
import tripleo.elijah.util.SimplePrintLoggerToRemoveSoon;
import tripleo.elijah_durable_congenial.comp.Finally;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;
import tripleo.elijah_durable_congenial.comp.i.ErrSink;
import tripleo.elijah_durable_congenial.comp.queries.CL_CompilerInstructions;
import tripleo.elijah_durable_congenial.comp.queries.QueryEzFileToModule;
import tripleo.elijah_durable_congenial.comp.queries.QueryEzFileToModuleParams;
import tripleo.elijah_durable_congenial.util.Helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static tripleo.elijah.util.Mode.SUCCESS;

class EzM {
	private final CompilationClosure cc;

	EzM(final CompilationClosure aC) {
		cc = aC;
	}

	@NotNull Operation<CompilerInstructions> parseEzFile1(final @NotNull SourceFileParserParams p) {
		final File   f            = p.f();
		final String absolutePath = f.getAbsolutePath();

		logProgress(27, "   " + absolutePath);

		if (!f.exists()) {
			final ErrSink errSink = p.cc().errSink();
			errSink.reportError("File doesn't exist " + absolutePath);
			return Operation.failure(new FileNotFoundException());
		} else {
			realParseEzFile(p);
			var t = EventualExtract.of(p.eoci());
			return t;
		}
	}

	private void logProgress(final int code, final String message) {
		final String x = "%d %s".formatted(code, message);
		System.out.println(x);
	}

	@NotNull void realParseEzFile(final @NotNull SourceFileParserParams p) {
		final String f    = p.file_name();
		final File   file = p.f();

		try {
			final InputStream s = p.cc().io().readFile(tripleo.wrap.File.wrap(file));

			final Eventual<Operation<CompilerInstructions>> eoci = p.eoci();//new Eventual<>(); // another is created elsewhere, (Java/C-ism)

			realParseEzFile(f, s, file, p.cc().getCompilation(), eoci);

			eoci.then(oci -> {
				if (oci.mode() == SUCCESS) {
					Operation<String> hash = Helpers.getHashForFilename(p.f().toString());
					//System.err.println("***** 166 " + hash.success());

					final ICompilerInput input = p.input();

					// FIXME stdlib.ez will not get it's hash for example 07/03
					if (input != null) {
						input.accept_hash(hash.success());
					} else {
						//assert false;
						final Compilation c = p.cc().getCompilation();
						if (c.reports().outputOn(Finally.Outs.Out_6262)) {
							System.err.println("***** 6262 " + f);
						}
					}
				}
			});
		} catch (FileNotFoundException aE) {
			//return Operation.failure(aE);
			assert false;
		}
	}

	@NotNull void realParseEzFile(final String f,
	                              final @Nullable InputStream s,
	                              final @NotNull File file,
	                              final @NotNull Compilation c,
	                              final Eventual<Operation<CompilerInstructions>> eeee) {
		final String absolutePath;
		try {
			absolutePath = file.getCanonicalFile().toString(); // TODO 04/10 hash this and "attach"
			//queryDB.attach(compilerInput, new EzFileIdentity_Sha256($hash)); // ??
		} catch (IOException aE) {
			eeee.fail(aE);
			return;
		}

		// TODO 04/10
		// Cache<CompilerInput, CompilerInstructions> fn2ci /*EzFileIdentity??*/(MAP/*??*/, resolver is try stmt)
		if (c.fn2ci().containsKey(absolutePath)) { // don't parse twice
			// TODO 04/10
			// ...queryDB.attach(compilerInput, new EzFileIdentity_Sha256($hash)); // ?? fnci
			final Operation<CompilerInstructions> success = Operation.success(c.fn2ci().get(absolutePath));
			eeee.resolve(success);
			return;
		}

		try {
			//final var ecio1 = this.cc.queryDb().simpleQuery(QueryEzFileToModuleParams.class, f, s);//parseEzFile_(f, s);
			final var ecio1 = parseEzFile_(f, s);
			final var ecio = ecio1.getOp();
			ecio.then((Operation<CompilerInstructions> cio) -> {
				if (cio.mode() != SUCCESS) {
					final Exception e = cio.failure();
					assert e != null;

					SimplePrintLoggerToRemoveSoon.println_err_2(("parser exception: " + e));
					e.printStackTrace(System.err);
					//s.close();
					eeee.resolve(cio);
				}

				final CompilerInstructions R = cio.success();
				R.setFilename(file.toString());
				c.fn2ci().put(absolutePath, R);

				eeee.resolve(cio);
			});
			ecio.onFail(failure -> {
				final Exception e = ((ExceptionDiagnostic) failure).getException();
				assert e != null;

				SimplePrintLoggerToRemoveSoon.println_err_2(("parser exception: " + e));
				e.printStackTrace(System.err);
				//s.close();
				eeee.fail(failure);
			});
		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (IOException aE) {
					// TODO promise inside finally: is *this* ok??
					eeee.fail(aE);
				}
			}
		}
	}

	private CL_CompilerInstructions parseEzFile_(final String f, final InputStream s) {
		final QueryEzFileToModuleParams qp = new QueryEzFileToModuleParams(f, s);
		var                             c  = new QueryEzFileToModule(this.cc.queryDb(), qp).calculate();
		return c;
	}
}
