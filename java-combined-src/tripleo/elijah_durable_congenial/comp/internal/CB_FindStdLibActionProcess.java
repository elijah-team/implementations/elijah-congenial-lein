package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Mode;
import tripleo.elijah.util.Ok;
import tripleo.elijah.util.Operation;
import tripleo.elijah_durable_congenial.comp.i.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CB_FindStdLibActionProcess implements CB_Action {
	private final SingleActionProcess _process;
	private final CB_Action           _action;

	private final List<CB_OutputString> o = new ArrayList<>();

	public CB_FindStdLibActionProcess(final CompilationEnclosure aCe, final CR_State aCrState) {
		this._action  = new CB_FindStdLibAction(aCe, aCrState);
		this._process = new SingleActionProcess(_action);
	}

	@Override
	public void execute() {
		_process.a.execute();
	}

	@Override
	public String name() {
		return "CB_FindStdLibActionProcess";
	}

	@Override
	public @NotNull List<CB_OutputString> outputStrings() {
		return o;
	}

	public class CB_FindStdLibAction implements CB_Action {
		private final CompilationEnclosure ce;
		private final     CR_State                  crState;
		private final     Operation<CompilerDriven> op;
		private final     List<CB_OutputString>     o = new ArrayList<>(); // FIXME 07/01 how is this modified?
		//private final Object               op;
		private @Nullable CD_FindStdLib             findStdLib;

		public CB_FindStdLibAction(final CompilationEnclosure aCe, final CR_State aCrState) {
			ce      = aCe;
			crState = aCrState;

			//findStdLib =
			op = obtain();
			logProgress(Prov.obtain, op);
		}

		private Operation<CompilerDriven> obtain() {
			var x = ce.getCompilationDriver().get(Compilation.CompilationAlways.Tokens.COMPILATION_RUNNER_FIND_STDLIB2);
			if (x.mode() == Mode.SUCCESS) {
				this.findStdLib = (CD_FindStdLib) x.success();
			}
			return x;
		}

		public void logProgress(Prov code, @Nullable Object o) {
//		val name = name()
			switch (code) {
			case find_stdlib -> {
				assert o != null;
				//noinspection unchecked
				var op = (Operation<Ok>) o;
				var text = op.toString();
				final CB_OutputString os = new COutputString(text);
				Objects.requireNonNull(outputStrings()).add(os);
			}
			case get_push_item -> {
				assert o != null;
				//noinspection unchecked
				//var op = (Operation<CompilerInstructions>) o;
				//op;
			}
			case obtain -> {
				//noinspection unchecked
				var op = (Operation<CompilerDriven>)o; //Operation<CompilerInstructions>
				assert op != null;
				var text = op.toString();
				final  CB_OutputString os  = new COutputString(text);
				Objects.requireNonNull(outputStrings()).add(os);
			}
			case execute_end -> {
				// TODO 23/11/12 say something
			}
			case execute_begin -> {
				// TODO 23/11/12 say something
			}
			}
		}

		@Override
		public void execute() {
			logProgress(Prov.execute_begin, null);

			var                                     preludeName = Compilation.CompilationAlways.defaultPrelude();
			if (findStdLib != null) {
				final Eventual<Operation<CompilerInstructions>> eventual = new Eventual<>();
				findStdLib.findStdLib(crState, preludeName, eventual);

				eventual.then((Operation<CompilerInstructions> op) -> { // tri state??
					logProgress(Prov.find_stdlib, op);
					switch (op.mode()) {
					case SUCCESS, FAILURE -> {}//xop[0] = op;
					default -> throw new IllegalStateException("Unexpected value: " + op.mode());
					}
				});
				logProgress(Prov.execute_end, null);

			}
		}

		@Override
		public String name() {
			return "find std lib";
		}

		@Override
		public @Nullable List<CB_OutputString> outputStrings() {
			return o;
		}

		private void getPushItem(Operation<CompilerInstructions> oci) {
			logProgress(Prov.get_push_item, oci);

			if (oci.mode() == Mode.SUCCESS) {
				var c = ce.getCompilation();

				// README 09/09 not double
				//c.pushItem(oci.success());
				c.use(oci.success(), true);
			} else {
				throw new IllegalStateException(oci.failure());
			}
		}

		public enum Prov {
			obtain,
			find_stdlib,
			get_push_item,
			execute_end,
			execute_begin
		}
	}
}
