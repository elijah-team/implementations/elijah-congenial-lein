package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Mode;
import tripleo.elijah.util.Operation;
import tripleo.elijah_congenial.startup.SourceFileReasoning;
import tripleo.elijah_durable_congenial.comp.Finally;
import tripleo.elijah_durable_congenial.comp.i.CD_FindStdLib;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;
import tripleo.elijah_durable_congenial.comp.queries.QuerySourceFileParser;

import java.util.Objects;

public class CD_FindStdLibImpl implements CD_FindStdLib {
	@Override
	public void findStdLib(final @NotNull CR_State crState,
	                       final @NotNull String aPreludeName,
	                       final @NotNull Eventual<Operation<CompilerInstructions>> coci) {
		try {
			final CompilationRunner compilationRunner = crState.runner();

			final Eventual<Operation<CompilerInstructions>> eoci = _____findStdLib(aPreludeName, compilationRunner._accessCompilation().getCompilationClosure(), compilationRunner);
			eoci.then(coci::resolve);
			eoci.onFail(coci::fail);
			assert eoci.isResolved();
			return;
		} catch (Exception aE) {
			coci.fail(aE);
		}
	}

	public Eventual<Operation<CompilerInstructions>> _____findStdLib(
			final @NotNull String prelude_name,
			final @NotNull CompilationClosure cc,
			final @NotNull CompilationRunner cr) {
		var slr = cc.paths().stdlibRoot();
		var pl  = slr.child("lib-" + prelude_name);
		var sle = pl.child("stdlib.ez");

		var local_stdlib_1 = sle.toFile();
		cc.getCompilation().reports().makeOutput(Finally.Outs.Out_40, () -> ("3939 " + local_stdlib_1));

		// TODO stdlib path here
		final java.io.File local_stdlib = new java.io.File("lib_elijjah/lib-" + prelude_name + "/stdlib.ez");

		if (!Objects.equals(sle.toFile(), local_stdlib)) {
			throw new AssertionError();
		}

		final Operation<CompilerInstructions>[] oci = new Operation[]{null};
		if (local_stdlib.exists()) {
			Eventual<Operation<CompilerInstructions>> xoci = null;

			try {
				final String                                    name = local_stdlib.getName();
				final Eventual<Operation<CompilerInstructions>> e    = new Eventual<>();

				// TODO really want EIT_Input or CK_SourceFile here 07/01
				final SourceFileParserParams p    = new SourceFileParserParams(null, local_stdlib, name, cc, e, SourceFileReasoning.STDLIB);
				final QuerySourceFileParser  qsfp = new QuerySourceFileParser(cr);
				xoci = qsfp.process(p);
				xoci.then(xxoci -> {
					if (xxoci.mode() == Mode.SUCCESS) {
						cc.pushItem(xxoci.success());
					}
				});
				xoci.onFail(fff->{assert false;});
				assert xoci.isResolved();
				return e;
			} catch (final Exception e) {
//				return Operation.failure(e);
				assert xoci == null || xoci != null && xoci.isPending();
				Eventual<Operation<CompilerInstructions>> fe = new Eventual<>(); // is this bad??
				fe.fail(e);
				xoci = fe;
			}
			return xoci;
		} else {
			assert oci[0] != null;

			if (oci[0].mode() != Mode.FAILURE) {
				throw new IllegalStateException("expecting failure mode here.");
			}

			//return Objects.requireNonNull(oci[0]);
			//return Operation.failure(new Exception() {
			//	public String message() {
			//		return "No stdlib found";
			//	}
			//});

			return null; //oci[0];
		}
	}
}
