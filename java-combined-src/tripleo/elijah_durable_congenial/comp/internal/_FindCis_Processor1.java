package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.diagnostic.*;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.comp.i.ILazyCompilerInstructions;
import tripleo.elijah.util.Maybe;
import tripleo.elijah_congenial_durable.comp.EDC_Factory;
import tripleo.elijah_congenial_durable.comp.EDC_Factory1;
import tripleo.elijah_durable_congenial.comp.CompilerInput;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;
import tripleo.elijah_durable_congenial.comp.i.ErrSink;

import java.io.File;
import java.nio.file.NotDirectoryException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

class _FindCis_Processor1 implements _FindCis_Processor {
	final List<ICompilerInput> x = new ArrayList<>();

	@Override
	public List<ICompilerInput> x() {
		return x;
	}

	@Override
	public void _processInput(_FindCis_Process aFcip,
	                          @NotNull List<ICompilerInput> x,
	                          @NotNull ICompilerInput input,
	                          CR_FindCIs aCRFindCIs) {
		final List<ICompilerInput> inputList          = aFcip.inputs();
		final CompilationClosure   compilationClosure = aFcip.comp();

		final Compilation c       = aFcip.comp().getCompilation();
		final ErrSink     errSink = aFcip.comp().errSink();

		switch (((CompilerInput) input).ty()) {
		case NULL -> {
		}
		case SOURCE_ROOT -> {
		}
		default -> {
			return;
		}
		}

		final String  file_name = input.getInp();
		final File    f         = new File(file_name);
		final boolean matches2  = Pattern.matches(".+\\.ez$", file_name);
		if (matches2) {
			EDC_Factory                     factory =new EDC_Factory1();
			final ILazyCompilerInstructions ilci    = factory.mkILazyCompilerInstructions(input, c.getCompilationClosure());
			report(input, ilci);
		} else {
			//errSink.reportError("9996 Not an .ez file "+file_name);
			if (f.isDirectory()) {
				aCRFindCIs._inputIsDirectory(c, x, input, f);
			} else {
				final NotDirectoryException d = new NotDirectoryException(f.toString());
				errSink.reportDiagnostic(new ExceptionDiagnostic(d));
//				errSink.reportError("9995 Not a directory " + f.getAbsolutePath());
			}
		}
	}

	private void report(final ICompilerInput input, final ILazyCompilerInstructions ilci) {
		final Maybe<ILazyCompilerInstructions> m4   = new Maybe<>(ilci, null);
		report(input, m4);
	}

	private void report(final ICompilerInput input, final Maybe<ILazyCompilerInstructions> m4) {
		input.accept_ci(m4);
		x.add(input);
	}
}
