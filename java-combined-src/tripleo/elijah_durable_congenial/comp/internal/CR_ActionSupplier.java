package tripleo.elijah_durable_congenial.comp.internal;

import tripleo.elijah_durable_congenial.comp.i.CR_Action;

/**
 * Create as many of these as you want, something something
 */
public interface CR_ActionSupplier {
	CR_Action get();
}
