package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.i.ICompilerInput;

import java.util.List;

interface _FindCis_Processor {

	List<ICompilerInput> x();

	void _processInput(_FindCis_Process aFcip,
	                   @NotNull List<ICompilerInput> x,
	                   @NotNull ICompilerInput input,
	                   CR_FindCIs aCRFindCIs);
}
