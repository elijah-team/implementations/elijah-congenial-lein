package tripleo.elijah_durable_congenial.comp.internal;

import org.jdeferred2.*;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.*;
import tripleo.elijah.comp.i.ILazyCompilerInstructions;
import tripleo.elijah.util.*;
import tripleo.elijah_congenial.process.CBW;
import tripleo.elijah_durable_congenial.comp.i.*;

import java.util.ArrayList;
import java.util.List;

public class CompilationBus implements ICompilationBus {
	private final @NotNull  CompilerDriver   cd;
	private final @NotNull Compilation      c;
	private final @NotNull List<CB_Process> _processes           = new ArrayList<>();
	private final @NotNull IProgressSink    _defaultProgressSink = new DefaultProgressSink();
	private final CBW cbw;

	public CompilationBus(final @NotNull CompilationEnclosure ace) {
		c  = ace.getCompilationAccess().getCompilation();
		cd = new CompilerDriver(this);

//		ace.setCompilerDriver(cd);
		cbw = new CBW(this);
	}

	@Override
	public void add(final @NotNull CB_Action action) {
		final CB_Process process = new SingleActionProcess(action);
		cbw.submit(process);
//		_processes.add(process);
	}

	@Override
	public IProgressSink defaultProgressSink() {
		return _defaultProgressSink;
	}

	@Override
	public CompilerDriver getCompilationDriver() {
		return getCd();
	}

	@Override
	public void add(final @NotNull CB_Process aProcess) {
		_processes.add(aProcess);
	}

	@Override
	public void inst(final @NotNull ILazyCompilerInstructions aLazyCompilerInstructions) {
		final Eventual<Operation<CompilerInstructions>> eventual = aLazyCompilerInstructions.getEventual();
		if (eventual.isResolved()) {
			eventual.then(new DoneCallback<Operation<CompilerInstructions>>() {
				@Override
				public void onDone(final Operation<CompilerInstructions> aCompilerInstructionsOperation) {
					_defaultProgressSink.note(IProgressSink.Codes.LazyCompilerInstructions_inst,
							ProgressSinkComponent.CompilationBus_,
							-1,
							new Object[]{aCompilerInstructionsOperation});
				}
			});
		} else  {

		final CompilerInstructions compilerInstructions = aLazyCompilerInstructions.get();
		_defaultProgressSink.note(IProgressSink.Codes.LazyCompilerInstructions_inst,
				ProgressSinkComponent.CompilationBus_,
				-1,
				new Object[]{compilerInstructions});
	}
	}

	@Override
	public void option(final @NotNull CompilationChange aChange) {
		aChange.apply(c);
	}

	@Override
	public List<CB_Process> processes() {
		return _processes;
	}

	@Override
	public void runOneProcess(final CB_Process process) {
		process.steps().stream().forEach(aCBAction -> aCBAction.execute());
	}

	@Override
	public void start() {
		cbw.start();
	}

	public void runProcesses() {
		List<CB_Process> processes = _processes;
		int              size      = 0;

		while (size < processes.size()) {
			for (int i = size; i < processes.size(); i++) {
				final CB_Process process = processes.get(i);

				runOneProcess(process);
			}

			size = processes.size();
		}
		assert processes.size() == size;
	}

	public @NotNull CompilerDriver getCd() {
		return cd;
	}

	//private static class DefaultProgressSink implements IProgressSink {
	//	@Override
	//	public void note(final Codes aCode, final @NotNull ProgressSinkComponent aProgressSinkComponent, final int aType, final Object[] aParams) {
	//		SimplePrintLoggerToRemoveSoon.println_err_2(aProgressSinkComponent.printErr(aCode, aType, aParams));
	//	}
	//}
}
