package tripleo.elijah_durable_congenial.comp.internal;

import tripleo.elijah_durable_congenial.comp.AccessBus;
import tripleo.elijah_durable_congenial.comp.i.CompilationEnclosure;
import tripleo.elijah_durable_congenial.comp.i.ICompilationAccess2;
import tripleo.elijah_durable_congenial.comp.i.IPipelineAccess;

public record CSS_RunEnv(
		ICompilationAccess2 access,
		CompilationEnclosure ce,
		IPipelineAccess pa,
		CompilationRunner cr,
		CompilerDriver cd,
		AccessBus ab

) {
}
