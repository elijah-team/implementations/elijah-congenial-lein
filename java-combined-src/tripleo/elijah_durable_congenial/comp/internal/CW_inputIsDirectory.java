package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.comp.i.ILazyCompilerInstructions;
import tripleo.elijah.diagnostic.Diagnostic;
import tripleo.elijah.nextgen.inputtree.EIT_InputType;
import tripleo.elijah.util.Maybe;
import tripleo.elijah_congenial.startup.CompilerInstructionsList;
import tripleo.elijah_congenial_durable.comp.ILazyCompilerInstructions_;
import tripleo.elijah_durable_congenial.comp.diagnostic.TooManyEz_ActuallyNone;
import tripleo.elijah_durable_congenial.comp.diagnostic.TooManyEz_BeSpecific;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;
import tripleo.elijah_durable_congenial.comp.queries.QuerySearchEzFiles;

import java.io.File;
import java.util.List;
import java.util.function.Consumer;

@SuppressWarnings({"UtilityClassCanBeEnum", "FinalClass"})
public final class CW_inputIsDirectory {
	private CW_inputIsDirectory() { }

	public static void apply(final @NotNull ICompilerInput input,
							 final @NotNull Compilation c,
							 final @NotNull File f,
							 final @NotNull Consumer<ICompilerInput> x) {
		CompilerInstructions ez_file;
		input.setDirectory(f);

		final List<CompilerInstructions> ezs = searchEzFiles(f, c.getCompilationClosure());

		switch (ezs.size()) {
		case 0:
			final Diagnostic d_toomany = new TooManyEz_ActuallyNone();
			final Maybe<ILazyCompilerInstructions> m = new Maybe<>(null, d_toomany);
			input.accept_ci(m);
			x.accept(input);
			break;
		case 1:
			ez_file = ezs.get(0);
			final ILazyCompilerInstructions ilci = ILazyCompilerInstructions_.of(ez_file);
			final Maybe<ILazyCompilerInstructions> m3 = new Maybe<>(ilci, null);
			input.accept_ci(m3);
			x.accept(input);
			break;
		default:
			//final Diagnostic d_toomany = new TooManyEz_UseFirst();
			//add_ci(ezs.get(0));

			// more than 1 (negative is not possible)
			final Diagnostic d_toomany2 = new TooManyEz_BeSpecific();
			final Maybe<ILazyCompilerInstructions> m2 = new Maybe<>(null, d_toomany2);
			input.accept_ci(m2);
			x.accept(input);
			break;
		}

		c.reports().addInput(input, EIT_InputType.EZ_FILE);
	}

	private static List<CompilerInstructions> searchEzFiles(final @NotNull File directory, final @NotNull CompilationClosure ccl) {
		final QuerySearchEzFiles       q     = new QuerySearchEzFiles(ccl);
		final CompilerInstructionsList eolci = q.process(directory);

		assert eolci!=null;


		//eolci.then
		//
		//switch (olci.mode()) {
		//case SUCCESS -> {
		//	return olci.success();
		//}
		//case FAILURE -> {
		//	ccl.errSink().reportDiagnostic(olci.failure());
		//	return List_of();
		//}
		//default -> throw new IllegalStateException("Unexpected value: " + olci.mode());
		//}

		return eolci.getList();
	}
}
