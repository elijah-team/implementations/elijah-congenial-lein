package tripleo.elijah_durable_congenial.comp.internal;

import org.jdeferred2.DoneCallback;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Ok;
import tripleo.elijah.util.Operation;
import tripleo.elijah_durable_congenial.comp.i.CR_Action;

import java.util.List;

public enum U { ;
	public static void _run_action_list__internal(final @NotNull CR_State crState, final @NotNull CB_Output out, final CR_Action each, final @NotNull List<Operation<Ok>> crActionResultList) {
		each.attach(crState.runner());
		final Eventual<Operation<Ok>> eoo = new Eventual<>();
		eoo.then(new DoneCallback<Operation<Ok>>() {
			@Override
			public void onDone(Operation<Ok> e) {
				crActionResultList.add(e);
			}
		});
		each.execute(crState, out, eoo);
		//assert eoo.isResolved(); // !?
		eoo.register(crState.ca().getCompilation().getFluffy());
	}
}
