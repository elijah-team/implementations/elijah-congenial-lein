package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.stateful.DefaultStateful;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Ok;
import tripleo.elijah.util.Operation;
import tripleo.elijah_durable_congenial.comp.i.*;

import java.io.File;
import java.util.List;

public class CR_FindCIs extends DefaultStateful implements CR_Action {
	private final @NotNull List<ICompilerInput> inputs;
	private final @NotNull CCI                  cci;
	private final @NotNull IProgressSink        _ps;

	public CR_FindCIs(final @NotNull CompilerBeginning beginning) {
		//State st = CompilationRunner.ST.INITIAL; // que?? 07/01

		inputs = beginning.compilerInput();

		var comp         = beginning.compilation();
		var progressSink = beginning.progressSink();

		// TODO 09/05 look at 2 different progressSinks
		cci = new DefaultCCI(comp, comp._cis(), progressSink);
		_ps = comp.getCompilationEnclosure().getCompilationBus().defaultProgressSink();
	}

	@Override
	public void attach(final @NotNull CompilationRunner cr) {
	}

	@Override
	public void execute(final @NotNull CR_State st, final @NotNull CB_Output aO, final Eventual<Operation<Ok>> eoo) {
		final Compilation c = st.ce().getCompilation();

		final List<ICompilerInput> x = find_cis(new _FindCis_Process(inputs, c.getCompilationClosure()));
		for (final ICompilerInput compilerInput : x) {
			cci.accept(compilerInput.acceptance_ci(), _ps);
		}

		final Operation<Ok> success = Operation.success(Ok.instance());
		eoo.resolve(success);
	}

	private List<ICompilerInput> find_cis(final _FindCis_Process fcip) {
		final List<ICompilerInput> inputList          = fcip.inputs();
		final CompilationClosure   compilationClosure = fcip.comp();

		_FindCis_Processor r = new _FindCis_Processor1();

		for (final ICompilerInput input : inputList) {
			r._processInput(fcip, r.x(), input, this);
		}

		return r.x();
	}

	@Override
	public @NotNull String name() {
		return "find cis";
	}

	void _inputIsDirectory(final @NotNull Compilation c,
	                       final @NotNull List<ICompilerInput> x,
	                       final @NotNull ICompilerInput input,
	                       final @NotNull File f) {
		CW_inputIsDirectory.apply(input, c, f, (final @NotNull ICompilerInput inp) -> x.add(inp));
	}

	@Override
	public String toString() {
		return "CR_Action::" + getClass().getName();
	}
}
