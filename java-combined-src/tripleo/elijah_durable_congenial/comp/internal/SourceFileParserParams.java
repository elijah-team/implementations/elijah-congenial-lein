package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Operation;
import tripleo.elijah_congenial.startup.SourceFileReasoning;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;

import java.io.File;

public record SourceFileParserParams(
		/*@NotNull*/
		ICompilerInput input,
		@NotNull File f,
		@NotNull String file_name,
		@NotNull CompilationClosure cc,
		@NotNull Eventual<Operation<CompilerInstructions>> eoci,
		@NotNull SourceFileReasoning reasoning
) { }
