package tripleo.elijah_durable_congenial.comp.internal;

import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.i.IProgressSink;

import java.util.List;

public record CompilerBeginning(
		Compilation compilation,
		CompilerInstructions compilerInstructions,
		List<ICompilerInput> compilerInput,
		IProgressSink progressSink,
		Compilation.CompilationConfig cfg
) {
}
