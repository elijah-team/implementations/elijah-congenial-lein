package tripleo.elijah_durable_congenial.comp.internal;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Ok;
import tripleo.elijah.util.Operation;
import tripleo.elijah_durable_congenial.comp.i.CR_Action;

public class CR_AlmostComplete implements CR_Action {
	private CompilationRunner compilationRunner;

	@Override
	public void attach(final @NotNull CompilationRunner cr) {
		compilationRunner = cr;
	}

	@Override
	public void execute(final @NotNull CR_State st, final CB_Output aO, final Eventual<Operation<Ok>> eoo) {
		compilationRunner.cis.almostComplete();
		final Operation<Ok> success = Operation.success(Ok.instance());
		eoo.resolve(success);
	}

	@Override
	public @NotNull String name() {
		return "cis almostComplete";
	}

	@Override
	public String toString() { return "CR_Action::"+getClass().getName(); }
}
