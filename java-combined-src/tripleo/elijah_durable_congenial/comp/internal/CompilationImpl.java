package tripleo.elijah_durable_congenial.comp.internal;

import io.reactivex.rxjava3.core.Observer;
import lombok.*;
import org.jetbrains.annotations.*;
import tripleo.elijah.ci.*;
import tripleo.elijah.comp.*;
import tripleo.elijah.comp.i.*;
import tripleo.elijah.util.*;
import tripleo.elijah.world.impl.*;
import tripleo.elijah_congenial.startup.*;
import tripleo.elijah_durable_congenial.comp.*;
import tripleo.elijah_durable_congenial.comp.i.*;
import tripleo.elijah_durable_congenial.comp.nextgen.*;
import tripleo.elijah_durable_congenial.lang.i.*;
import tripleo.elijah_durable_congenial.lang.impl.*;
import tripleo.elijah_durable_congenial.nextgen.inputtree.*;
import tripleo.elijah_durable_congenial.nextgen.outputtree.*;
import tripleo.elijah_durable_congenial.nextgen.query.*;
import tripleo.elijah_durable_congenial.stages.deduce.*;
import tripleo.elijah_durable_congenial.stages.deduce.fluffy.i.*;
import tripleo.elijah_durable_congenial.stages.deduce.fluffy.impl.*;
import tripleo.elijah_durable_congenial.util.*;
import tripleo.elijah_durable_congenial.world.i.*;
import tripleo.elijah_durable_congenial.world.impl.*;

import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public class CompilationImpl implements Compilation {
	public static boolean NOCIS = false;

	private final @NotNull FluffyCompImpl    _fluffyComp;
	@Getter
	private final CIS                               _cis;
	@Getter
	private final CompilationConfig                 cfg;
	@Getter
	private final Map<String, CompilerInstructions> fn2ci;
	private final List<OS_Module>                   modules;
	private final                  USE                               use;
	private final                  ErrSink                           errSink;
	private final                  int                               _compilationNumber;
	private final                  CP_Paths                          paths;
	private final                  EIT_InputTree                     _input_tree;
	private final                  CompFactory                       _con;
	private final                  Finally                           _f;
	private final                  LivingRepo                        _repo;
	private final                  CompilationEnclosure              compilationEnclosure;
	private final                        IO              io;
	private final /*gnu.trove.TIntList*/ List<Integer>   simpleSignals = new ArrayList<>();
	private @Nullable EOT_OutputTree       _output_tree  = null;
	@Getter
	private           CompilerInstructions rootCI;
	private           IPipelineAccess      _pa;

	public CompilationImpl(final ErrSink aErrSink, final IO aIo) {
		this.errSink = aErrSink;
		this.io      = aIo;

		this._compilationNumber = new Random().nextInt(Integer.MAX_VALUE);
		fn2ci                   = new HashMap<>();
		modules                 = new ArrayList<>();

		this.paths = new CP_Paths(this);

		_fluffyComp = new FluffyCompImpl(this);

		cfg = new CompilationConfig();

		_input_tree = new EIT_InputTree();

		compilationEnclosure = new CompilationEnclosure(this);
		_repo                = new DefaultLivingRepo();

		_cis = new CIS();
		use  = new USE(this);

		_con = new DefaultCompFactory();
		_f   = new Finally();
	}

	@NotNull
	private static Function<String, ICompilerInput> stringListToCIList() { // oops, but good
		return s -> {
			final ICompilerInput input = new CompilerInput(s);
			if (s.equals(input.getInp())) {
				input.setSourceRoot();
			} else {
				assert false;
			}
			return input;
		};
	}

	public void testMapHooks(final List<IFunctionMapHook> aMapHooks) {
		//pipelineLogic.dp.
	}

	private class DefaultCompFactory implements CompFactory {
		@Override
		public @NotNull EIT_ModuleInput createModuleInput(final OS_Module aModule) {
			return new EIT_ModuleInput(aModule, CompilationImpl.this);
		}

		@Override
		public @NotNull Qualident createQualident(final @NotNull List<String> sl) {
			Qualident R = new QualidentImpl();
			for (String s : sl) {
				R.append(Helpers.string_to_ident(s));
			}
			return R;
		}

		@Override
		public @NotNull IInputRequest createInputRequest(final File aFile, final boolean aDo_out, final @Nullable LibraryStatementPart aLsp) {
			return new InputRequest(aFile, aLsp);
		}

		@Override
		public @NotNull WorldModule createWorldModule(final OS_Module m) {
			CompilationEnclosure ce = getCompilationEnclosure();
			final WorldModule    R  = new DefaultWorldModule(m, ce);

			return R;
		}
	}	@Override
	public void feedCmdLine(final @NotNull List<String> args) {
		final List<ICompilerInput>      inputs     = stringListToCompilerInputs(args);
		final DefaultCompilerController controller = new DefaultCompilerController();
		feedInputs(inputs, controller);
	}

	@Override
	public void subscribeCI(final @NotNull Observer<CompilerInstructions> aCio) {
		_cis.subscribe(aCio);
	}




	@Override
	public @NotNull FluffyComp getFluffy() {
		return _fluffyComp;
	}

	@Override
	public @NotNull EOT_OutputTree getOutputTree() {
		if (_output_tree == null) {
			_output_tree = new EOT_OutputTree();
		}

		return _output_tree;
	}


	@Override
	public void feedInputs(final @NotNull List<ICompilerInput> inputs, final @NotNull CompilerController controller) {
		if (inputs.isEmpty()) {
			controller.printUsage();
			return;
		}

		getFluffy().setController(controller);

		compilationEnclosure.setCompilerInput(inputs);

		controller._setInputs(this, inputs);
		controller.processOptions();
		controller.runner();
	}


	@Override
	public Finally reports() {
		return _f;
	}

	@Override
	public ICompilationAccess2 getCompilationAccess2() {
		return new CompilationAccess2Impl(this);
	}

	@Override
	public @NotNull CompilationClosure getCompilationClosure() {
		return new CompilationClosure() {
			@Override
			public ErrSink errSink() {
				return errSink;
			}

			@Override
			public IO io() {
				return io;
			}

			@Override
			public @NotNull Compilation getCompilation() {
				return CompilationImpl.this;
			}

			@Override
			public File getFile(final String aFileName, final ICompilerInput aInput) {
				System.err.println("** Create file from CompilerInput: " + aFileName);
				return new File(aFileName);
			}

			@Override
			public QueryDatabase queryDb() {
				return this.getCompilation().queryDb();
			}

			@Override
			public CP_Paths paths() {
				return getCompilation().paths();
			}

			@Override
			public CompilationEnclosure getCompilationEnclosure() {
				return getCompilation().getCompilationEnclosure();
			}
		};
	}

	@Override
	public QueryDatabase queryDb() {
		return new QueryDatabase(this);
	}

@Override
	@NotNull
	public List<ICompilerInput> stringListToCompilerInputs(final List<String> strings) {
	return strings.stream()
				.map(CompilerInput::new)
				.collect(Collectors.toList());
	}

	@Override
	public void pushItem(final PI_SourceFileParserParams aSourceFileParserParams) {
		final Operation<CompilerInstructions> oci = this.getCompilationEnclosure().getCompilationRunner().parseEzFile(aSourceFileParserParams.getP());
		assert oci != null;
	}

	@Override
	public Map<String, CompilerInstructions> fn2ci() {
		return this.fn2ci;
	}

	@Override
	public USE use() {
		return use;
	}

	@Override
	public CIS _cis() {
		return _cis;
	}

	@Override
	public CompilationEnclosure getCompilationEnclosure() {
		return compilationEnclosure;
	}

	@Override
	public void addModule__(final @NotNull OS_Module module, final @NotNull String fn) {
		modules.add(module);
		use.addModule(module, fn);
	}

	@Override
	public @NotNull CompFactory con() {
		return _con;
	}

	@Override
	public void eachModule(final @NotNull Consumer<OS_Module> object) {
		for (OS_Module mod : modules) {
			object.accept(mod);
		}
	}

	@Override
	public int errorCount() {
		return errSink.errorCount();
	}

	@Override
	public CompilerBeginning beginning(final @NotNull CompilationRunner compilationRunner) {
		return new CompilerBeginning(this, rootCI, compilationEnclosure.getCompilerInput(), compilationRunner.progressSink, cfg());
	}

	@Override
	public void pushItem(final CSS_SimpleSignal aSimpleSignal) {
		if (aSimpleSignal.isOnce()) {
			final int klazzHash = aSimpleSignal.getClass().hashCode(); // assuming this is consistent
			if (simpleSignals.contains(klazzHash))
				return;
			else {
				simpleSignals.add(klazzHash);
			}
		}
		if (aSimpleSignal.canRun()) {
			final CSS_RunEnv are = new CSS_RunEnv(getCompilationAccess2(),
			                                      compilationEnclosure,
			                                      pa(),
			                                      compilationEnclosure.getCompilationRunner(),
			                                      compilationEnclosure.getCompilationDriver(),
			                                      EventualExtract.of(compilationEnclosure.getAccessBusPromise()));
			aSimpleSignal.simpleSignalRun(are);
		} else {
			if (aSimpleSignal instanceof CSS_HasInstructions chi) {
				hasInstructions(chi.rootCI());
			} else assert false;
		}
	}

	@Override
	public void feed(final List<CF_Base> aCFList) {
		for (CF_Base cfBase : aCFList) {
			System.err.println("" + cfBase);
		}
	}

	@Override
	public Operation2<WorldModule> findPrelude(final String prelude_name) {
		return use.findPrelude(prelude_name);
	}

	@Override
	public String getCompilationNumberString() {
		return String.format("%08x", _compilationNumber);
	}

	@Override
	public ErrSink getErrSink() {
		return errSink;
	}

	@Override
	public IO getIO() {
		return io;
	}

	@Override
	public OS_Package getPackage(final @NotNull Qualident pkg_name) {
		return _repo.getPackage(pkg_name.toString());
	}

	@Override
	public String getProjectName() {
		return rootCI.getName();
	}

	private void hasInstructions(final CompilerInstructions aRootCI) {
		final CompilationEnclosure ce = getCompilationEnclosure();
		assert !ce.getCompilerInput().isEmpty();

		//this.signals().hasInstructions()
		//		.signal(this.con().createSignal_hasInstructions(pa, cis)); // this is wrong
		//		.signal(pa, List_of(cis.get(0)));

		rootCI = aRootCI;
		ce.getCompilationRunner().start(rootCI, pa()); // .
	}

	@Override
	public boolean isPackage(final @NotNull String pkg) {
		return _repo.hasPackage(pkg);
	}

	@Override
	public OS_Package makePackage(final Qualident pkg_name) {
		return _repo.makePackage(pkg_name);
	}

	@Override
	public @NotNull ModuleBuilder moduleBuilder() {
		return new ModuleBuilder(this);
	}

	@Override
	public IPipelineAccess pa() {
		assert _pa != null;

		return _pa;
	}

	@Override
	public void set_pa(IPipelineAccess a_pa) {
		_pa = a_pa;

		compilationEnclosure.pipelineAccessPromise.resolve(_pa);
	}

	@Override
	public void pushItem(CompilerInstructions aci) {
		_cis.onNext(aci);
	}



	@Override
	public void use(final @NotNull CompilerInstructions compilerInstructions, final boolean do_out) {
		use.use(compilerInstructions, do_out);    // NOTE Rust
	}

	@Override
	public LivingRepo world() {
		return _repo;
	}

	@Override
	public LivingRepo livingRepo() {
		return _repo;
	}

	@Override
	public CP_Paths paths() {
		return paths;
	}

	@Override
	public @NotNull EIT_InputTree getInputTree() {
		return _input_tree;
	}

	@Override
	public @NotNull CompilationConfig cfg() {
		return cfg;
	}
}
