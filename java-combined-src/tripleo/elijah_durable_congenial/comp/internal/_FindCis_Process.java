package tripleo.elijah_durable_congenial.comp.internal;

import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;

import java.util.List;

record _FindCis_Process(
		List<ICompilerInput> inputs,
		CompilationClosure comp
) {
}
