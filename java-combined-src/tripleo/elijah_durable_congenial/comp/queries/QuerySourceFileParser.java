package tripleo.elijah_durable_congenial.comp.queries;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Operation;
import tripleo.elijah_durable_congenial.comp.internal.CompilationRunner;
import tripleo.elijah_durable_congenial.comp.internal.SourceFileParserParams;

public class QuerySourceFileParser {
	private final CompilationRunner cr;

	public QuerySourceFileParser(final CompilationRunner aCr) {
		cr = aCr;
	}

	public Eventual<Operation<CompilerInstructions>> process(final @NotNull SourceFileParserParams p) {
		final var oci = cr.realParseEzFile(p);
		return oci;
	}
}
