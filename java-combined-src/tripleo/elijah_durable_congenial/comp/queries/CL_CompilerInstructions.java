package tripleo.elijah_durable_congenial.comp.queries;

import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Operation;
import tripleo.elijjah.EzLexer;
import tripleo.elijjah.EzParser;

public class CL_CompilerInstructions {
	private Eventual<Operation<CompilerInstructions>> eventual;
	private QueryEzFileToModuleParams                 params;

	public void advise(final QueryEzFileToModuleParams aParams) {
		this.params = aParams;
	}

	public void advise(final EzLexer aLexer, final EzParser aParser) {
		// no-op wot
	}

	public Eventual<Operation<CompilerInstructions>> getOp() {
		if (this.eventual == null) {
			this.eventual = new Eventual<>();
		}
		return this.eventual;
	}

	public void setOp(final Eventual<Operation<CompilerInstructions>> aOp) {
		eventual = aOp;
	}
}
