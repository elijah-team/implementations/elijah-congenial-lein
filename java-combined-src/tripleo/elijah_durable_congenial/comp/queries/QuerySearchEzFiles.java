package tripleo.elijah_durable_congenial.comp.queries;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Operation;
import tripleo.elijah_congenial.startup.CompilerInstructionsList;
import tripleo.elijah_congenial.startup.SourceFileReasoning;
import tripleo.elijah_durable_congenial.comp.i.CompilationClosure;
import tripleo.elijah_durable_congenial.comp.i.ErrSink;
import tripleo.elijah_durable_congenial.comp.internal.SourceFileParserParams;

import java.io.File;
import java.io.FilenameFilter;
import java.util.regex.Pattern;

public class QuerySearchEzFiles {
	private final @NotNull CompilationClosure cc;
	private final          FilenameFilter     ez_files_filter = new EzFilesFilter();

	public QuerySearchEzFiles(final @NotNull CompilationClosure ccl) {
		this.cc = ccl;
	}

	public @NotNull CompilerInstructionsList process(final @NotNull File directory) {
		final CompilerInstructionsList cil     = new CompilerInstructionsList(directory, this.cc);
		final ErrSink                  errSink = cc.errSink();
		final String[]                 list    = directory.list(ez_files_filter);

		if (list != null) {
			cil.setDirectoryList(list);
			for (final String file_name : list) {
				try {
					Eventual<Operation<CompilerInstructions>> eeee   = new Eventual<>();
					final File                                file   = new File(directory, file_name);
					final CompilerInstructions                ezFile = parseEzFile(file, file.toString(), cc, eeee);
					if (ezFile != null) {
						cil.put(file, ezFile);
					} else {
						cil.reportError(9995, file);
						errSink.reportError("9995 ezFile is null " + file); // TODO Diagnostic
					}
				} catch (final Exception e) {
					errSink.exception(e);  // TODO Diagnostic or Operation
				}
			}
		}

		return cil;
	}

	@Nullable CompilerInstructions parseEzFile(final @NotNull File f, final @NotNull String file_name, final @NotNull CompilationClosure cc, final @NotNull Eventual<Operation<CompilerInstructions>> e) {
		var p = new SourceFileParserParams(null, f, file_name, cc, e, SourceFileReasoning.EZ);
		return cc.getCompilationEnclosure().getCompilationRunner().parseEzFile(p).success();
	}

	private static class EzFilesFilter implements FilenameFilter {
		@Override
		public boolean accept(final File file, final String s) {
			final boolean matches2 = Pattern.matches(".+\\.ez$", s);
			return matches2;
		}
	}
}
