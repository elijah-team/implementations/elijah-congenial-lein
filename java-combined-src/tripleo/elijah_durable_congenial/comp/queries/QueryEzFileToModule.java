package tripleo.elijah_durable_congenial.comp.queries;

import antlr.RecognitionException;
import antlr.TokenStreamException;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.ci_impl.CompilerInstructionsImpl;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.NotImplementedException;
import tripleo.elijah.util.Operation;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.nextgen.query.QueryDatabase;
import tripleo.elijjah.EzLexer;
import tripleo.elijjah.EzParser;

import java.io.InputStream;

public class QueryEzFileToModule {
	private final QueryDatabase             queryDatabase;
	private final QueryEzFileToModuleParams params;
	private       Eventual<Operation<CompilerInstructions>> eoci;

	public QueryEzFileToModule(final QueryDatabase aQueryDatabase, final QueryEzFileToModuleParams aParams) {
		queryDatabase = aQueryDatabase;
		params        = aParams;
	}

	public CL_CompilerInstructions calculate() {
		final var result = new CL_CompilerInstructions();

		final var op = getOp();
		result.setOp(op);

		result.advise(params);

		final String      f = params.sourceFilename;
		final InputStream s = params.inputStream;

		final EzLexer lexer = new EzLexer(s);
		lexer.setFilename(f);
		final EzParser parser = new EzParser(lexer);
		parser.setFilename(f);

		parser.ci = new CompilerInstructionsImpl();

		try {
			final CompilerInstructions instructions = new CompilerInstructionsImpl();
			parser.ci = instructions;
			parser.program();
			op.resolve(Operation.success(instructions));
		} catch (RecognitionException | TokenStreamException aE) {
			var ff = Operation.failure(aE);
			op.fail(aE);
		}

		return result;
	}

	private Eventual<Operation<CompilerInstructions>> getOp() {
		if (this.eoci == null) {
			this.eoci = new Eventual<>();
		}
		return this.eoci;
	}

	public OS_Module load(final QueryDatabase qb) {
		throw new NotImplementedException();
	}
}
