package tripleo.elijah_durable_congenial.comp;

import tripleo.elijah_durable_congenial.nextgen.outputtree.EOT_FileNameProvider;
import tripleo.elijah_durable_congenial.nextgen.outputtree.EOT_OutputFile;

public class Finally_Output {
	final         EOT_FileNameProvider fileNameProvider;
	private final EOT_OutputFile       off;

	public Finally_Output(final EOT_FileNameProvider aFileNameProvider, final EOT_OutputFile aOff) {
		fileNameProvider = aFileNameProvider;
		off              = aOff;
	}

	public String name() {
		return fileNameProvider.getFilename();
	}

	@Override
	public String toString() {
		return "Output{" +
				"fileNameProvider=" + fileNameProvider.getFilename().toString() +
				'}';
	}
}
