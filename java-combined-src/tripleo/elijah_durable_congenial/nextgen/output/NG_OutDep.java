package tripleo.elijah_durable_congenial.nextgen.output;

import tripleo.elijah.nextgen.comp_model.CM_Module;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;

public record NG_OutDep(OS_Module module) implements CM_Module {
	@Override
	public String getFilename() {
		return module.getFileName();
	}

	public OS_Module getModule() {
		return module;
	}
}
