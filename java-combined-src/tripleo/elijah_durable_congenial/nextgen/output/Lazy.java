package tripleo.elijah_durable_congenial.nextgen.output;

public interface Lazy<T> {

	T getValue();

}
