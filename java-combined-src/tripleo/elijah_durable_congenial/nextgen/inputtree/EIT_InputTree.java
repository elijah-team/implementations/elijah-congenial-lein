package tripleo.elijah_durable_congenial.nextgen.inputtree;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.util.Operation;

import java.util.ArrayList;
import java.util.List;

public class EIT_InputTree {
	private final List<_Node> nodes = new ArrayList<>();

	public void addNode(final ICompilerInput aInput) {
		next_node().setCompilerInput(aInput);
	}

	private @NotNull _Node next_node() {
		var R = new _Node();
		nodes.add(R);
		return R;
	}

	public void setNodeOperation(final ICompilerInput input, final Operation<?> operation) {
		for (_Node node : nodes) {
			if (node.getCompilerInput() == input) {
				node.operation = operation;
			}
		}
	}

	class _Node {
		public  Operation<?>   operation;
		private ICompilerInput compilerInput;

		public ICompilerInput getCompilerInput() {
			return compilerInput;
		}

		public void setCompilerInput(final ICompilerInput aCompilerInput) {
			compilerInput = aCompilerInput;
		}
	}
}
