package tripleo.elijah_durable_congenial.nextgen.inputtree;

import tripleo.elijah.nextgen.inputtree.EIT_InputType;

public interface EIT_Input {
	EIT_InputType getType();

	// TODO CP_Filename, hashPromise??
}
