package tripleo.elijah_durable_congenial.nextgen.query;

import tripleo.elijah_durable_congenial.comp.i.Compilation;

public class QueryDatabase {
	private final Compilation compilation;

	public QueryDatabase(final Compilation aCompilation) {
		compilation = aCompilation;
	}
}
