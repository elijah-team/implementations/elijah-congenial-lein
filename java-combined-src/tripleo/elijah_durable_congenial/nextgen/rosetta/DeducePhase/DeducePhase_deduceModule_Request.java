package tripleo.elijah_durable_congenial.nextgen.rosetta.DeducePhase;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.sanaa.ElIntrinsics;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.nextgen.rosetta.DeduceTypes2.DeduceTypes2Request;
import tripleo.elijah_durable_congenial.nextgen.rosetta.Rosetta;
import tripleo.elijah_durable_congenial.stages.deduce.DeducePhase;
import tripleo.elijah_durable_congenial.stages.deduce.DeduceTypes2;
import tripleo.elijah_durable_congenial.stages.logging.ElLog;

public final class DeducePhase_deduceModule_Request {
	@NotNull
	private final OS_Module       module;
	@NotNull
	private final Iterable        listOfEvaFunctions;
	@NotNull
	private final ElLog.Verbosity verbosity;
	@NotNull
	private final DeducePhase     deducePhase;
	@Nullable
	private       DeduceTypes2    createdDeduceTypes2;

	public DeducePhase_deduceModule_Request(@NotNull OS_Module module, @NotNull Iterable listOfEvaFunctions, @NotNull ElLog.Verbosity verbosity, @NotNull DeducePhase deducePhase) {
		ElIntrinsics.checkNotNullParameter(module, "module");
		ElIntrinsics.checkNotNullParameter(listOfEvaFunctions, "listOfEvaFunctions");
		ElIntrinsics.checkNotNullParameter(verbosity, "verbosity");
		ElIntrinsics.checkNotNullParameter(deducePhase, "deducePhase");
		this.module             = module;
		this.listOfEvaFunctions = listOfEvaFunctions;
		this.verbosity          = verbosity;
		this.deducePhase        = deducePhase;
	}

	@NotNull
	public static DeduceTypes2 createDeduceTypes2Singleton(@NotNull DeducePhase_deduceModule_Request moduleRequest) {
		ElIntrinsics.checkNotNullParameter(moduleRequest, "moduleRequest");
		if (moduleRequest.createdDeduceTypes2 == null) {
			moduleRequest.createdDeduceTypes2 = moduleRequest.createDeduceTypes2();
		}

		DeduceTypes2 var10000 = moduleRequest.createdDeduceTypes2;
		ElIntrinsics.checkNotNull(var10000);
		return var10000;
	}

	@NotNull
	public DeducePhase_deduceModule_Request copy(@NotNull OS_Module module, @NotNull Iterable listOfEvaFunctions, @NotNull ElLog.Verbosity verbosity, @NotNull DeducePhase deducePhase) {
		ElIntrinsics.checkNotNullParameter(module, "module");
		ElIntrinsics.checkNotNullParameter(listOfEvaFunctions, "listOfEvaFunctions");
		ElIntrinsics.checkNotNullParameter(verbosity, "verbosity");
		ElIntrinsics.checkNotNullParameter(deducePhase, "deducePhase");
		return new DeducePhase_deduceModule_Request(module, listOfEvaFunctions, verbosity, deducePhase);
	}

	@NotNull
	public OS_Module getModule() {
		return this.module;
	}

	@NotNull
	public Iterable getListOfEvaFunctions() {
		return this.listOfEvaFunctions;
	}

	@NotNull
	public ElLog.Verbosity getVerbosity() {
		return this.verbosity;
	}

	@NotNull
	public DeducePhase getDeducePhase() {
		return this.deducePhase;
	}

	@NotNull
	public DeduceTypes2 createDeduceTypes2() {
		DeduceTypes2Request deduceTypes2Request = new DeduceTypes2Request(this.module, this.deducePhase, this.verbosity);
		DeduceTypes2 var10000 = Rosetta.create(deduceTypes2Request);
		ElIntrinsics.checkNotNullExpressionValue(var10000, "create(...)");
		return var10000;
	}

	@NotNull
	public OS_Module component1() {
		return this.module;
	}

	@NotNull
	public Iterable component2() {
		return this.listOfEvaFunctions;
	}

	@NotNull
	public ElLog.Verbosity component3() {
		return this.verbosity;
	}

	@NotNull
	public DeducePhase component4() {
		return this.deducePhase;
	}

	public int hashCode() {
		int result = this.module.hashCode();
		result = result * 31 + this.listOfEvaFunctions.hashCode();
		result = result * 31 + this.verbosity.hashCode();
		result = result * 31 + this.deducePhase.hashCode();
		return result;
	}

	public boolean equals(@Nullable Object other) {
		if (this == other) {
			return true;
		} else if (!(other instanceof DeducePhase_deduceModule_Request var2)) {
			return false;
		} else {
			if (!ElIntrinsics.areEqual(this.module, var2.module)) {
				return false;
			} else if (!ElIntrinsics.areEqual(this.listOfEvaFunctions, var2.listOfEvaFunctions)) {
				return false;
			} else if (this.verbosity != var2.verbosity) {
				return false;
			} else {
				return ElIntrinsics.areEqual(this.deducePhase, var2.deducePhase);
			}
		}
	}

	@NotNull
	public String toString() {
		return "DeducePhase_deduceModule_Request(module=" + this.module + ", listOfEvaFunctions=" + this.listOfEvaFunctions + ", verbosity=" + this.verbosity + ", deducePhase=" + this.deducePhase + ')';
	}
}
