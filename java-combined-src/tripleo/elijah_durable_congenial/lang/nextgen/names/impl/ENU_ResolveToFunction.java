package tripleo.elijah_durable_congenial.lang.nextgen.names.impl;

import tripleo.elijah_durable_congenial.lang.i.FunctionDef;
import tripleo.elijah_durable_congenial.lang.nextgen.names.i.EN_Understanding;

public record ENU_ResolveToFunction(FunctionDef fd) implements EN_Understanding {
}
