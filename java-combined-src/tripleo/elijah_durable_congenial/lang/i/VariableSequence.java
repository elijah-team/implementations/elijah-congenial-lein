package tripleo.elijah_durable_congenial.lang.i;

import tripleo.elijah_durable_congenial.lang2.ElElementVisitor;

import java.util.Collection;
import java.util.List;

public interface VariableSequence extends FunctionItem, StatementItem, ClassItem {
	void addAnnotation(AnnotationClause a);

	List<AnnotationClause> annotations();

	void defaultModifiers(TypeModifiers aModifiers);

	@Override
	El_Category getCategory();

	@Override
	void setCategory(El_Category aCategory);

	@Override
	Context getContext();

	@Override
	OS_Element getParent();

	void setParent(OS_Element parent);

	@Override
	void visitGen(ElElementVisitor visit);

	void setContext(Context ctx);

	Collection<VariableStatement> items();

	VariableStatement next();

	void setTypeName(TypeName aTn);

	@Override
	String toString();

	@Override
	default void serializeTo(SmallWriter sw) {

	}
}
