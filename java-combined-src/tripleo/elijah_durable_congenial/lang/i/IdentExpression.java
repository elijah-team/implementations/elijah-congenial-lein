package tripleo.elijah_durable_congenial.lang.i;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.diagnostic.Locatable;
import tripleo.elijah_durable_congenial.lang.impl.IdentExpressionImpl;
import tripleo.elijah_durable_congenial.lang.nextgen.names.i.EN_Name;
import tripleo.elijah_durable_congenial.util.Helpers;

public interface IdentExpression extends IExpression, OS_Element, Resolvable, Locatable {
	@Contract("_ -> new")
	static @NotNull IdentExpression forString(String string) {
		return new IdentExpressionImpl(Helpers.makeToken(string), "<inline-absent2>");
	}

	@Override
	ExpressionKind getKind();

	@Override
	void setKind(ExpressionKind aIncrement);

	@Override
	IExpression getLeft();

	@Override
	void setLeft(IExpression iexpression);

	@Override
	OS_Type getType();

	@Override
	void setType(OS_Type deducedExpression);

	@Override
	boolean is_simple();

	@Override
	String repr_();

	@NotNull
	String getText();

	void setContext(Context context);

	EN_Name getName();

	@Override
	void serializeTo(SmallWriter sw);
}
