package tripleo.elijah_durable_congenial.lang.i;

import antlr.Token;

public interface VariableReference extends IExpression {
	String getName();

	@Override
	OS_Type getType();

	@Override
	void setType(OS_Type deducedExpression);

	@Override
	boolean is_simple();

	@Override
	String repr_();

	void setMain(String s);

	void setMain(Token t);

	@Override
	String toString();
}
