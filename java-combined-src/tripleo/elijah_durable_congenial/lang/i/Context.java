package tripleo.elijah_durable_congenial.lang.i;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.lang.impl.ContextImpl;
import tripleo.elijah_durable_congenial.lang.nextgen.names.i.EN_Name;

import java.util.List;

public interface Context {
	ContextImpl.Expectation expect(String aName, OS_Element aElement);

	List<ContextImpl.Expectation> getExpectations();

	@NotNull
	Compilation compilation();

	@Nullable Context getParent();

	LookupResultList lookup(@NotNull String name);

	LookupResultList lookup(String name,
							int level,
							LookupResultList Result,
							ISearchList alreadySearched,
							boolean one);

	@NotNull
	OS_Module module();

	void addName(EN_Name aName);

}
