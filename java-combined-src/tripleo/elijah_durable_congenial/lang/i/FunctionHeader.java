package tripleo.elijah_durable_congenial.lang.i;

public interface FunctionHeader {
	FormalArgList getFal();

	void setFal(FormalArgList aFal);

	FunctionModifiers getModifier();

	void setModifier(FunctionModifiers aModifiers);

	IdentExpression getName();

	void setName(IdentExpression aIdentExpression);

	TypeName getReturnType();

	void setReturnType(TypeName aTypeName);
}
