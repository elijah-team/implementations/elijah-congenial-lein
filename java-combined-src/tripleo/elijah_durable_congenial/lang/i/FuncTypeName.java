package tripleo.elijah_durable_congenial.lang.i;

import java.io.File;

public interface FuncTypeName extends TypeName {
	void argList(FormalArgList op);

	void argList(TypeNameList tnl);

	@Override
	int getColumn();

	@Override
	int getColumnEnd();

	@Override
	File getFile();

	@Override
	int getLine();
	@Override
	int getLineEnd();

@Override
	Context getContext();

	@Override
	void setContext(Context context);

	@Override
	boolean isNull();
	@Override
	Type kindOfType();

	boolean argListIsGeneric();

void returnValue(TypeName rtn);

	// @Override
	void type(TypeModifiers typeModifiers);








}
