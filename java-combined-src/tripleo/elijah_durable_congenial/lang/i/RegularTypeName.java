package tripleo.elijah_durable_congenial.lang.i;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.lang.impl.RegularTypeNameImpl;
import tripleo.elijah_durable_congenial.util.Helpers;

public interface RegularTypeName extends NormalTypeName {
	/*
	 * Null context. Possibly only for testing.
	 */
	static @NotNull RegularTypeName makeWithStringTypeName(@NotNull String aTypeName) {
		final RegularTypeName R = new RegularTypeNameImpl(null);
		R.setName(Helpers.string_to_qualident(aTypeName));
		return R;
	}

	void addGenericPart(TypeNameList tn2);

	TypeNameList getGenericPart();

	String getName();

	void setName(Qualident aS);

	Qualident getRealName();

	Context getContext();

	Type kindOfType();

	void setContext(Context ctx);

	@Override
	OS_Element getResolvedElement();

	@Override
	void setResolvedElement(OS_Element element);

	@Override
	boolean hasResolvedElement();

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	String toString();
}
