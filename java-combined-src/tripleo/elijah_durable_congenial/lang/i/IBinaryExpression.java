package tripleo.elijah_durable_congenial.lang.i;

public interface IBinaryExpression extends IExpression {

	IExpression getRight();

	void setRight(IExpression right);

	void set(IBinaryExpression ex);

}
