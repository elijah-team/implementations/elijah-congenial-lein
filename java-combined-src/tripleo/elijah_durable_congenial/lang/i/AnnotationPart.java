package tripleo.elijah_durable_congenial.lang.i;

public interface AnnotationPart {
	Qualident annoClass();

	ExpressionList getExprs();

	void setExprs(ExpressionList el);

	void setClass(Qualident q);
}
