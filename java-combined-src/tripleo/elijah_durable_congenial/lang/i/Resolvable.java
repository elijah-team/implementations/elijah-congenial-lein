package tripleo.elijah_durable_congenial.lang.i;

/**
 * Created 8/20/20 7:24 PM
 */
public interface Resolvable {
	OS_Element getResolvedElement();

	void setResolvedElement(OS_Element element);

	boolean hasResolvedElement();
}
