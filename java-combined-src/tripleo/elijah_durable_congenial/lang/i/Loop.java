package tripleo.elijah_durable_congenial.lang.i;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.contexts.LoopContext;
import tripleo.elijah_durable_congenial.lang2.ElElementVisitor;

import java.util.List;

public interface Loop extends StatementItem, FunctionItem, OS_Element {
	void expr(IExpression aExpr);

	void frompart(IExpression aExpr);

	@Override
	Context getContext();

	@Override
	OS_Element getParent();

	@Override
		// OS_Element
	void visitGen(ElElementVisitor visit);

	void setContext(LoopContext ctx);

	@NotNull
	IExpression getFromPart();

	List<StatementItem> getItems();

	String getIterName();

	IdentExpression getIterNameToken();

	@NotNull
	IExpression getToPart();

	LoopTypes getType();

	void iterName(IdentExpression s);

	void scope(Scope3 aSco);

	void topart(IExpression aExpr);

	void type(LoopTypes aType);

	@Override
	default void serializeTo(SmallWriter sw) {

	}
}
