package tripleo.elijah_durable_congenial.lang.i;

import antlr.Token;

public interface GetItemExpression extends IExpression {
	/*
	 * (non-Javadoc)
	 *
	 * @see tripleo.elijah.lang.impl.IExpression#getKind()
	 */
	@Override
	ExpressionKind getKind();

	@Override
	OS_Type getType();

	@Override
	void setType(OS_Type deducedExpression);

	/*
	 * (non-Javadoc)
	 *
	 * @see tripleo.elijah.lang.impl.IExpression#is_simple()
	 */
	@Override
	boolean is_simple();

	IExpression index();

	void parens(Token lb, Token rb);
}
