package tripleo.elijah_durable_congenial.lang.i;

public interface StringExpression extends IExpression {
	@Override
	ExpressionKind getKind();

	@Override
	IExpression getLeft();

	@Override
	void setLeft(IExpression iexpression);

	@Override
	OS_Type getType();

	@Override
	void setType(OS_Type deducedExpression);

	@Override
	boolean is_simple();

	@Override
	String repr_();

	String getText();

	void set(String g);

	@Override
	String toString();
}
