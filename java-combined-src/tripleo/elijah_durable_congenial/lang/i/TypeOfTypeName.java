package tripleo.elijah_durable_congenial.lang.i;

import tripleo.elijah_durable_congenial.stages.deduce.DeduceTypes2;
import tripleo.elijah_durable_congenial.stages.deduce.ResolveError;

import java.io.File;

public interface TypeOfTypeName extends TypeName {
	// TODO what about keyword
	int getColumn();

	int getColumnEnd();

	File getFile();

	// TODO what about keyword
	int getLine();

	int getLineEnd();

	Context getContext();

	void setContext(Context context);

	boolean isNull();

	Type kindOfType();

	TypeName resolve(Context ctx, DeduceTypes2 deduceTypes2) throws ResolveError;

	void set(TypeModifiers modifiers_);

	Qualident typeOf();

	void typeOf(Qualident xy);
}
