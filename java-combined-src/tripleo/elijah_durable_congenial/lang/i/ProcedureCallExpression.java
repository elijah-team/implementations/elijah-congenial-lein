package tripleo.elijah_durable_congenial.lang.i;

public interface ProcedureCallExpression extends IExpression {
	ExpressionList exprList();

	ExpressionList getArgs();

	void setArgs(ExpressionList aExpl);

	@Override
	OS_Type getType();

	void identifier(IExpression ee);

	String printableString();

	@Override
	String toString();
}
