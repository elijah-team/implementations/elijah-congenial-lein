package tripleo.elijah_durable_congenial.lang.i;

import tripleo.elijah.diagnostic.Locatable;

public interface NumericExpression extends IExpression, Locatable {
	@Override
		// IExpression
	ExpressionKind getKind();

	@Override
		// IExpression
	void setKind(ExpressionKind aType);

	@Override
	IExpression getLeft();

	@Override
	void setLeft(IExpression aLeft);

	@Override
		// IExpression
	OS_Type getType();

	@Override
		// IExpression
	void setType(OS_Type deducedExpression);

	@Override
	boolean is_simple();

	@Override
	String repr_();

	@Override
	int getLine();

	int getValue();

	@Override
	String toString();
}
