package tripleo.elijah_durable_congenial.lang.i;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface VariableTypeName extends TypeName {
	void addGenericPart(TypeNameList tn2);

	@Override
	boolean equals(Object o);

	@Override
	Context getContext();

	@Override
	void setContext(Context ctx);

	@Override
	boolean isNull();

	@Override
	Type kindOfType();

	TypeNameList getGenericPart();

	@NotNull
	Collection<TypeModifiers> getModifiers();

	Qualident getRealName();

	OS_Element getResolvedElement();

	void setResolvedElement(OS_Element element);

	@Override
	int hashCode();

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	/* #@ requires pr_name != null; */
	// pr_name is null when first created
	@Override
	String toString();

	boolean hasResolvedElement();

	void setName(Qualident aQualident);
}
