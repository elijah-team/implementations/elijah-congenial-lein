package tripleo.elijah_durable_congenial.lang.i;

import java.io.File;

public interface GenericTypeName extends TypeName {
	int getColumn();

	int getColumnEnd();

	File getFile();
	int getLine();

int getLineEnd();

	Context getContext();
void setContext(Context context);

	boolean isNull();
Type kindOfType();

	void set(TypeModifiers modifiers_);
void setConstraint(TypeName aConstraint);

	void typeName(Qualident xy);








}
