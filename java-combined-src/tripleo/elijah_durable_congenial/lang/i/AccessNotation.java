package tripleo.elijah_durable_congenial.lang.i;

import antlr.Token;
import tripleo.elijah_durable_congenial.lang2.ElElementVisitor;

public interface AccessNotation extends OS_Element {
	El_Category getCategory();

	void setCategory(Token category);

	@Override
	Context getContext();

	@Override
	OS_Element getParent();

	@Override
	void visitGen(ElElementVisitor visit);

	@Override
	default void serializeTo(SmallWriter sw) {

	}

	void setShortHand(Token shorthand);

	void setTypeNames(TypeNameList tnl);
}
