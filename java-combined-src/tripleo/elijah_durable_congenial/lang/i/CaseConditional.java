package tripleo.elijah_durable_congenial.lang.i;

import tripleo.elijah_durable_congenial.contexts.CaseContext;
import tripleo.elijah_durable_congenial.lang.impl.CaseConditionalImpl;
import tripleo.elijah_durable_congenial.lang2.ElElementVisitor;

import java.util.HashMap;

public interface CaseConditional extends OS_Element, StatementItem, FunctionItem {
	void addScopeFor(IExpression expression, CaseConditional caseScope);

	void expr(IExpression expr);

	@Override
	Context getContext();

	@Override
	OS_Element getParent();

	@Override
	void visitGen(ElElementVisitor visit);

	@Override
	default void serializeTo(SmallWriter sw) {

	}

	void setContext(CaseContext ctx);

	IExpression getExpr();

	HashMap<IExpression, CaseConditionalImpl.CaseScopeImpl> getScopes();

	void postConstruct();

	void scope(Scope3 aSco, IExpression aExpr1);

	void setDefault();

	interface CaseScope extends OS_Element {
	}
}
