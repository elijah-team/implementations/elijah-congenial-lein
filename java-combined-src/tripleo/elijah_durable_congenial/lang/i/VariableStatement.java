package tripleo.elijah_durable_congenial.lang.i;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.diagnostic.Locatable;
import tripleo.elijah_durable_congenial.lang2.ElElementVisitor;

public interface VariableStatement extends @NotNull Locatable, OS_Element {
	@Override
	Context getContext();

	@Override
	void visitGen(ElElementVisitor visit);

	@Override
	default void serializeTo(SmallWriter sw) {

	}

	String getName();

	void setName(IdentExpression s);

	IdentExpression getNameToken();

	TypeModifiers getTypeModifiers();

	void initial(IExpression aExpr);

	@NotNull
	IExpression initialValue();

	void set(TypeModifiers y);

	void setTypeName(@NotNull TypeName tn);

	@NotNull
	TypeName typeName();
}
