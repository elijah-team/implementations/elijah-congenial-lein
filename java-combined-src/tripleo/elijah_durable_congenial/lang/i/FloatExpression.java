package tripleo.elijah_durable_congenial.lang.i;

public interface FloatExpression extends IExpression {
	@Override
	ExpressionKind getKind();

	@Override
	void setKind(ExpressionKind aType);

	@Override
	IExpression getLeft();

	@Override
	void setLeft(IExpression aLeft);

	@Override
	OS_Type getType();

	@Override
	void setType(OS_Type deducedExpression);

	@Override
	boolean is_simple();

	@Override
	String repr_();

	@Override
	String toString();
}
