package tripleo.elijah_durable_congenial.lang.i;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.ci.LibraryStatementPart;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.contexts.ModuleContext;
import tripleo.elijah_durable_congenial.entrypoints.EntryPoint;
import tripleo.elijah_durable_congenial.lang2.ElElementVisitor;

import java.util.Collection;
import java.util.List;

public interface OS_Module extends OS_Element {
	void add(ModuleItem anElement);

	@NotNull List<EntryPoint> entryPoints();

	@Nullable OS_Element findClass(String aClassName);

	void finish();

	@NotNull Compilation getCompilation();

	@Override Context getContext();

	@Override @Nullable OS_Element getParent();

	@Override void visitGen(@NotNull ElElementVisitor visit);

	@Override void serializeTo(SmallWriter sw);

	void setParent(@NotNull Compilation parent);

	void setContext(ModuleContext mctx);

	String getFileName();

	void setFileName(String fileName);

	LibraryStatementPart getLsp();

	void setLsp(@NotNull LibraryStatementPart lsp);

	boolean hasClass(String className); // OS_Container

	boolean isPrelude();

	void setPrelude(OS_Module success);

	void postConstruct();

	OS_Module prelude();

	OS_Package pullPackageName();

	OS_Package pushPackageNamed(Qualident aPackageName);

	void setIndexingStatement(IndexingStatement idx);

	@NotNull Collection<ModuleItem> getItems();
}
