package tripleo.elijah_durable_congenial.lang.i;

public interface FunctionBody {
	void addPostCondition(Postcondition aPostcondition);

	void addPreCondition(Precondition aPrecondition);

	boolean getAbstract();

	void setAbstract(boolean aAbstract);

	Scope3 scope3();

	void setScope3(Scope3 aSc);
}
