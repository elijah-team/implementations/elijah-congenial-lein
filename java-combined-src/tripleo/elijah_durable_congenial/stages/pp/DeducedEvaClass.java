package tripleo.elijah_durable_congenial.stages.pp;

import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;

public interface DeducedEvaClass {
	IEvaClass getCarrier();
}
