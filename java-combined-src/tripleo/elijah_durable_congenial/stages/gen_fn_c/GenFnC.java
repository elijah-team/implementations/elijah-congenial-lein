package tripleo.elijah_durable_congenial.stages.gen_fn_c;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.PipelineLogic;
import tripleo.elijah_congenial.pipelines.eva.FunctionStatement;
import tripleo.elijah_durable_congenial.comp.i.IPipelineAccess;
import tripleo.elijah_durable_congenial.entrypoints.ArbitraryFunctionEntryPoint;
import tripleo.elijah_durable_congenial.entrypoints.EntryPoint;
import tripleo.elijah_durable_congenial.entrypoints.MainClassEntryPoint;
import tripleo.elijah_durable_congenial.pre_world.Mirror_ArbitraryFunctionEntryPoint;
import tripleo.elijah_durable_congenial.pre_world.Mirror_EntryPoint;
import tripleo.elijah_durable_congenial.pre_world.Mirror_MainClassEntryPoint;
import tripleo.elijah_durable_congenial.stages.gen_fn.GenerateFunctions;
import tripleo.elijah_durable_congenial.stages.gen_fn.GeneratePhase;
import tripleo.elijah_durable_congenial.stages.gen_fn.IClassGenerator;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaFunction;
import tripleo.elijah_durable_congenial.stages.logging.ElLog;

public class GenFnC {
	private IPipelineAccess pa;
	private PipelineLogic   pipelineLogic;

	public void set(final IPipelineAccess aPa0) {
		this.pa = aPa0;
	}

	public void set(final PipelineLogic aPl) {
		pipelineLogic = aPl;
	}

	public void addLog(final ElLog aLOG) {
		pa.addLog(aLOG);
	}

	public void addEntryPoint(final EntryPoint aEntryPoint,
	                          final IClassGenerator aDcg,
							  final GenerateFunctions gf) {
		addEntryPoint(getMirrorEntryPoint(aEntryPoint, gf), aDcg);
	}

	public void addEntryPoint(final Mirror_EntryPoint aMirrorEntryPoint, final IClassGenerator aDcg) {
		pa.getCompilationEnclosure().addEntryPoint(aMirrorEntryPoint, aDcg);
	}

	@NotNull
	private Mirror_EntryPoint getMirrorEntryPoint(final EntryPoint entryPoint, final GenerateFunctions gf) {
		final Mirror_EntryPoint m;
		if (entryPoint instanceof final @NotNull MainClassEntryPoint mcep) {
			m = new Mirror_MainClassEntryPoint(mcep, gf);
		} else if (entryPoint instanceof final @NotNull ArbitraryFunctionEntryPoint afep) {
			m = new Mirror_ArbitraryFunctionEntryPoint(afep, gf);
		} else {
			throw new IllegalStateException("unhandled");
		}
		return m;
	}

	public GeneratePhase getGeneratePhase() {
		return pipelineLogic.generatePhase;
	}

	public void addFunctionStatement(final IEvaFunction aGf) {
		pa.addFunctionStatement(new FunctionStatement(aGf));
	}
}
