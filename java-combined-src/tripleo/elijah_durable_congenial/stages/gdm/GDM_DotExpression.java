package tripleo.elijah_durable_congenial.stages.gdm;

import org.jdeferred2.DoneCallback;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.lang.i.DotExpression;
import tripleo.elijah_durable_congenial.stages.gen_fn.GenerateFunctions;
import tripleo.elijah_durable_congenial.stages.gen_fn.IdentTableEntry;
import tripleo.elijah.util.Eventual;

import java.util.function.Consumer;

public class GDM_DotExpression implements GDM_Item {
	private final GenerateFunctions         generateFunctions;
	private final DotExpression             dotExpression;
	private final Eventual<Object>                                        _p_DeduceTypes2 = new Eventual<>();
	private       Eventual<IdentTableEntry> _p_IdentTableEntry = new Eventual<>();

	public GDM_DotExpression(final GenerateFunctions aGenerateFunctions, final DotExpression aDotExpression) {
		generateFunctions = aGenerateFunctions;
		dotExpression     = aDotExpression;
	}

	public void onIdentTableEntry(final Consumer<IdentTableEntry> ic) {
		_p_IdentTableEntry.then(ic::accept);
	}

	public void resolveIdentTableEntry(final IdentTableEntry ite) {
		_p_IdentTableEntry.resolve(ite);
	}

	@Override
	public void resolveDeduceTypes2(final Object aDeduceTypes2) {
		_p_DeduceTypes2.resolve(aDeduceTypes2);
	}

	@Override public void onDeduceTypes2(final DoneCallback<@NotNull Object> cb) {
		_p_DeduceTypes2.then(cb);
	}
}
