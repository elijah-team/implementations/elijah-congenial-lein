package tripleo.elijah_durable_congenial.stages.gdm;

import org.jdeferred2.DoneCallback;
import org.jetbrains.annotations.NotNull;

public interface GDM_Item {
	void resolveDeduceTypes2(Object/*DeduceTypes2*/ aDeduceTypes2);

	void onDeduceTypes2(DoneCallback<@NotNull Object> cb);
}
