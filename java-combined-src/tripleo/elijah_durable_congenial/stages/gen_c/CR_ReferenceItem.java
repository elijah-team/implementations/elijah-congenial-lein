package tripleo.elijah_durable_congenial.stages.gen_c;

import tripleo.elijah.util.Eventual;
import tripleo.elijah.nextgen.outputstatement.EG_Statement;
import tripleo.elijah_durable_congenial.stages.gen_fn.BaseTableEntry;
import tripleo.elijah_durable_congenial.stages.instructions.InstructionArgument;
import tripleo.elijah.util.Operation2;

public interface CR_ReferenceItem {
	String getArg();

	void setArg(String aArg);

	CReference.Connector getConnector();

	void setConnector(CReference.Connector aConnector);

	GenerateC_Item getGenerateCItem();

	void setGenerateCItem(GenerateC_Item aGenerateCItem);

	InstructionArgument getInstructionArgument();

	void setInstructionArgument(InstructionArgument aInstructionArgument);

	Eventual<GenerateC_Item> getPrevious();

	void setPrevious(Eventual<GenerateC_Item> aPrevious);

	CReference.Reference getReference();

	void setReference(CReference.Reference aReference);

	String getText();

	void setText(String aText);

	Operation2<EG_Statement> getStatement();

	void setStatement(Operation2<EG_Statement> aStatement);

	BaseTableEntry getTableEntry();
}
