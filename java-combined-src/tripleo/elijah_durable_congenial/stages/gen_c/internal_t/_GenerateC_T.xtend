package tripleo.elijah.stages.gen_c.internal_t

import java.util.ArrayList
import java.util.Collection
import java.util.HashMap
import java.util.Map
import tripleo.elijah.stages.gen_c.DeducedBaseEvaFunction
import tripleo.elijah.stages.gen_c.WC
import tripleo.elijah.stages.gen_c.GenerateC
import tripleo.elijah.stages.gen_c.WhyNotGarish_Class
import tripleo.elijah.stages.gen_c.WhyNotGarish_Constructor
import tripleo.elijah.stages.gen_c.WhyNotGarish_Function
import tripleo.elijah.stages.gen_c.WhyNotGarish_Item
import tripleo.elijah.stages.gen_c.WhyNotGarish_Namespace
import tripleo.elijah.stages.gen_fn.EvaNode
import tripleo.elijah.stages.gen_fn.IBaseEvaFunction
import tripleo.elijah.stages.gen_fn.IEvaClass
import tripleo.elijah.stages.gen_fn.IEvaConstructor
import tripleo.elijah.stages.gen_fn.IEvaNamespace

abstract class _GenerateC_T {
	protected final Map<EvaNode, WhyNotGarish_Item> a_directory = new HashMap();
	
	def Collection<WhyNotGarish_Item> __directoryValues() { a_directory.values() }
	def Collection<WhyNotGarish_Item> __directoryValuesCopy() { new ArrayList(a_directory.values()) }
	
	def WhyNotGarish_Constructor a_lookup(IEvaConstructor aGf) {
	  if (a_directory.containsKey(aGf)) {
		return a_directory.get(aGf) as WhyNotGarish_Constructor;
	  }

	  var ncc = wc().newWhyNotGarish_Constructor(aGf, _this());
	  a_directory.put(aGf, ncc);
	  return ncc;
	}

	def WhyNotGarish_Function a_lookup(IBaseEvaFunction aGf) {
	  if (a_directory.containsKey(aGf)) {
		return a_directory.get(aGf) as WhyNotGarish_Function;
	  }

	  var ncf = wc().newWhyNotGarish_Function(aGf, _this());
	  a_directory.put(aGf, ncf);
	  return ncf;
	}

	def a_lookup(IEvaClass aGc) {
	  if (a_directory.containsKey(aGc)) {
		return a_directory.get(aGc) as WhyNotGarish_Class;
	  }

	  var nck = wc().newWhyNotGarish_Class(aGc, _this());
	  a_directory.put(aGc, nck);
	  return nck;
	}

	def a_lookup( IEvaNamespace en) {
	  if (a_directory.containsKey(en)) {
		return a_directory.get(en) as WhyNotGarish_Namespace;
	  }

	  var ncn = wc().newWhyNotGarish_Namespace(en, _this());
	  a_directory.put(en, ncn);
	  return ncn;
	}

	def abstract GenerateC _this()

  def abstract WC wc()

	def WhyNotGarish_Function a_lookup(DeducedBaseEvaFunction aGf) {
	  return a_lookup(aGf.getCarrier() as IBaseEvaFunction)
	}
}
