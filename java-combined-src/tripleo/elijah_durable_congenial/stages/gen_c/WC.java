package tripleo.elijah_durable_congenial.stages.gen_c;


import tripleo.elijah_durable_congenial.stages.gen_fn.*;

public interface WC {
	EvaConstructor newEvaConstructor();

	EvaNamespace newEvaNamespace();

	WhyNotGarish_Constructor newWhyNotGarish_Constructor(IEvaConstructor ec, GenerateC gc);
	WhyNotGarish_Function newWhyNotGarish_Function(IBaseEvaFunction ec, GenerateC gc);
	WhyNotGarish_Class newWhyNotGarish_Class(IEvaClass ec, GenerateC gc);
	WhyNotGarish_Namespace newWhyNotGarish_Namespace(IEvaNamespace ec, GenerateC gc);
}
