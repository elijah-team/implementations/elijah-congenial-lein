package tripleo.elijah_durable_congenial.stages.gen_c;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.jdeferred2.DoneCallback;
import tripleo.elijah.util.Eventual;
import tripleo.elijah_durable_congenial.lang.i.OS_Element;
import tripleo.elijah_durable_congenial.stages.gen_fn.EvaNode;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;
import tripleo.elijah_durable_congenial.stages.gen_fn.IdentTableEntry;
import tripleo.elijah_durable_congenial.stages.gen_fn.ProcTableEntry;

@EqualsAndHashCode
// @EqualsHashCode
//  https://eclipse.dev/Xtext/xtend/documentation/204_activeannotations.html#data-annotation
public final class GR_re_is_FunctionDef implements GRRR, GR_EvaNodeAble {
	@Getter
	private final ProcTableEntry  pte;
	@Getter
	private final IEvaClass       cheatClass;
	@Getter
	private final IdentTableEntry ite;
	@Getter
	private final CRI_Ident       cri_ident;
	private final OS_Element      repo_element;
	private final GI_FunctionDef  gi_item;

	private final Eventual<EvaNode> resolvedP = new Eventual<>();

	// TODO too many params make jack feel like a cheater
	public GR_re_is_FunctionDef(final ProcTableEntry aPte,
								final IEvaClass a_cheat,
								final IdentTableEntry aIte,
								final CRI_Ident aCRIIdent,
								final GI_FunctionDef aGiItem) {
		this.pte          = aPte;
		this.cheatClass   = a_cheat;
		this.ite          = aIte;
		this.cri_ident    = aCRIIdent;
		this.repo_element = ite.getResolvedElement();
		this.gi_item      = aGiItem;
	}

	@Override
	public void onResolve(final DoneCallback<EvaNode> cb) {
		gi_item.resolving(this);
		resolvedP.then(cb);
	}

	@Override
	public void reverseResolving(final Object aObject) {
		this.gi_item._re_is_FunctionDef(pte, cheatClass, ite, resolvedP);
	}
}
