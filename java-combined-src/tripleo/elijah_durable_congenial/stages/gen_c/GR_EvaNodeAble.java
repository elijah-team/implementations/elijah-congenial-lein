package tripleo.elijah_durable_congenial.stages.gen_c;

import org.jdeferred2.DoneCallback;

import tripleo.elijah_durable_congenial.stages.gen_fn.EvaNode;

public interface GR_EvaNodeAble {
	void onResolve(DoneCallback<EvaNode> cb);
}
