package tripleo.elijah_durable_congenial.stages.gen_generic;

public interface IGenerateResultWatcher {
	public void complete();

	public void item(GenerateResultItem item);
}