package tripleo.elijah_durable_congenial.stages.gen_generic;

import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.stages.gen_fn.GNCoded;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;
import tripleo.elijah_durable_congenial.world.i.LivingClass;

public interface IREvaClass extends GNCoded {
	ClassStatement getKlass();
IEvaClass getEvaClass();@Override
	int getCode();

	@Override
	void setCode(int aI);
		@Override
	default Role getRole() {
		return Role.CLASS;
	}

void setLiving(LivingClass aLivingClass);








}
