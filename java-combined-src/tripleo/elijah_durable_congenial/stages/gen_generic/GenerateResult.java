package tripleo.elijah_durable_congenial.stages.gen_generic;

import io.reactivex.rxjava3.core.Observer;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.ci.LibraryStatementPart;
import tripleo.elijah_durable_congenial.stages.gen_c.OutputFileC;
import tripleo.elijah_durable_congenial.stages.gen_fn.EvaClass;
import tripleo.elijah_durable_congenial.stages.gen_fn.EvaNamespace;
import tripleo.elijah_durable_congenial.stages.gen_fn.EvaNode;
import tripleo.elijah_durable_congenial.stages.pp.IPP_Function;
import tripleo.elijah_durable_congenial.stages.pp.PP_Constructor;
import tripleo.util.buffer.Buffer;

import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public interface GenerateResult {

	void close();

	void add(Buffer b, EvaNode n, TY ty, LibraryStatementPart aLsp, Dependency d);

	void addClass(TY ty, EvaClass aClass, Buffer aBuf, LibraryStatementPart aLsp);

	/* (non-Javadoc)
	 * @see tripleo.elijah.stages.gen_generic.GenerateResult#addFunction(tripleo.elijah.stages.gen_fn.tripleo.elijah.stages.gen_fn.IBaseEvaFunction, tripleo.util.buffer.Buffer, tripleo.elijah.stages.gen_generic.Old_GenerateResult.TY, tripleo.elijah.ci.LibraryStatementPart)
	 */
	void addFunction(IPP_Function aGeneratedFunction, @NotNull Buffer aBuffer, @NotNull TY aTY, LibraryStatementPart aLsp);

	void additional(GenerateResult aGenerateResult);

	void addConstructor(PP_Constructor aEvaConstructor, Buffer aBuffer, TY aTY, LibraryStatementPart aLsp);

	void completeItem(GenerateResultItem aGenerateResultItem);

	void addNamespace(TY ty, EvaNamespace aNamespace, Buffer aBuf, LibraryStatementPart aLsp);

	void addWatcher(IGenerateResultWatcher w);

	void observe(Observer<GenerateResultItem> obs);

	void signalDone();

	void outputFiles(Consumer<Map<String, OutputFileC>> cmso);

	List<Old_GenerateResultItem> results();

	void signalDone(Map<String, OutputFileC> aOutputFiles);

	void subscribeCompletedItems(Observer<GenerateResultItem> aGenerateResultItemObserver);

	public enum TY {
		HEADER, IMPL, PRIVATE_HEADER
	}
}
