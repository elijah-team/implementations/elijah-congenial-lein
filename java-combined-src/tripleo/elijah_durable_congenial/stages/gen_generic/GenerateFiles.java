package tripleo.elijah_durable_congenial.stages.gen_generic;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.work.WorkList;
import tripleo.elijah.work.WorkManager;
import tripleo.elijah_durable_congenial.stages.gen_fn.EvaNode;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaConstructor;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaFunction;
import tripleo.elijah_durable_congenial.stages.gen_generic.pipeline_impl.GenerateResultSink;
import tripleo.elijah_durable_congenial.stages.logging.ElLog;
import tripleo.elijah_durable_congenial.stages.pp.IPP_Constructor;
import tripleo.elijah_durable_congenial.stages.pp.IPP_Function;

import java.util.Collection;
import java.util.List;

public interface GenerateFiles extends CodeGenerator {
	@NotNull
	static Collection<EvaNode> classes_to_list_of_generated_nodes(@NotNull Collection<IEvaClass> aEvaClasses) {
		return Collections2.transform(aEvaClasses, new Function<IEvaClass, EvaNode>() {
			@org.checkerframework.checker.nullness.qual.Nullable
			@Override
			public @Nullable EvaNode apply(@org.checkerframework.checker.nullness.qual.Nullable IEvaClass input) {
				return input;
			}
		});
	}

	@NotNull
	static Collection<EvaNode> constructors_to_list_of_generated_nodes(@NotNull Collection<IEvaConstructor> aEvaConstructors) {
		return Collections2.transform(aEvaConstructors, new Function<IEvaConstructor, EvaNode>() {
			@org.checkerframework.checker.nullness.qual.Nullable
			@Override
			public @Nullable EvaNode apply(@org.checkerframework.checker.nullness.qual.Nullable IEvaConstructor input) {
				return input;
			}
		});
	}

	@NotNull
	static Collection<EvaNode> functions_to_list_of_generated_nodes(@NotNull Collection<IEvaFunction> generatedFunctions) {
		return Collections2.transform(generatedFunctions, new Function<IEvaFunction, EvaNode>() {
			@org.checkerframework.checker.nullness.qual.Nullable
			@Override
			public @Nullable EvaNode apply(@org.checkerframework.checker.nullness.qual.Nullable IEvaFunction input) {
				return input;
			}
		});
	}

	void generate_constructor(IPP_Constructor aGf, GenerateResult aGr, WorkList aWl, GenerateResultSink aResultSink, final WorkManager aWorkManager, final @NotNull GenerateResultEnv aFileGen);

	void generate_function(IPP_Function aEvaFunction, GenerateResult aGenerateResult, WorkList aWorkList, GenerateResultSink aResultSink);

	GenerateResult generateCode(Collection<EvaNode> lgn, @NotNull GenerateResultEnv aFileGen);

	<T> GenerateResultEnv getFileGen();

	GenerateResult resultsFromNodes(@NotNull List<EvaNode> aNodes, WorkManager wm, GenerateResultSink grs, @NotNull GenerateResultEnv fg);

	ElLog elLog();

	void finishUp(final GenerateResult aGenerateResult, final WorkManager wm, final WorkList aWorkList);
}
