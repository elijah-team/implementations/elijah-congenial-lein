package tripleo.elijah_durable_congenial.stages.deduce;

import tripleo.elijah_durable_congenial.stages.deduce.tastic.ITastic;

public interface ITasticMap {

	boolean containsKey(Object aO);

	ITastic get(Object aO);

	void put(Object aO, ITastic aR);

}
