package tripleo.elijah_durable_congenial.stages.deduce;

import tripleo.elijah_durable_congenial.comp.i.ErrSink;
import tripleo.elijah_durable_congenial.stages.logging.ElLog;

public // 00

record DT_Env(ElLog LOG, ErrSink errSink, DeduceCentral central) {
}
