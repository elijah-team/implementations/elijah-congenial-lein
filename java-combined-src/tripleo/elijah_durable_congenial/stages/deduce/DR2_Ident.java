package tripleo.elijah_durable_congenial.stages.deduce;

import tripleo.elijah_durable_congenial.lang.i.IdentExpression;

public interface DR2_Ident {
	IdentExpression getEva();
}
