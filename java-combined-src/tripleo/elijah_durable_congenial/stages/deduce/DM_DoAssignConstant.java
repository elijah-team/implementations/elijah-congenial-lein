package tripleo.elijah_durable_congenial.stages.deduce;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.stages.gen_fn.IdentTableEntry;
import tripleo.elijah_durable_congenial.stages.instructions.ConstTableIA;
import tripleo.elijah_durable_congenial.stages.instructions.Instruction;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;

public record DM_DoAssignConstant(
		@NotNull IBaseEvaFunction generatedFunction,
		@NotNull Instruction instruction,
		@NotNull IdentTableEntry idte,
		@NotNull ConstTableIA constTableIA
) {
}
