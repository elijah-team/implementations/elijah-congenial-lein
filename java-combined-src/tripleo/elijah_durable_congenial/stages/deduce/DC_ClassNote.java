package tripleo.elijah_durable_congenial.stages.deduce;

import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.lang.i.Context;

import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;
import tripleo.elijah_durable_congenial.stages.gen_fn.IdentTableEntry;

class DC_ClassNote {
	private final DeduceCentral    central;
	private final Context          ctx;
	private final ClassStatement   e;
	private       DC_ClassNote_DT2 dc_classNote_dt2;

	public DC_ClassNote(final ClassStatement aE, final Context aCtx, final DeduceCentral aCentral) {
		e       = aE;
		ctx     = aCtx;
		central = aCentral;
	}

	public void attach(final IdentTableEntry aIte, final IBaseEvaFunction aGeneratedFunction) {
		final DeduceTypes2 deduceTypes2 = central.getDeduceTypes2();
		dc_classNote_dt2 = deduceTypes2._inj().new_DC_ClassNote_DT2(aIte, aGeneratedFunction, deduceTypes2);
	}

	static class DC_ClassNote_DT2 {
		private final DeduceTypes2    deduceTypes2;
		private final IBaseEvaFunction generatedFunction;
		private final IdentTableEntry ite;

		public DC_ClassNote_DT2(final IdentTableEntry aIte, final IBaseEvaFunction aGeneratedFunction, final DeduceTypes2 aDeduceTypes2) {
			ite               = aIte;
			generatedFunction = aGeneratedFunction;
			deduceTypes2      = aDeduceTypes2;
		}

		public DeduceTypes2 getDeduceTypes2() {
			return deduceTypes2;
		}

		public IBaseEvaFunction getGeneratedFunction() {
			return generatedFunction;
		}

		public IdentTableEntry getIte() {
			return ite;
		}
	}
}
