package tripleo.elijah_durable_congenial.stages.deduce;

import com.google.common.collect.ImmutableList;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.stages.gen_fn.EvaClass;
import tripleo.elijah_durable_congenial.stages.gen_fn.EvaNode;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaClass;
import tripleo.elijah.util.ProgramMightBeWrongIfYouAreHere;
import tripleo.elijah_congenial.deduce.t.TEvaClass;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GeneratedClasses implements Iterable<EvaNode> {
	private final DeducePhase         deducePhase;
	private final List<DP_MutEvaNode> mens = new ArrayList<>();
	private       int                 generation;

	public GeneratedClasses(final DeducePhase aDeducePhase) {
		deducePhase = aDeducePhase;
	}

	@Override
	public String toString() {
		return "GeneratedClasses{size=%d, generation=%d}".formatted(getGeneratedClasses().size(), generation);
	}

	@NotNull
	public List<EvaNode> getGeneratedClasses() {
		return mens.stream().map(x -> ((EvaNode) x.getTClass().getEvaClass())).toList();
	}

	public void add(EvaNode aClass) {
		final TEvaClass tClass;

		if (aClass instanceof EvaClass) {
			tClass = new TEvaClass((IEvaClass) aClass);
		} else if (aClass instanceof @NotNull TEvaClass tc) {
			tClass = tc;
		} else throw new ProgramMightBeWrongIfYouAreHere();

		deducePhase.pa._send_GeneratedClass(tClass);

		mens.add(new DP_MutEvaNode() {
			private boolean _resolved_var_table_entries;

			@Override
			public boolean resolved_var_table_entries() {
				return this._resolved_var_table_entries;
			}

			@Override
			public void resolve_var_table_entries(final DeducePhase aDeducePhase) {
				assert !_resolved_var_table_entries;
			}

			@Override
			public TEvaClass getTClass() {
				return tClass;
			}
		});
	}

	public @NotNull List<EvaNode> copy() {
		++generation;
		return ImmutableList.<EvaNode>copyOf(getGeneratedClasses());
	}

	@Override
	public @NotNull Iterator<EvaNode> iterator() {
		return getGeneratedClasses().iterator();
	}

	public int size() {
		return mens.size();
	}

	public Iterable<DP_MutEvaNode> mutCopy() {
		return mens;
	}
}
