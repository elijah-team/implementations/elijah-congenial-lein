package tripleo.elijah_durable_congenial.stages.deduce;

import tripleo.elijah.util.UnintendedUseException;
import tripleo.elijah.util.Ok;
import tripleo.elijah.util.Operation;

class Times {
	public interface T {
		Operation<Ok> call(); // ??
	}

	public static class Once implements T {
		@Override
		public Operation<Ok> call() {
			throw new UnintendedUseException();
			//return null;
		}
	}
}
