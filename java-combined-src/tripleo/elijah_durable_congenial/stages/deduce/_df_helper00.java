package tripleo.elijah_durable_congenial.stages.deduce;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah_durable_congenial.stages.gen_fn.*;

import java.util.Collection;
import java.util.List;

class _df_helper00 {
	/**
	 * Deduce functions or constructors contained in classes list
	 *
	 * @param aGeneratedClasses assumed to be a list of {@link EvaContainerNC}
	 * @param dfhi              specifies what to select for:<br>
	 *                          {@link #dfhi_functions} will select all functions from {@code functionMap}, and <br>
	 *                          {@link #dfhi_constructors} will select all constructors from {@code constructors}.
	 * @param <T>               generic parameter taken from {@code dfhi}
	 * @return the number of deduced functions or constructors, or 0
	 */
	public static <T> int df_helper(@NotNull Iterable<EvaNode> aGeneratedClasses, @NotNull df_helper_i<T> dfhi, DeduceTypes2 dt2) {
		final Sizer sizer = new Sizer();

		for (EvaNode evaNode : aGeneratedClasses) {
			evaNode.withEvaContainerNC((@NotNull EvaContainerNC generatedContainerNC) -> {
				final @Nullable df_helper<T> dfh = dfhi.get(generatedContainerNC);
				if (dfh != null) {
					@NotNull Collection<T> lgf2 = dfh.collection();
					for (final T generatedConstructor : lgf2) {
						if (dfh.deduce(generatedConstructor, dt2))
							sizer.increment();
					}
				}
			});
		}
		return sizer.size();
	}

	public static void deduceFunctions(final @NotNull Iterable<IEvaFunction> lgf, final DeduceTypes2 deduceTypes2) {
		for (final EvaNode evaNode : lgf) {
			if (evaNode instanceof @NotNull final EvaFunction generatedFunction) {
				deduceTypes2.deduceOneFunction(generatedFunction, deduceTypes2.phase);
			}
		}
		@NotNull List<EvaNode> generatedClasses = (deduceTypes2.phase.generatedClasses.copy());
		// TODO consider using reactive here
		int size;
		do {
			size             = df_helper(generatedClasses, new dfhi_functions());
			generatedClasses = deduceTypes2.phase.generatedClasses.copy();
		} while (size > 0);
		do {
			size             = df_helper(generatedClasses, new dfhi_constructors());
			generatedClasses = deduceTypes2.phase.generatedClasses.copy();
		} while (size > 0);
	}

	/**
	 * Deduce functions or constructors contained in classes list
	 *
	 * @param aGeneratedClasses assumed to be a list of {@link EvaContainerNC}
	 * @param dfhi              specifies what to select for:<br>
	 *                          {@link dfhi_functions} will select all functions from {@code functionMap}, and <br>
	 *                          {@link dfhi_constructors} will select all constructors from {@code constructors}.
	 * @param <T>               generic parameter taken from {@code dfhi}
	 * @return the number of deduced functions or constructors, or 0
	 */
	static <T> int df_helper(@NotNull List<EvaNode> aGeneratedClasses, @NotNull df_helper_i<T> dfhi) {
		final Sizer sizer = new Sizer();

		for (EvaNode evaNode : aGeneratedClasses) {
			evaNode.withEvaContainerNC((@NotNull EvaContainerNC generatedContainerNC) -> {
				final @Nullable df_helper<T> dfh = dfhi.get(generatedContainerNC);
				if (dfh != null) {
					@NotNull Collection<T> lgf2 = dfh.collection();
					for (final T generatedConstructor : lgf2) {
						if (dfh.deduce(generatedConstructor, null))
							sizer.increment();
					}
				}
			});
		}
		return sizer.size();
	}

	interface df_helper_i<T> {
		@Nullable df_helper<T> get(EvaContainerNC generatedClass);
	}

	interface df_helper<T> {
		@NotNull Collection<T> collection();

		boolean deduce(T generatedConstructor, DeduceTypes2 dt2);
	}

	static class dfhi_constructors implements df_helper_i<IEvaConstructor> {
		@Override
		public @Nullable df_helper_Constructors get(EvaContainerNC aGeneratedContainerNC) {
			if (aGeneratedContainerNC instanceof EvaClass) // TODO namespace constructors
				return new df_helper_Constructors((EvaClass) aGeneratedContainerNC);
			else
				return null;
		}
	}

	static class dfhi_functions implements df_helper_i<IEvaFunction> {
		@Override
		public @NotNull df_helper_Functions get(EvaContainerNC aGeneratedContainerNC) {
			return new df_helper_Functions(aGeneratedContainerNC);
		}
	}

	static class Sizer {
		int size = 0;

		public void increment() {
			size++;
		}

		public int size() {
			return size;
		}
	}

	static class df_helper_Functions implements df_helper<IEvaFunction> {
		private final EvaContainerNC generatedContainerNC;

		public df_helper_Functions(EvaContainerNC aGeneratedContainerNC) {
			generatedContainerNC = aGeneratedContainerNC;
		}

		@Override
		public @NotNull Collection<IEvaFunction> collection() {
			return generatedContainerNC.functionMap.values();
		}

		@Override
		public boolean deduce(@NotNull IEvaFunction aGeneratedFunction, DeduceTypes2 dt2) {
			return dt2.deduceOneFunction(aGeneratedFunction, dt2.phase);
		}
	}

	static class df_helper_Constructors implements df_helper<IEvaConstructor> {
		private final EvaClass evaClass;

		public df_helper_Constructors(EvaClass aEvaClass) {
			evaClass = aEvaClass;
		}

		@Override
		public @NotNull Collection<IEvaConstructor> collection() {
			return evaClass.constructors.values();
		}

		@Override
		public boolean deduce(final IEvaConstructor generatedConstructor, DeduceTypes2 dt2) {
			return dt2.deduceOneConstructor(generatedConstructor, dt2.phase);
		}
	}

}
