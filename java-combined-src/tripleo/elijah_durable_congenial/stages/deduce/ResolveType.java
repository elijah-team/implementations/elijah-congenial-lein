package tripleo.elijah_durable_congenial.stages.deduce;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.util.NotImplementedException;
import tripleo.elijah_durable_congenial.contexts.ClassContext;
import tripleo.elijah_durable_congenial.lang.i.*;
import tripleo.elijah_durable_congenial.lang.impl.AliasStatementImpl;
import tripleo.elijah_durable_congenial.lang.types.OS_AnyType;
import tripleo.elijah_durable_congenial.lang.types.OS_GenericTypeNameType;
import tripleo.elijah_durable_congenial.stages.gen_fn.GenType;
import tripleo.elijah_durable_congenial.stages.gen_fn.GenTypeImpl;
import tripleo.elijah_durable_congenial.stages.logging.ElLog;

import java.util.function.Consumer;

/**
 * Created 11/18/21 10:51 PM
 */
public enum ResolveType {
	;

	private final ResolveTypeInjector __inj = new ResolveTypeInjector();

	public static @NotNull GenType resolve_type(final @NotNull OS_Module module,
	                                            final @NotNull OS_Type type,
	                                            final Context ctx,
	                                            final @NotNull ElLog LOG,
	                                            final @NotNull DeduceTypes2 dt2) throws ResolveError {
		@NotNull GenType R = new GenTypeImpl();
		if (type.getType() != OS_Type.Type.USER_CLASS)
			R.setTypeName(type);

		switch (type.getType()) {

		case BUILT_IN:
			resolve_built_in(module, type, dt2, R);
			break;
		case USER:
			resolve_user2(type, LOG, dt2, R::copy);
			break;
		case USER_CLASS:
			R.setResolved(type);
			break;
		case FUNCTION:
			break;
		case FUNC_EXPR:
			R.setResolved(type);//((OS_FuncExprType)type).getElement();
			break;
		default:
			throw new IllegalStateException("565 Unexpected value: " + type.getType());
		}

		return R;
	}

	private static void resolve_built_in(final @NotNull OS_Module module, final @NotNull OS_Type type, final @NotNull DeduceTypes2 dt2, final @NotNull GenType aR) throws ResolveError {
		switch (type.getBType()) {
		case SystemInteger -> {
			@NotNull String typeName = type.getBType().name();
			assert typeName.equals("SystemInteger");
			OS_Module prelude = module.prelude();
			if (prelude == null) // README Assume `module' IS prelude
				prelude = module;
			final LookupResultList lrl  = prelude.getContext().lookup(typeName);
			@Nullable OS_Element   best = lrl.chooseBest(null);
			while (!(best instanceof ClassStatement)) {
				if (best instanceof AliasStatementImpl) {
					best = DeduceLookupUtils._resolveAlias2((AliasStatementImpl) best, dt2);
				} else if (OS_Type.isConcreteType(best)) {
					throw new NotImplementedException();
				} else
					throw new NotImplementedException();
			}
			if (best == null) {
				var i = dt2.makeIdent(typeName);

				//final IdentExpression ident = IdentExpression.forString(typeName);
				throw new ResolveError(i, lrl);
			}
			aR.setResolved(((ClassStatement) best).getOS_Type());
		}
		case String_ -> {
			@NotNull String typeName = type.getBType().name();
			assert typeName.equals("String_");
			OS_Module prelude = module.prelude();
			if (prelude == null) // README Assume `module' IS prelude
				prelude = module;
			final LookupResultList lrl  = prelude.getContext().lookup("ConstString"); // TODO not sure about String
			@Nullable OS_Element   best = lrl.chooseBest(null);
			while (!(best instanceof ClassStatement)) {
				if (best instanceof AliasStatementImpl) {
					best = DeduceLookupUtils._resolveAlias2((AliasStatement) best, dt2);
				} else if (OS_Type.isConcreteType(best)) {
					throw new NotImplementedException();
				} else
					throw new NotImplementedException();
			}
			if (best == null) {
				//final IdentExpression ident = IdentExpression.forString(typeName);
				final IdentExpression ident = dt2.makeIdent(typeName);
				throw new ResolveError(ident, lrl);
			}
			aR.setResolved((((ClassStatement) best).getOS_Type()));
		}
		case SystemCharacter -> {
			@NotNull String typeName = type.getBType().name();
			assert typeName.equals("SystemCharacter");
			OS_Module prelude = module.prelude();
			if (prelude == null) // README Assume `module' IS prelude
				prelude = module;
			final LookupResultList lrl  = prelude.getContext().lookup("SystemCharacter");
			@Nullable OS_Element   best = lrl.chooseBest(null);
			while (!(best instanceof ClassStatement)) {
				if (best instanceof AliasStatementImpl) {
					best = DeduceLookupUtils._resolveAlias2((AliasStatement) best, dt2);
				} else if (OS_Type.isConcreteType(best)) {
					throw new NotImplementedException();
				} else
					throw new NotImplementedException();
			}
			if (best == null) {
				//final IdentExpression ident = IdentExpression.forString(typeName);
				final IdentExpression ident = dt2.makeIdent(typeName);
				throw new ResolveError(ident, lrl);
			}
			aR.setResolved(((ClassStatement) best).getOS_Type());
		}
		case Boolean -> {
			OS_Module prelude = module.prelude();
			if (prelude == null) // README Assume `module' IS prelude
				prelude = module;
			final LookupResultList     lrl  = prelude.getContext().lookup("Boolean");
			final @Nullable OS_Element best = lrl.chooseBest(null);
			aR.setResolved(((ClassStatement) best).getOS_Type()); // TODO might change to Type
		}
		default -> throw new IllegalStateException("531 Unexpected value: " + type.getBType());
		}
	}

	private static void resolve_user2(final @NotNull OS_Type type,
									  final @NotNull ElLog LOG,
									  final @NotNull DeduceTypes2 dt2,
									  final @NotNull Consumer<GenType> cgt) throws ResolveError {
		resolve_user(type, LOG, dt2, cgt);
	}

	private static void resolve_user(final @NotNull OS_Type type,
									 final @NotNull ElLog LOG,
									 final @NotNull DeduceTypes2 dt2,
									 final @NotNull Consumer<GenType> cgt) throws ResolveError {
		final GenType ggg = new GenTypeImpl();

		final TypeName tn1 = type.getTypeName();
		switch (tn1.kindOfType()) {
		case NORMAL -> {
			final Qualident tn = ((NormalTypeName) tn1).getRealName();

			LOG.info("799 [resolving USER type named] " + tn);

			if (tn != null) {
				final LookupResultList lrl = DeduceLookupUtils.lookupExpression(tn, tn1.getContext(), dt2);

				@Nullable OS_Element best = lrl.chooseBest(null);
				while (best instanceof AliasStatementImpl) {
					best = DeduceLookupUtils._resolveAlias2((AliasStatement) best, dt2);
				}

				if (best == null) {
					if (tn.asSimpleString().equals("Any")) {
						/*return*/
						ggg.setResolved(new OS_AnyType()); // TODO not a class
					}
					throw new ResolveError(tn1, lrl);
				}

				if (best instanceof ClassContext.OS_TypeNameElement) {
					/*return*/
					ggg.setResolved(new OS_GenericTypeNameType((ClassContext.OS_TypeNameElement) best)); // TODO not a class
				} else
					ggg.setResolved(((ClassStatement) best).getOS_Type());
			}
		}
		case FUNCTION, GENERIC -> throw new NotImplementedException();
		case TYPE_OF -> {
			final TypeOfTypeName type_of = (TypeOfTypeName) tn1;
			final Qualident      q       = type_of.typeOf();
			if (q.parts().size() == 1 && q.parts().get(0).getText().equals("self")) {
				assert type_of.getContext() instanceof ClassContext;
				ggg.setResolved(((ClassContext) type_of.getContext()).getCarrier().getOS_Type());
			}
			int y = 2;

		}

//				throw new NotImplementedException();
		default -> throw new IllegalStateException("414 Unexpected value: " + tn1.kindOfType());
		}

		cgt.accept(ggg);
	}

	ResolveTypeInjector _inj() {
		return __inj;
	}

	static class ResolveTypeInjector {
	}
}
