/*
	package tripleo.elijah.stages.deduce.post_bytecode;
       
	import tripleo.elijah.stages.gen_fn.IdentTableEntry;
	import tripleo.elijah.stages.gen_fn.VariableTableEntry;
       
	public class DED_VTE implements DED {
		private final VariableTableEntry variableTableEntry;
       
		public DED_VTE(VariableTableEntry aVariableTableEntry) {
			variableTableEntry = aVariableTableEntry;
		}
       
		public VariableTableEntry getVariableTableEntry() {
			return variableTableEntry;
		}
       
		@Override
		public Kind kind() {
			return Kind.DED_Kind_VariableTableEntry;
		}
       
	}
*/
package tripleo.elijah_durable_congenial.stages.deduce.post_bytecode;

import tripleo.elijah_durable_congenial.lang.i.OS_Element;
import tripleo.elijah_durable_congenial.stages.deduce.DeduceTypes2;
import tripleo.elijah_durable_congenial.stages.gen_fn.GenType;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;

public interface IDeduceElement3 {
	DED elementDiscriminator();

	DeduceTypes2 deduceTypes2();

	OS_Element getPrincipal();

	IBaseEvaFunction generatedFunction();

	GenType genType();

	/**
	 * how is this different from {@link DED.Kind} ??
	 *
	 * @return
	 */
	DeduceElement3_Kind kind();

	enum DeduceElement3_Kind {
		CLASS,
		FUNCTION,
		GEN_FN__CTE,
		GEN_FN__GC_VTE,
		GEN_FN__ITE,
		GEN_FN__PTE,
		// ...,
		GEN_FN__VTE, NAMESPACE
	}
}
