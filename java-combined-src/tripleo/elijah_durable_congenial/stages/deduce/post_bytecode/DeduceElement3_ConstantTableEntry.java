package tripleo.elijah_durable_congenial.stages.deduce.post_bytecode;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.diagnostic.Diagnostic;
import tripleo.elijah_durable_congenial.lang.i.OS_Element;
import tripleo.elijah_durable_congenial.lang.i.OS_Type;
import tripleo.elijah_durable_congenial.stages.deduce.DeduceTypes2;
import tripleo.elijah_durable_congenial.stages.deduce.DeduceTypes2.DeduceTypes2Injector;
import tripleo.elijah_durable_congenial.stages.deduce.post_bytecode.DED.DED_CTE;
import tripleo.elijah_durable_congenial.stages.gen_fn.ConstantTableEntry;
import tripleo.elijah_durable_congenial.stages.gen_fn.GenType;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;

public class DeduceElement3_ConstantTableEntry implements IDeduceElement3 {
	private final    ConstantTableEntry principal;
	public           DeduceTypes2       deduceTypes2;
	public           Diagnostic         diagnostic;
	public           IDeduceElement3    deduceElement3;
	public           IBaseEvaFunction   generatedFunction;
	public @Nullable OS_Type            osType;
	private          GenType            genType;

	@Contract(pure = true)
	public DeduceElement3_ConstantTableEntry(final ConstantTableEntry aConstantTableEntry) {
		principal = aConstantTableEntry;
	}

	@Override
	public @NotNull DED elementDiscriminator() {
		return new DED_CTE(principal);
	}

	@Override
	public DeduceTypes2 deduceTypes2() {
		return deduceTypes2;
	}

	@Override
	public OS_Element getPrincipal() {
		return principal.getDeduceElement3().getPrincipal();
	}

	@Override
	public IBaseEvaFunction generatedFunction() {
		return generatedFunction;
	}

	@Override
	public GenType genType() {
		return genType;
	}

	@Override
	public @NotNull DeduceElement3_Kind kind() {
		return DeduceElement3_Kind.GEN_FN__CTE;
	}

	private DeduceTypes2Injector _inj() {
		return deduceTypes2()._inj();
	}
}
