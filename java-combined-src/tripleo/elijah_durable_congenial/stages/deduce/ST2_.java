package tripleo.elijah_durable_congenial.stages.deduce;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.lang.i.NamespaceStatement;
import tripleo.elijah_durable_congenial.lang.i.OS_Element;
import tripleo.elijah_durable_congenial.lang.impl.VariableStatementImpl;
import tripleo.elijah_durable_congenial.stages.gen_fn.IEvaFunction;
import tripleo.elijah_durable_congenial.stages.gen_fn.IdentTableEntry;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;

class ST2_ {
	ST2_.State___post_dof_idte_register_resolved state___postDofIdteRegisterResolved;

	public DeduceTypes2.DT_State getState___post_dof_idte_register_resolved() {
		if (state___postDofIdteRegisterResolved == null) {
			state___postDofIdteRegisterResolved = new ST2_.State___post_dof_idte_register_resolved();
		}
		return state___postDofIdteRegisterResolved;
	}

	class State___post_dof_idte_register_resolved implements DeduceTypes2.DT_State {
		@Override
		public void applyState(final Times.T aTimes, final CHAIN aCHAIN, final Object xx) {
			//X x = (X)xx;
			DT_Function x = (DT_Function) xx;

			final IBaseEvaFunction     generatedFunction = x.state_generatedFunction();
			final @NotNull DeducePhase deducePhase       = x.state_deducePhase();
			for (@NotNull IdentTableEntry identTableEntry : generatedFunction._idte_list()) {
				if (identTableEntry.getResolvedElement() instanceof final @NotNull VariableStatementImpl vs) {
					OS_Element el  = vs.getParent().getParent();
					OS_Element el2 = generatedFunction.getFD().getParent();
					if (el != el2) {
						if (el instanceof ClassStatement || el instanceof NamespaceStatement)
							// NOTE there is no concept of gf here
							deducePhase.registerResolvedVariable(identTableEntry, el, vs.getName());
					}
				}
			}
		}

		public record X(IEvaFunction generatedFunction, DeducePhase deducePhase) {
		}
	}
}
