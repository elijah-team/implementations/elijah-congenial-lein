package tripleo.elijah_durable_congenial.stages.deduce.fluffy.i;

import tripleo.elijah.ci.LibraryStatementPart;

public interface FluffyLsp {

	LibraryStatementPart getLsp();
}
