package tripleo.elijah_durable_congenial.stages.deduce.fluffy.i;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.util.EventualRegister;
import tripleo.elijah_durable_congenial.comp.i.CompilerController;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;

public interface FluffyComp extends EventualRegister {
	void find_multiple_items(final @NotNull OS_Module aModule);

	FluffyModule module(OS_Module aModule);

	CompilerController getController();

	void setController(Object aController);

	Object/*CF_Inputs*/ getStart();

	void setStart(Object/*CF_Inputs*/ aCFInputs);
}
