package tripleo.elijah_durable_congenial.stages.deduce;

import tripleo.elijah_congenial.deduce.t.TEvaClass;

public interface DP_MutEvaNode {
	boolean resolved_var_table_entries();

	void resolve_var_table_entries(DeducePhase aDeducePhase);

	TEvaClass getTClass();
}
