package tripleo.elijah_durable_congenial.stages.deduce;

import tripleo.elijah_durable_congenial.lang.i.Context;
import tripleo.elijah_durable_congenial.stages.gen_fn.IdentTableEntry;
import tripleo.elijah_durable_congenial.stages.instructions.IdentIA;
import tripleo.elijah_durable_congenial.stages.instructions.Instruction;
import tripleo.elijah_durable_congenial.stages.gen_fn.IBaseEvaFunction;

public record DM_DoAssignIdent(
		IBaseEvaFunction generatedFunction,
		Context fd_ctx,
		Instruction instruction,
		IdentIA identIA,
		IdentTableEntry agn_lhs_ite
) {
}
