package tripleo.elijah_durable_congenial.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.lang.i.*;
import tripleo.elijah_durable_congenial.stages.deduce.*;
import tripleo.elijah_durable_congenial.stages.gen_generic.*;
import tripleo.elijah_durable_congenial.world.i.LivingClass;

import java.util.List;
import java.util.function.Function;

public interface IEvaClass extends DependencyTracker, EvaContainer, IDependencyReferent, GNCoded, IREvaClass {
	void addAccessNotation(AccessNotation an);

	void addConstructor(ConstructorDef aConstructorDef, @NotNull IEvaConstructor aGeneratedFunction);

	void createCtor0();

	void fixupUserClasses(@NotNull DeduceTypes2 aDeduceTypes2, Context aContext);

	void generateCode(GenerateResultEnv aFileGen, @NotNull CodeGenerator aCodeGenerator);

	@Override
	OS_Element getElement();

	@Override
	ClassStatement getKlass();

	@Override
	void setLiving(LivingClass aLiving);

	@NotNull String getName();

	@NotNull String getNumberedName();

	//@Override
	//@NotNull Role getRole();

	default boolean getPragma(String auto_construct) { // TODO this should be part of ContextImpl
		return false;
	}

	@Override
	void register(@NotNull ICodeRegistrar aRegistrar);

	@Override
	String identityString();

	@Override
	OS_Module module();

	boolean isGeneric();

	boolean resolve_var_table_entries(@NotNull DeducePhase aDeducePhase);

	void addDependentFunction(FunctionInvocation aDependentFunction);

	EvaNode getFunction(FunctionDef aFunctionDef);

	void addFunction(FunctionDef aFunctionDef, IEvaFunction aGf);

	List<VarTableEntry> varTable();

	IEvaFunction functionMapGet(FunctionDef aFunctionDef);

	void functionMapDeferred(final FunctionDef aFunctionDef, final FunctionMapDeferred aFunctionMapDeferred);

	void addDependentType(GenType aGenType);

	void putConstructor(ConstructorDef aCd, IEvaConstructor aGf);

	void singleGenerate(Class<?> aKey, Function<Void, Boolean> f);

	ClassInvocation _ci();

	Iterable<? extends IEvaFunction> getFunctionMapValues();

	Iterable<? extends IEvaClass> getClassMapValues();
}
