package tripleo.elijah_durable_congenial.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah_durable_congenial.lang.i.OS_Element;
import tripleo.elijah_durable_congenial.stages.deduce.FunctionInvocation;
import tripleo.elijah_durable_congenial.stages.instructions.InstructionArgument;

public interface DT_Resolvable {
	static @NotNull DT_Resolvable from(@NotNull InstructionArgument aInstructionArgument, /*@NotNull*/ OS_Element aElement, FunctionInvocation aFunctionInvocation) {
		return new DT_Resolvable() {
			@Override
			public Object deduceItem() {
				return aFunctionInvocation;
			}

			@Override
			public InstructionArgument instructionArgument() {
				return aInstructionArgument;
			}

			@Override
			public OS_Element element() {
				return aElement;
			}
		};
	}

	static @NotNull DT_Resolvable from(@NotNull InstructionArgument ia) {
		return new DT_Resolvable() {
			@Override
			public @Nullable Object deduceItem() {
				return null;
			}

			@Override
			public InstructionArgument instructionArgument() {
				return ia;
			}

			@Override
			public @Nullable OS_Element element() {
				return null;
			}
		};
	}

	@Nullable Object deduceItem();

	InstructionArgument instructionArgument();

	@Nullable OS_Element element();
}
