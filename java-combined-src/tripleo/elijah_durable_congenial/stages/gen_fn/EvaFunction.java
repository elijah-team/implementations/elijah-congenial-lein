/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah_durable_congenial.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.lang.i.FunctionDef;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.stages.gen_generic.ICodeRegistrar;

import java.util.List;

/**
 * Created 6/27/21 9:40 AM
 */
public class EvaFunction extends BaseEvaFunction implements IEvaFunction {
	public final @Nullable FunctionDef fd;

	public EvaFunction(final @Nullable FunctionDef functionDef) {
		fd = functionDef;
	}

	//
	// region toString
	//

	@Override
	public @NotNull Role getRole() {
		return Role.FUNCTION;
	}
	@Override
	public void register(final @NotNull ICodeRegistrar aRegistrar) {
		aRegistrar.registerFunction1(this);
	}@Override
	public @NotNull FunctionDef getFD() {
		if (fd != null) return fd;
		throw new IllegalStateException("No function");
	}

	@Override
	public String name() {
		if (fd == null) {
            throw new IllegalArgumentException("null fd");
        }
		return fd.name().asString();
	}

@Override
	public List<VariableTableEntry> vte_list() {
		return _vte_list();
	}

	// endregion

		@Override
	public List<IdentTableEntry> idte_list() {
		return _idte_list();
	}
	@Override
	public List<ProcTableEntry> pte_list() {
		return _prte_list();
	}@Override
	public @Nullable VariableTableEntry getSelf() {
		if (getFD().getParent() instanceof ClassStatement)
			return getVarTableEntry(0);
		else
			return null;
	}

	@Override
	public String identityString() {
		return String.valueOf(fd);
	}

	@Override
	public @NotNull OS_Module module() {
		return getFD().getContext().module();
	}

	@Override
	public String toString() {
		String pte_string = fd.getArgs().toString(); // TODO wanted PTE.getLoggingString
		return String.format("<EvaFunction %s %s %s>", fd.getParent(), fd.name(), pte_string);
	}







}

//
//
//
