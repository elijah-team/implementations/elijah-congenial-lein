package tripleo.elijah_durable_congenial.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.util.Maybe;
import tripleo.elijah.util.NotImplementedException;
import tripleo.elijah_durable_congenial.lang.i.*;
import tripleo.elijah_durable_congenial.lang.impl.*;
import tripleo.elijah_durable_congenial.stages.gen_generic.CodeGenerator;
import tripleo.elijah_durable_congenial.stages.gen_generic.GenerateResultEnv;
import tripleo.elijah_durable_congenial.stages.gen_generic.ICodeRegistrar;
import tripleo.elijah_durable_congenial.util.Helpers;
import tripleo.elijah_durable_congenial.world.i.LivingNamespace;

/**
 * Created 12/22/20 5:39 PM
 */
public class EvaNamespace extends EvaContainerNC implements IEvaNamespace {
	private final OS_Module          module;
	private final NamespaceStatement namespaceStatement;
	public        LivingNamespace    _living;

	public EvaNamespace(NamespaceStatement aNamespaceStatement, OS_Module aModule) {
		namespaceStatement = aNamespaceStatement;
		module             = aModule;
	}

	@Override
	public void addAccessNotation(AccessNotationImpl an) {
		throw new NotImplementedException();
	}

	@Override
	public void createCtor0() {
		// TODO implement me
		FunctionDef fd = new FunctionDefImpl(namespaceStatement, namespaceStatement.getContext());
		fd.setName(Helpers.string_to_ident("<ctor$0>"));
		Scope3Impl scope3 = new Scope3Impl(fd);
		fd.scope(scope3);
		for (VarTableEntry varTableEntry : varTable) {
			if (varTableEntry.initialValue != IExpression.UNASSIGNED) {
				IExpression left  = varTableEntry.nameToken;
				IExpression right = varTableEntry.initialValue;

				IExpression e = ExpressionBuilder.build(left, ExpressionKind.ASSIGNMENT, right);
				scope3.add(new StatementWrapperImpl(e, fd.getContext(), fd));
			} else {
				if (getPragma("auto_construct")) {
					scope3.add(new ConstructStatementImpl(fd, fd.getContext(), varTableEntry.nameToken, null, null));
				}
			}
		}
	}

	@Override
	public String getName() {
		return namespaceStatement.getName();
	}

	@Override
	public NamespaceStatement getNamespaceStatement() {
		return this.namespaceStatement;
	}

	@Override
	public @NotNull Role getRole() {
		return Role.NAMESPACE;
	}

	@Override
	public void register(final @NotNull ICodeRegistrar aRegistrar) {
		aRegistrar.registerNamespace(this);
	}

	@Override
	public void setLiving(final LivingNamespace aLivingNamespace) {
		_living = aLivingNamespace;
	}

	@Override
	public OS_Element getElement() {
		return getNamespaceStatement();
	}

	@Override
	public void generateCode(final GenerateResultEnv aFileGen, final CodeGenerator aGgc) {

	}

	@Override
	public @NotNull Maybe<VarTableEntry> getVariable(String aVarName) {
		for (VarTableEntry varTableEntry : varTable) {
			if (varTableEntry.nameToken.getText().equals(aVarName))
				return new Maybe<>(varTableEntry, null);
		}
		return new Maybe<>(null, _def_VarNotFound);
	}

	@Override
	public String identityString() {
		return String.valueOf(namespaceStatement);
	}

	@Override
	public OS_Module module() {
		return module;
	}
}
