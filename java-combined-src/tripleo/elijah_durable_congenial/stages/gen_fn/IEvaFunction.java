package tripleo.elijah_durable_congenial.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah_durable_congenial.lang.i.FunctionDef;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.stages.deduce.nextgen.DR_Ident;
import tripleo.elijah_durable_congenial.stages.gen_generic.ICodeRegistrar;

import java.util.List;

public interface IEvaFunction extends DependencyTracker, IBaseEvaFunction, GNCoded {
	@Override
	@NotNull FunctionDef getFD();

	@Override
	@Nullable VariableTableEntry getSelf();

	@Override
	@NotNull Role getRole();

	@Override
	void register(@NotNull ICodeRegistrar aRegistrar);

	@Override
	String identityString();

	@Override
	@NotNull OS_Module module();

	String name();

	List<VariableTableEntry> vte_list();

	List<IdentTableEntry> idte_list();

	List<ProcTableEntry> pte_list();

	DR_Ident getIdent(ProcTableEntry aProcTableEntry);
}
