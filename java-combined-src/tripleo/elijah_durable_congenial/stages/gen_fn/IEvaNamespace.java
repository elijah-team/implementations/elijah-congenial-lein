package tripleo.elijah_durable_congenial.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.lang.i.FunctionDef;
import tripleo.elijah_durable_congenial.lang.i.NamespaceStatement;
import tripleo.elijah_durable_congenial.lang.i.OS_Element;
import tripleo.elijah_durable_congenial.lang.i.OS_Module;
import tripleo.elijah_durable_congenial.lang.impl.AccessNotationImpl;
import tripleo.elijah_durable_congenial.stages.gen_generic.CodeGenerator;
import tripleo.elijah_durable_congenial.stages.gen_generic.GenerateResultEnv;
import tripleo.elijah_durable_congenial.stages.gen_generic.ICodeRegistrar;
import tripleo.elijah_durable_congenial.stages.gen_generic.IDependencyReferent;
import tripleo.elijah.util.Maybe;
import tripleo.elijah_durable_congenial.world.i.LivingNamespace;

public interface IEvaNamespace extends DependencyTracker, EvaContainer, IDependencyReferent, GNCoded {
	void addAccessNotation(AccessNotationImpl an);

	void createCtor0();

	@Override
	OS_Element getElement();

	@Override
	@NotNull Maybe<VarTableEntry> getVariable(String aVarName);

	String getName();

	NamespaceStatement getNamespaceStatement();

	default boolean getPragma(String auto_construct) { // TODO this should be part of ContextImpl
		return false;
	}

	@Override
	@NotNull Role getRole();

	@Override
	void register(@NotNull ICodeRegistrar aRegistrar);

	void generateCode(GenerateResultEnv aFileGen, CodeGenerator aGgc);

	@Override
	String identityString();

	@Override
	OS_Module module();

	IEvaFunction getFunction(FunctionDef aFunctionDef);

	void addFunction(FunctionDef aFunctionDef, IEvaFunction aGf);

	void setLiving(LivingNamespace aLivingNamespace);
}
