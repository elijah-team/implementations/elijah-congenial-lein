package tripleo.elijah_durable_congenial.stages.gen_fn;

import tripleo.elijah_durable_congenial.stages.deduce.nextgen.DR_Item;

import java.util.List;

public interface AddDrsSource {
	IBaseEvaFunction getFunction();

	List<DR_Item> getItems();
}
