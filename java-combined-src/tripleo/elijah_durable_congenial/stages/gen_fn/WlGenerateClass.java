package tripleo.elijah_durable_congenial.stages.gen_fn;

import org.jdeferred2.DoneCallback;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.stages.gen_fn_r.*;
import tripleo.elijah_durable_congenial.stages.deduce.GeneratedClasses;
import tripleo.elijah.util.Eventual;
import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.nextgen.rosetta.Rosetta;
import tripleo.elijah_durable_congenial.stages.deduce.ClassInvocation;
import tripleo.elijah_durable_congenial.stages.gen_fn_r.RegisterClassInvocation_env;
import tripleo.elijah_durable_congenial.stages.gen_fn_r.GenerateEvaClassRequest;
import tripleo.elijah_durable_congenial.stages.gen_generic.ICodeRegistrar;
import tripleo.elijah.util.Holder;
import tripleo.elijah.util.NotImplementedException;
import tripleo.elijah.work.WorkJob;
import tripleo.elijah.work.WorkManager;

/**
 * Created 5/16/21 12:41 AM
 */
public class WlGenerateClass implements WorkJob {
    private final @NotNull ClassStatement              classStatement;
    private final @NotNull ClassInvocation             classInvocation;
    private final          GenerateFunctions           generateFunctions;
    private final @NotNull RegisterClassInvocation_env __passthru_env;
    private final          ICodeRegistrar              cr;
    //private       EvaClass       Result;
    private final          Eventual<IEvaClass>         resultPromise = new Eventual<>();
    private                boolean                     _isDone       = false;

    public WlGenerateClass(GenerateFunctions aGenerateFunctions,
                           @NotNull ClassInvocation aClassInvocation,
                           GeneratedClasses coll,
                           final ICodeRegistrar aCodeRegistrar) {
        classStatement    = aClassInvocation.getKlass();
        generateFunctions = aGenerateFunctions;
        classInvocation   = aClassInvocation;
        if (coll != null) {
            resultPromise.then(coll::add);
        }

		cr = aCodeRegistrar;

		__passthru_env = null;
	}

    public WlGenerateClass(final GenerateFunctions aGenerateFunctions,
                           final ClassInvocation aClassInvocation,
                           final GeneratedClasses coll,
                           final ICodeRegistrar aCodeRegistrar,
                           final RegisterClassInvocation_env aEnv) {
        classStatement    = aClassInvocation.getKlass();
        generateFunctions = aGenerateFunctions;
        classInvocation   = aClassInvocation;
        if (coll != null) {
            resultPromise.then(coll::add);
        }

		cr = aCodeRegistrar;

		__passthru_env = aEnv;
	}

    @Override
    public boolean isDone() {
        return _isDone;
    }

    @Override
    public void run(WorkManager aWorkManager) {
        final Eventual<IEvaClass> resolvePromise = classInvocation.resolveDeferred();

        resolvePromise.then(resultPromise::resolve); // 24/02/28 whoa

        // README 23/11/10 Could uncomment, but failure is Void, not Diagnostic
        //resolvePromise.fail(resultPromise::fail(x));

		switch (resolvePromise.state()) {
		case PENDING:
			GenerateEvaClassRequest rq = new GenerateEvaClassRequest(generateFunctions, classStatement, classInvocation, __passthru_env);
			GenerateEvaClassResponse rsp = new GenerateEvaClassResponse();
			Rosetta.GECR rosetta = Rosetta.create(rq, rsp);

			rosetta.apply();

			rsp.getEvaClassPromise().then(kl -> {
				//kl.setCode(generateFunctions.module.getCompilation().nextClassCode());

				cr.registerClass1(kl);

                    resolvePromise.resolve(kl);
                });

			break;
		case RESOLVED:
			Holder<IEvaClass> hgc = new Holder<IEvaClass>();
			resolvePromise.then(hgc::set);
			//Result = hgc.get();
			break;
		case REJECTED:
			throw new NotImplementedException();
		}
		_isDone = true;
	}

	public void resultPromise(final DoneCallback<IEvaClass> cb) {
		resultPromise.then(cb);
	}
}
