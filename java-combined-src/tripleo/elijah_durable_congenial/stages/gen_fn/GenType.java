package tripleo.elijah_durable_congenial.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah_durable_congenial.comp.i.ErrSink;
import tripleo.elijah_durable_congenial.contexts.ClassContext;
import tripleo.elijah_durable_congenial.lang.i.AliasStatement;
import tripleo.elijah_durable_congenial.lang.i.ClassStatement;
import tripleo.elijah_durable_congenial.lang.i.DecideElObjectType;
import tripleo.elijah_durable_congenial.lang.i.LookupResultList;
import tripleo.elijah_durable_congenial.lang.i.NamespaceStatement;
import tripleo.elijah_durable_congenial.lang.i.NormalTypeName;
import tripleo.elijah_durable_congenial.lang.i.OS_Element;
import tripleo.elijah_durable_congenial.lang.i.OS_Type;
import tripleo.elijah_durable_congenial.lang.i.TypeName;
import tripleo.elijah_durable_congenial.stages.deduce.ClassInvocation;
import tripleo.elijah_durable_congenial.stages.deduce.DeduceLookupUtils;
import tripleo.elijah_durable_congenial.stages.deduce.DeducePhase;
import tripleo.elijah_durable_congenial.stages.deduce.DeduceTypes2;
import tripleo.elijah_durable_congenial.stages.deduce.FunctionInvocation;
import tripleo.elijah_durable_congenial.stages.deduce.IInvocation;
import tripleo.elijah_durable_congenial.stages.deduce.NamespaceInvocation;
import tripleo.elijah_durable_congenial.stages.deduce.ResolveError;
import tripleo.elijah.util.Mode;
import tripleo.elijah_durable_congenial.stages.deduce.nextgen.DR_Type;
import tripleo.elijah_durable_congenial.stages.logging.ElLog;
import tripleo.elijah.util.NotImplementedException;
import tripleo.elijah.util.Operation2;

import java.util.function.Supplier;

public interface GenType {
    static @Nullable GenType makeFromOSType(@NotNull OS_Type aVt, ClassInvocation.@NotNull CI_GenericPart aGenericPart, @NotNull DeduceTypes2 dt2, DeducePhase phase, @NotNull ElLog aLOG, @NotNull ErrSink errSink) {
        return makeGenTypeFromOSType(aVt, aGenericPart, aLOG, errSink, dt2, phase);
    }

    static @Nullable GenType makeGenTypeFromOSType(@NotNull OS_Type aType,
                                                   ClassInvocation.@NotNull CI_GenericPart aGenericPart,
                                                   @NotNull ElLog aLOG,
                                                   @NotNull ErrSink errSink, @NotNull DeduceTypes2 dt2, DeducePhase phase) {
        GenType gt = new GenTypeImpl();
        gt.setTypeName(aType);
        if (aType.getType() == OS_Type.Type.USER) {
            final TypeName tn1 = aType.getTypeName();
            if (tn1.isNull()) return null; // TODO Unknown, needs to resolve somewhere

            assert tn1 instanceof NormalTypeName;
            final NormalTypeName       tn  = (NormalTypeName) tn1;
            final LookupResultList     lrl = tn.getContext().lookup(tn.getName());
            final @Nullable OS_Element el  = lrl.chooseBest(null);

            DeduceTypes2.ProcessElement.processElement(el, new DeduceTypes2.IElementProcessor() {
                @Override
                public void elementIsNull() {
                    NotImplementedException.raise();
                }

                @Override
                public void hasElement(final OS_Element el) {
                    final Operation2<OS_Element> best1 = preprocess(el);
                    if (best1.mode() == Mode.FAILURE) {
                        aLOG.err("152 Can't resolve Alias statement " + el);
                        errSink.reportDiagnostic(best1.failure());
                        return;
                    }

                    final OS_Element best = best1.success();

                    switch (DecideElObjectType.getElObjectType(best)) {
                        case CLASS:
                            final ClassStatement classStatement = (ClassStatement) best;
                            gt.setResolved(classStatement.getOS_Type());
                            break;
                        case TYPE_NAME_ELEMENT:
                            final ClassContext.OS_TypeNameElement typeNameElement = (ClassContext.OS_TypeNameElement) best;
                            __hasElement__typeNameElement(typeNameElement);
                            break;
                        default:
                            aLOG.err("143 " + el);
                            throw new NotImplementedException();
                    }

                    if (gt.getResolved() != null)
                        gotResolved(gt);
                    else {
                        int y = 2; //05/22
                    }
                }

                private @NotNull Operation2<@NotNull OS_Element> preprocess(final OS_Element el) {
                    @Nullable OS_Element best = el;
                    try {
                        while (best instanceof AliasStatement) {
                            best = DeduceLookupUtils._resolveAlias2((AliasStatement) best, dt2);
                        }
                        assert best != null;
                        return Operation2.success(best);
                    } catch (ResolveError aResolveError) {
                        return Operation2.failure(aResolveError);
                    }
                }

                private void __hasElement__typeNameElement(final ClassContext.@NotNull OS_TypeNameElement typeNameElement) {
                    assert aGenericPart != null;

                    final OS_Type x = aGenericPart.get(typeNameElement.getTypeName());

                    switch (x.getType()) {
                        case USER_CLASS:
                            final @Nullable ClassStatement classStatement1 = x.getClassOf(); // always a ClassStatement

                            assert classStatement1 != null;

                            // TODO test next 4 (3) lines are copies of above
                            gt.setResolved(classStatement1.getOS_Type());
                            break;
                        case USER:
                            final NormalTypeName tn2 = (NormalTypeName) x.getTypeName();
                            final LookupResultList lrl2 = tn.getContext().lookup(tn2.getName());
                            final @Nullable OS_Element el2 = lrl2.chooseBest(null);

                            // TODO test next 4 lines are copies of above
                            if (el2 instanceof final @NotNull ClassStatement classStatement2) {
                                gt.setResolved(classStatement2.getOS_Type());
                            } else
                                throw new NotImplementedException();
                            break;
                    }
                }

                private void gotResolved(final @NotNull GenType gt) {
                    if (gt.getResolved().getClassOf().getGenericPart().size() != 0) {
                        //throw new AssertionError();
                        aLOG.info("149 non-generic type " + tn1);
                    }
                    gt.genCI(null, dt2, errSink, phase); // TODO aGenericPart
                    assert gt.getCi() != null;
                    genNodeForGenType2(gt);
                }
            });
        } else
            throw new AssertionError("Not a USER Type");
        return gt;
    }

    OS_Type getResolved();

    void setResolved(OS_Type aOSType);

    ClassInvocation genCI(TypeName aGenericTypeName,
                          DeduceTypes2 deduceTypes2,
                          ErrSink errSink,
                          DeducePhase phase);

    IInvocation getCi();

    /**
     * Sets the node for a GenType, invocation must already be set
     *
     * @param aGenType the GenType to modify.
     */
    static void genNodeForGenType2(@NotNull GenType aGenType) {
//		assert aGenType.nonGenericTypeName != null;

        final IInvocation invocation = aGenType.getCi();

        if (invocation instanceof NamespaceInvocation namespaceInvocation) {
            namespaceInvocation.onResolve(aGenType::setNode);
        } else if (invocation instanceof ClassInvocation classInvocation) {
            classInvocation.onResolve(aGenType::setNode);
        } else
            throw new IllegalStateException("invalid invocation");
    }

    void setCi(IInvocation aInvocation);

    //@ensures Result.ci != null
    //@ensures ResultgetResolved() != null
    static @NotNull GenType of(NamespaceStatement aNamespaceStatement, @NotNull Supplier<NamespaceInvocation> aNamespaceInvocationSupplier) {
        final GenType genType = new GenTypeImpl(aNamespaceStatement);

        final NamespaceInvocation nsi = aNamespaceInvocationSupplier.get();

        genType.setCi(nsi);

        return genType;
    }

    String asString();

    void copy(GenType aGenType);

    void genCIForGenType2(DeduceTypes2 deduceTypes2);

    void genCIForGenType2__(DeduceTypes2 aDeduceTypes2);

    boolean isNull();

    void set(@NotNull OS_Type aType);

    EvaNode getNode();

    void setNode(EvaNode aResult);

    NamespaceStatement getResolvedn();

    void setResolvedn(NamespaceStatement parent);

    OS_Type getTypeName();

    void setTypeName(OS_Type aType);

    TypeName getNonGenericTypeName();

    void setNonGenericTypeName(@NotNull TypeName typeName);

    FunctionInvocation getFunctionInvocation();

    void setFunctionInvocation(FunctionInvocation aFi);

    void setDrType(DR_Type aDrType);
}
