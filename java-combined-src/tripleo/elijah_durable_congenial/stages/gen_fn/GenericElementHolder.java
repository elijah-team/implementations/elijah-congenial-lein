package tripleo.elijah_durable_congenial.stages.gen_fn;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah_durable_congenial.lang.i.OS_Element;

/**
 * Created 6/30/21 2:31 AM
 */
public class GenericElementHolder implements IElementHolder {
	private final @NotNull OS_Element element;

	public GenericElementHolder(final @NotNull OS_Element aElement) {
		element = aElement;
	}

	@Override
	public @NotNull OS_Element getElement() {
		return element;
	}
}
