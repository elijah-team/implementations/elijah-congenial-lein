package tripleo.elijah_durable_congenial.stages.gen_fn;

import tripleo.elijah_durable_congenial.stages.instructions.InstructionArgument;

import java.util.List;

interface GFS_ProcedureCall {
	List<InstructionArgument> getIdentIAPathList();

	InstructionArgument simplify();
}
