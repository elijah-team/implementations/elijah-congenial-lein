package tripleo.elijah_durable_congenial.stages.write_stage.pipeline_impl;

public interface XPrintStream {
	void println(String aS);
}
