package tripleo.elijah.util;

public class ProgramIsWrongIfYouAreHere extends RuntimeException {
	public ProgramIsWrongIfYouAreHere(final String message) {
		super(message);
	}
}
