package tripleo.elijah.util;

import java.io.Serial;

public class UnintendedUseException extends RuntimeException {
	@Serial
	private static final long serialVersionUID = -1838486886356864300L;
	public UnintendedUseException(String string) {
		super(string);
	}
	public UnintendedUseException() {
		super();
	}
}
