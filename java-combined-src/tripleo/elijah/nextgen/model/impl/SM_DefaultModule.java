//package tripleo.elijah.nextgen.model.impl;
//
//import org.jetbrains.annotations.NotNull;
//import tripleo.elijah.lang.i.OS_Module;
//import tripleo.elijah.nextgen.inputtree.EIT_ModuleInput;
//import tripleo.elijah.nextgen.model.SM_Module;
//import tripleo.elijah.nextgen.model.SM_ModuleItem;
//
//import java.util.List;
//import java.util.stream.Collectors;
//
//public class SM_DefaultModule implements SM_Module {
//	private final EIT_ModuleInput EITModuleInput;
//
//	public SM_DefaultModule(final EIT_ModuleInput aEITModuleInput) {
//		EITModuleInput = aEITModuleInput;
//	}
//
//	@Override
//	public @NotNull List<SM_ModuleItem> items() {
//		final List<SM_ModuleItem> items = getModule().getItems().stream()
//				.map(SM_DefaultModuleItem::new)
//				.collect(Collectors.toList());
//
//		return items;
//	}
//
//	private OS_Module getModule() {
//		return EITModuleInput.module();
//	}
//}
