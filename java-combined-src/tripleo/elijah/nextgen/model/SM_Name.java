package tripleo.elijah.nextgen.model;

import tripleo.elijah.util.Eventual;

public interface SM_Name {
	String getText();

	Eventual<Object> getResolvedType(); // ?? get some inspiration for this

	//<K, V> void assoc(K aKey, V aValue);
	//<K, V> V deassoc(K aKey);

	enum SM_NameRole {
		Type,
		Class,
		Namespace,
		Member,
		Variable,
		Self
	}
}
