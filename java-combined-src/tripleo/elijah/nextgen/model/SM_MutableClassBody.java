package tripleo.elijah.nextgen.model;

public interface SM_MutableClassBody extends SM_ClassBody {
	void addChild(SM_ClassItem aChild);
}
