package tripleo.elijah.nextgen.model;

public interface SM_Node {
	default String asPrintable() {
		return this.toString();
	}
}
