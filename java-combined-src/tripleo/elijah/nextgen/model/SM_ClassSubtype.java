package tripleo.elijah.nextgen.model;

public enum SM_ClassSubtype {
	NORMAL,
	ABSTRACT,
	INTERFACE,
	SIGNATURE,
	STRUCT
}
