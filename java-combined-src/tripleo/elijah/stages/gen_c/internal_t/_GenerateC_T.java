//package tripleo.elijah.stages.gen_c.internal_t;
//
//import tripleo.elijah.stages.gen_c.*;
//
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.HashMap;
//import java.util.Map;
//
//@SuppressWarnings("all")
//public abstract class _GenerateC_T {
//  protected final Map<EvaNode, WhyNotGarish_Item> a_directory = new HashMap<EvaNode, WhyNotGarish_Item>();
//
//  public Collection<WhyNotGarish_Item> __directoryValues() {
//    return this.a_directory.values();
//  }
//
//  public Collection<WhyNotGarish_Item> __directoryValuesCopy() {
//    Collection<WhyNotGarish_Item> _values = this.a_directory.values();
//    return new ArrayList<WhyNotGarish_Item>(_values);
//  }
//
//  public WhyNotGarish_Constructor a_lookup(final IEvaConstructor aGf) {
//    boolean _containsKey = this.a_directory.containsKey(aGf);
//    if (_containsKey) {
//      WhyNotGarish_Item _get = this.a_directory.get(aGf);
//      return ((WhyNotGarish_Constructor) _get);
//    }
//    WhyNotGarish_Constructor ncc = this.wc().newWhyNotGarish_Constructor(aGf, this._this());
//    this.a_directory.put(aGf, ncc);
//    return ncc;
//  }
//
//  public WhyNotGarish_Function a_lookup(final IBaseEvaFunction aGf) {
//    boolean _containsKey = this.a_directory.containsKey(aGf);
//    if (_containsKey) {
//      WhyNotGarish_Item _get = this.a_directory.get(aGf);
//      return ((WhyNotGarish_Function) _get);
//    }
//    WhyNotGarish_Function ncf = this.wc().newWhyNotGarish_Function(aGf, this._this());
//    this.a_directory.put(aGf, ncf);
//    return ncf;
//  }
//
//  public WhyNotGarish_Class a_lookup(final IEvaClass aGc) {
//    boolean _containsKey = this.a_directory.containsKey(aGc);
//    if (_containsKey) {
//      WhyNotGarish_Item _get = this.a_directory.get(aGc);
//      return ((WhyNotGarish_Class) _get);
//    }
//    WhyNotGarish_Class nck = this.wc().newWhyNotGarish_Class(aGc, this._this());
//    this.a_directory.put(aGc, nck);
//    return nck;
//  }
//
//  public WhyNotGarish_Namespace a_lookup(final IEvaNamespace en) {
//    boolean _containsKey = this.a_directory.containsKey(en);
//    if (_containsKey) {
//      WhyNotGarish_Item _get = this.a_directory.get(en);
//      return ((WhyNotGarish_Namespace) _get);
//    }
//    WhyNotGarish_Namespace ncn = this.wc().newWhyNotGarish_Namespace(en, this._this());
//    this.a_directory.put(en, ncn);
//    return ncn;
//  }
//
//  public abstract GenerateC _this();
//
//  public abstract WC wc();
//
//  public WhyNotGarish_Function a_lookup(final DeducedBaseEvaFunction aGf) {
//    IEvaFunctionBase _carrier = aGf.getCarrier();
//    return this.a_lookup(((IBaseEvaFunction) _carrier));
//  }
//}
