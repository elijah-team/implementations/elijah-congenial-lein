package tripleo.elijah.stages.gen_fn_r;

import tripleo.elijah.util.*;
import tripleo.elijah_durable_congenial.stages.gen_fn.*;

public class GenerateEvaClassResponse {
	private final Eventual<IEvaClass> evaClassPromise = new Eventual<>();

	public Eventual<IEvaClass> getEvaClassPromise() {
		// antilombok
		return evaClassPromise;
	}
}
