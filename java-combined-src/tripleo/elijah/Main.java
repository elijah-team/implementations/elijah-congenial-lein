package tripleo.elijah;

import java.util.List;

import tripleo.elijah_durable_congenial.comp.IO;
import tripleo.elijah_durable_congenial.comp.StdErrSink;
import tripleo.elijah_durable_congenial.comp.i.Compilation;
import tripleo.elijah_durable_congenial.comp.internal.CompilationImpl;

public class Main {
	public Main () {}

	public void main(final List<String> args) throws Exception {
		System.err.println("Called main with "+args);
		final Compilation  comp = new CompilationImpl(new StdErrSink(), new IO());
//		final List<String> ls1  = new ArrayList<String>();
//
//		if (args != null) {
//			ls1.addAll(Arrays.asList(args));
//		}

		comp.feedCmdLine(args);
	}
}
