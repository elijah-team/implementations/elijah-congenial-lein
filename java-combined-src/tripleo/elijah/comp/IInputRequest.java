package tripleo.elijah.comp;

import tripleo.elijah.ci.LibraryStatementPart;
import tripleo.elijah.nextgen.inputtree.EIT_InputType;
//import tripleo.elijah.util.Operation2;
//import tripleo.elijah.world.i.WorldModule;

public interface IInputRequest {
	java.io.File file();

	boolean do_out();

	LibraryStatementPart lsp();

	//void setOp(Operation2<WorldModule> aOwm);

	Finally_Nameable getInput(EIT_InputType aTy);
}
