package tripleo.elijah.comp;

import tripleo.elijah.comp.i.ICompilerInput;
import tripleo.elijah.nextgen.inputtree.EIT_InputType;

public class Finally_Input {
	private final Finally_Nameable nameable;
	private final EIT_InputType    ty;

	public Finally_Input(final ICompilerInput aInp, final EIT_InputType aTy) {
		nameable = aInp.getNameable();
		ty       = aTy;
	}

	public Finally_Input(final Finally_Nameable aNameable, final EIT_InputType aTy) {
		nameable = aNameable;
		ty       = aTy;
	}

	public Finally_Input(final IInputRequest aInp, final EIT_InputType aTy) {
		nameable = aInp.getInput(aTy);
		ty       = aTy;
	}

	public String name() {
		return nameable.getNameableName();
	}

	@Override
	public String toString() {
		final String nameableName = nameable.getNameableName();

		return "Input{nameable=%s, ty=%s}".formatted(nameableName, ty);
	}
}
