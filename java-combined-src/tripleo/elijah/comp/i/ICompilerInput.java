package tripleo.elijah.comp.i;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.Finally_Input;
import tripleo.elijah.comp.Finally_Nameable;
import tripleo.elijah.nextgen.inputtree.EIT_InputType;
import tripleo.elijah.util.Maybe;

import java.io.File;

public interface ICompilerInput {
	/** and or of */
	@NotNull
	static Finally_Nameable getFinallyNameable(ICompilerInput _c) {
		return new Finally_Nameable() {
			@Override
			public String getNameableName() {
				return _c.getInp();
			}
		};
	}

	String getInp();

	void accept_ci(Maybe<ILazyCompilerInstructions> aMaybeCI);

	Maybe<ILazyCompilerInstructions> acceptance_ci();

	boolean isNull();

	boolean isSourceRoot();

	void setSourceRoot();

	void setDirectory(File f);

	void setArg();

	void accept_hash(String hash);

	Finally_Input getInput(EIT_InputType aTy);

	Finally_Nameable getNameable();
}
