package tripleo.elijah.comp.i;

import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.util.Eventual;
import tripleo.elijah.util.Operation;

public interface ILazyCompilerInstructions {
	Eventual<Operation<CompilerInstructions>> getEventual();

	CompilerInstructions get();
}
