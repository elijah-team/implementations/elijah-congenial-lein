package tripleo.elijah.ci;

public interface CiTypeCastExpression extends CiExpression {
	CiTypeName getTypeName();

	void setTypeName(CiTypeName typeName);

	@Override
	boolean is_simple();

	CiTypeName typeName();
}
