(ns congenial-lein.core
  (:require
    [donut.system :as ds])
  (:gen-class)
  (:import (tripleo.elijah Main)))

(def system
  {::ds/defs
   {:app
    {:printer
     #::ds{:start  (fn [_]
                     (.get (future
                       (ds/signal system :elijah/args)
                       (loop []
                         (println "hello!")
                         (Thread/sleep 1000)
                         (recur)))))
           :elijah (fn [{:keys [:elijah/args]}]
                     (.get (future
                             (.main (Main.) args))))
           :stop   (fn [{:keys [::ds/instance]}]
                     (future-cancel instance))}}}

   ::ds/signals {:elijah/args {:order :topsort}
                 ;:your.app/validate {:order :reverse-topsort}
                 }})

(defn main-signal [system]
  (ds/signal system :elijah/args))

(def system2
  {::ds/defs
   {:services
    {:printer #::ds{:start ;(fn [_] (print "donuts are yummy!"))}}
    ;{:compiler #::elijah{:start
     (fn [_] (print "pie tastes great too!"))}}
    }}
   ;::ds/signals {:elijah/start {:order :topsort}}
                 )

(ds/signal system2 ::ds/start)
;(ds/signal system :elijah/start)
  ;::elijah/start)


(defn -main
  "hmm"
  [& args]

  ;(.main (Main.) (if (nil? args) (list) args))

  (if (true? true)
    (do
      ;(update {} ::ds/signals :elijah/args (fn [x] (prn x)))

      (let [running-system (ds/signal system ::ds/start)]
        (main-signal system)
        (Thread/sleep 5000)

        (ds/signal running-system ::ds/stop))))

  (println "All done."))
